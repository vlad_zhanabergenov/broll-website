import { useEffect, useState } from 'react'

export default function useDeviceType() {
  const [type, setType] = useState<'desktop' | 'tablet' | 'mobile'>('desktop')

  useEffect(() => {
    refresh()

    window.addEventListener('resize', refresh)

    return () => {
      window.removeEventListener('resize', refresh)
    }
  }, [])

  const refresh = () => {
    if (window.innerWidth <= 767) {
      setType('mobile')
    } else if (window.innerWidth <= 960) {
      setType('tablet')
    } else {
      setType('desktop')
    }
  }

  return type
}
