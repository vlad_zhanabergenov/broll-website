import { isEmpty, mapValues, pickBy } from 'lodash'
import { useEffect, useState } from 'react'
import FormValidator, { FormValidatorFields } from 'services/formValidator'

interface UseForm<T> {
  fields: {
    [key in keyof T]: T[key] | {
      initialValue: T[key]
      required?: boolean
      validator?: keyof typeof FormValidator.validators
    }
  }
  onChange?: (name: keyof T, newValue: any, prev: any) => void
  handleSubmit?: (values: T, valid: boolean, errors: { [key in keyof T]: string | null }) => void
}

export default function useForm<T extends object>({ fields, onChange, handleSubmit }: UseForm<T>, deps: any[] = []) {
  const getInitialValues = (values: typeof fields) => mapValues(values, (value: any) => {
    if (typeof value === 'object' && value.hasOwnProperty('initialValue')) {
      return value.initialValue
    }
    return value
  }) as T

  const [values, setValues] = useState(getInitialValues(fields))
  const [errors, setErrors] = useState<{ [key in keyof T]: string | null }>(mapValues(values, () => null))

  useEffect(() => {
    const newValues = getInitialValues(fields)
    setValues(newValues)
  }, deps)

  const handleOnChange = (name: string, value: any) => {
    if (onChange) {
      onChange(name as keyof T, value, values[name as keyof T])
    }
  }

  const change = (data: { name: string, value: any }) => {
    handleOnChange(data.name, data.value)
    setValues((currentValues) => {
      return { ...currentValues, [data.name]: data.value }
    })
  }

  const validate = () => {
    const validator = new FormValidator<T>(values, mapValues(fields, (value: any, key: keyof T) => {
      if (typeof value !== 'object' && !value.hasOwnProperty('initialValue')) {
        return { initialValue: value, value: values[key] }
      }
      return { ...value, value: values[key] }
    }) as unknown as FormValidatorFields<T>)
    return validator.errors
  }

  const blur = (name: string) => {
    const validation = validate()
    setErrors({ ...errors, [name]: validation[name as keyof T] })
  }

  const submit = () => {
    const validation = validate()
    setErrors(validation)
    if (handleSubmit) {
      handleSubmit(values, isEmpty(pickBy(validation)), validation)
    }
  }

  const defaultForm = (data: any) => {
    setValues(data)
  }

  return { values, change, blur, errors, submit, defaultForm }
}
