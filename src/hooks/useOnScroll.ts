import { useEffect, useState } from 'react'

export default function useOnScroll(): number {
  const [scrollTop, setScrollTop] = useState(0)

  useEffect(() => {
    const onScroll = (e: Event) => {
      const target = e.target as Document

      if (target) {
        setScrollTop(target.documentElement.scrollTop)
      }
    }

    window.addEventListener("scroll", onScroll)

    return () => {
      window.removeEventListener("scroll", onScroll)
    }
  }, [scrollTop])

  return scrollTop
}
