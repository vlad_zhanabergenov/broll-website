import { useEffect, useRef } from 'react'

export default function useInterval(callback: () => void, delay: number | null, deps: any) {
  const savedCallback = useRef<any>()

  useEffect(() => {
    savedCallback.current = callback
  })

  useEffect(() => {
    function tick() {
      savedCallback.current()
    }
    if (delay !== null) {
      const id = setInterval(tick, delay)
      return () => clearInterval(id)
    }
  }, [delay, deps])
}