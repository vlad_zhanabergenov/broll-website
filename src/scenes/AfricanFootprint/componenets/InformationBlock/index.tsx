import React, { useState } from "react";

import { CloudImage } from "components";

import s from "./style.module.sass";
import cn from "classnames";
import AfricanFootprint1 from "assets/pictures/africanFootprint1";
import AfricanFootprint2 from "assets/pictures/africanFootprint2";
import AfricanFootprint3 from "assets/pictures/africanFootprint3";
import { Button } from '../../../../components'

const images = {
  pic1: AfricanFootprint1,
  pic2: AfricanFootprint2,
  pic3: AfricanFootprint3,
}

const InformationBlock: React.FC = () => {
  const [selectPic, setSelectPic] = useState(images.pic1)

  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.title, "semiBig m-0 red bold")}>
            Our Pan-African Coverage
          </h3>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 mb-4")}>
            A proudly South African business, <br /> with a footprint across
            Africa.
          </h3>
          <div className={s.imageContainer}>
            <CloudImage
              src={selectPic}
              alt="Broll"
              responsive={{
                desktopLarge: { w: "600" },
                desktop: { w: "500" },
                tablet: { w: "500" },
              }}
            />
          </div>
          <div className={s.btnList}>
            <Button red className={s.btn} onClick={() => setSelectPic(images.pic1)}>Physical Presence</Button>
            <Button className={s.btn} onClick={() => setSelectPic(images.pic2)}>Recent Executions</Button>
            <Button className={s.btn} onClick={() => setSelectPic(images.pic3)}>Ad-hoc / Team can be mobilized</Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
