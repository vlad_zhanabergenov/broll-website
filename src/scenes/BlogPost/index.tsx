// import { useForm } from 'hooks'
import React from 'react'
// import { Configure, Index } from 'react-instantsearch-core'

import { Breadcrumbs, SearchBar } from 'components'
// import { BlogResults } from './components'
// import type { Form } from 'components/BlogSearchBar'

import s from './style.module.sass'
import cn from 'classnames'

const BlogPostScene: React.FC = () => {
  // const { values, change } = useForm<Form>({
  //   fields: {
  //     query: { initialValue: "" },
  //     category: { initialValue: [] }
  //   }
  // })

  return (
    <div className={ s.BlogPostScene }>
      <div className={ s.layout }>
        <div className={ cn(s.section1, 'pt-2 ph-17 pb-4 box-sizing') }>
          <Breadcrumbs items={ ['Media Centre', 'Blog'] } className="mb-2"/>
          <div className={ cn(s.searchBar, 'pl-1 pr-3 box-sizing') }>
            <SearchBar/>
          </div>
        </div>
        <div className={ cn(s.section2, 'ph-content pb-6 box-sizing') }>
          <h2 className={ cn('thin m-0 mb-3') }>Blog</h2>
          <div className={ cn(s.search, 'mv-2') }>
            {/*<BlogSearchBar values={ values } change={ change }/>*/}
          </div>
          {/*<div className={ cn(s.results, 'mt-2 mb-6') }>*/}
          {/*  <BlogResults/>*/}
          {/*</div>*/}
        </div>
        <div className={ cn(s.section3, 'ph-content box-sizing') }>
          {/*<Post/>*/}
        </div>
      </div>
    </div>
  )
}

// const BlogConnected = BlogRaw
//
// const Blog: React.FC = () => {
//   return (
//     <>
//       <Configure hitsPerPage={ 9 }/>
//       <Index indexName='Blog'>
//         <BlogConnected/>
//       </Index>
//     </>
//   )
// }

export default BlogPostScene
