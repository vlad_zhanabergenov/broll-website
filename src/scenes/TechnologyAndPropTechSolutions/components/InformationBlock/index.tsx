import React from "react";
import s from "./style.module.sass";
import cn from "classnames";

import { CloudImage } from "components";
import TopImage from '../../../../assets/pictures/technologyAndPropTechSolutions'

const topLeftList = [
  "We allow technology to lead the way in serving the rapidly changing needs and requirements of our clients, and the market. Our property related technologies are distinctive in creating and delivering real value for commercial real estate portfolios.",
  "Our in-house IT team ensures integration synergies across the continent, leveraging combined systems and aggregated data for continuous improvement.",
];

const topRightList = [
  "Improved information quality.",
  "Better transparency on portfolio performance.",
  "Enables faster decision making.",
  "Anticipated trends to inform planning and strategy.",
  "Identification of risks and opportunities.",
  "Integrates resources, skills and people on-the-ground.",
];

const leftList = [
  "Dedicated technology development team.",
  "Software development",
  "Mobile apps",
  "Data migration",
  "Information security audits",
];

const rightList = [
  "Disaster recovery and business continuity",
  "Audits",
  "Support and training",
  "IT Governance",
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
          Enabling real-time decisions.
          </h3>
          <div className={ cn(s.topImage, "mt-1") }>
            <CloudImage
              src={ TopImage }
              alt="Broll"
              className="covered"
              responsive={{
                desktop: { w: '1920' },
              }}
            />
          </div>
          <div className={s.container}>
            <div className={cn(s.left, "")}>
              <h3 className={cn(s.title, "semiBig bold red-4")}>
                Our Value Proposition
              </h3>
              {topLeftList.map((item) => (
                <p
                  className={cn(
                    s.text,
                    "white small light letterSpacingNormal mb-0"
                  )}
                >
                  {item}
                </p>
              ))}
            </div>
            <div className={cn(s.right)}>
              <h3 className={cn(s.title, "semiBig bold m-0 mb-1")}>
              Integrated PropTech Solutions Providing:
              </h3>
              {topRightList.map((item, i) => (
                <p key={i} className="x-small regular">
                  {item}
                </p>
              ))}
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-3")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-1")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {leftList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
            <div className={cn(s.right, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {rightList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className={cn(s.section3, "grey-2 pb-4")}>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0 mt-1")}>15</h2>
                <p className={cn(s.text, "small light")}>
                In-house IT professionals.
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0 mt-1")}>2,000</h2>
                <p className={cn(s.text, "small light")}>
                End-users supported daily across Africa 
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light mt-1")}>Support to</p>
                <h2 className={cn(s.title, "small bold m-0")}>100</h2>
                <p className={cn(s.text, "small light")}>remote sites</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
