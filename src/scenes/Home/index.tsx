import React from 'react'

import { HeroSearch, Disclaimer, Mission } from 'components'
import { HeroImageCarousel, FeaturedOfferings, NewsMediaGrid } from './components'

import cn from 'classnames'
import s from "./style.module.sass"

interface Props {
  heroCarouselList: HeroCarouselItem[]
  newsMediaList: NewsMediaItem[]
  featuredOfferingsList: AnyPropertyWithCustomFields[]
}

const HomeScene: React.FC<Props> = ({
  heroCarouselList,
  newsMediaList,
  featuredOfferingsList
}) => {
  return (
    <div className={ s.HomeScene }>
      <div className={ s.section1 }>
        <div className={ s.layerDark }/>
        <div className={ s.layerRed }/>
        <div className={ cn(s.carousel, 'ph box-sizing') }>
          <HeroImageCarousel auto timing={ 5000 } list={ heroCarouselList }/>
        </div>
      </div>
      <div className={ cn(s.section2, 'pb-2 ph box-sizing') }>
        <HeroSearch/>
      </div>
      <div className={ cn(s.section3, 'pt-3 pb-5 box-sizing') }>
        <FeaturedOfferings list={ featuredOfferingsList }/>
      </div>
      <div className={ cn(s.section4) }>
        <Mission/>
      </div>
      <div className={ cn(s.section5, 'pt-5 pb-2 box-sizing') }>
        <NewsMediaGrid list={ newsMediaList }/>
      </div>
      <Disclaimer/>
    </div>
  )
}

export default HomeScene
