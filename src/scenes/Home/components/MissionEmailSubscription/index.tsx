import React, { useEffect, useState } from 'react'
import { useForm } from 'hooks'
import { useForm as useFormspree } from '@formspree/react'

import { Button, Input } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Form {
  email: string
}

const MissionEmailSubscription: React.FC = () => {
  const [loading, setLoading] = useState(false)
  const [message, setMessage] = useState("")

  const [formspreeState, handleFormspreeSubmit] = useFormspree(process.env['NEXT_PUBLIC_FORMSPREE_EMAIL_SUBSCRIPTION_ID']!)

  useEffect(() => {
    if (formspreeState.succeeded) {
      setLoading(false)
      change({ name: 'email', value: "" })
      setMessage("Your message has been sent successfully.")
    } else if (formspreeState.errors.length) {
      setLoading(false)
      setMessage("Something went wrong, please try again later.")
    }
  }, [formspreeState])

  const handleSubmit = (values: Form, valid: boolean) => {
    if (valid) {
      setLoading(true)
      handleFormspreeSubmit(values)
    }
  }

  const { values, errors, change, submit } = useForm<Form>({
    fields: {
      email: { initialValue: "", validator: 'email', required: true }
    }, handleSubmit
  })

  return (
    <div className={ cn(s.MissionEmailSubscription) }>
      <div className={ cn(s.layout, "pt-6 pb-5 ph-6 box-sizing") }>
        <div className={ s.left }>
          <h3 className="big bold white mt-0 mb-05">We’re on a mission</h3>
          <p className={ cn(s.text, "small light white m-0") }>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
            nonummy nibh euismod tincidunt.
          </p>
        </div>
        <div className={ cn(s.right, "box-sizing") }>
          <h4 className={ cn(s.text, "thin white m-0 mr-1") }>Email</h4>
          <Input linear white name="email" value={ values.email } onChange={ change } onPressEnter={ submit } error={ errors.email } message={ message }/>
          <Button secondary white className="ml-1" onClick={ submit } loading={ loading }>Send</Button>
        </div>
      </div>
    </div>
  )
}

export default MissionEmailSubscription
