import React, { useRef } from 'react'
import useDeviceType from 'hooks/useDeviceType'
import dynamic from 'next/dynamic'
import Slider, { Settings } from "react-slick"
import { startCase } from 'lodash'
import formatNumberWith from 'services/formatNumberWith'

import { Link, Icon, SpecialOffer, CloudImage } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrow.svg'
import GLAIcon from 'assets/icons/gla.svg'
import TagIcon from 'assets/icons/tag.svg'
import MoneyIcon from 'assets/icons/money.svg'
import ViewIcon from 'assets/icons/view.svg'
import NoPropertyImage from 'assets/pictures/noPropertyImage'

const PropertyCardSlider = dynamic(
  () => import('components/PropertyCardSlider'),
  { ssr: false }
)

const settings: Settings = {
  className: "slickSlider",
  lazyLoad: 'ondemand',
  centerMode: true,
  infinite: true,
  centerPadding: "0",
  slidesToShow: 3,
  speed: 500,
  responsive: [
    {
      breakpoint: 960,
      settings: {
        slidesToShow: 1,
        centerPadding: "180px"
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        centerPadding: "60px"
      }
    }
  ]
}

interface Props {
  list: AnyPropertyWithCustomFields[]
}

const FeaturedOfferingsCarousel: React.FC<Props> = ({ list }) => {
  const deviceType = useDeviceType()

  const sliderRefDesktop = useRef<Slider>(null)
  const sliderRefMobile = useRef<Slider>(null)

  const handleChange = (type: 'prev' | 'next') => {
    if (sliderRefDesktop.current) {
      if (type === 'prev') {
        sliderRefDesktop.current.slickPrev()
      } else {
        sliderRefDesktop.current.slickNext()
      }
    }
    if (sliderRefMobile.current) {
      if (type === 'prev') {
        sliderRefMobile.current.slickPrev()
      } else {
        sliderRefMobile.current.slickNext()
      }
    }
  }

  if (!list.length) {
    return null
  }

  const mapList = list.map(item => {
    const itemImage = item.best_image || item.property_images?.[0]?.image_path_url || NoPropertyImage
    return (
      <div key={ `${ item.objectID }-${ item.property_name }` } className={ cn(s.item, "p-1 box-sizing") }>
        <div className={ s.layout }>
          <div className={ s.top }>
            { item.hasSpecialDeals &&
              <SpecialOffer className={ cn(s.special) }/>
            }
            <div className={ cn(s.label, "box-sizing pv-05 ph-1") }>
              <p className="bold small uppercase m-0">
                { startCase(item.dealType) }
              </p>
            </div>
            <div className={ s.image }>
              { item.property_images !== undefined && item.property_images?.length > 1 ?
                <PropertyCardSlider images={ item.property_images } data={ item }/>
              :
                <CloudImage
                  className="covered"
                  src={ itemImage }
                  alt={ item.property_name }
                  responsive={{
                    desktop: { w: '500', ar: '1.5' },
                    tablet: { w: '400', ar: '1.5' },
                    mobile: { w: '300', ar: '1.2' }
                  }}
                />
              }
            </div>
            <div className={ cn(s.imageLabel, "pt-3 pb-1 ph-3 box-sizing") }>
              <h3 className="big bold white m-0">{ item.suburb }</h3>
            </div>
          </div>
          <Link to='property' data={{ property: item }}>
            <div className={ cn(s.bottom, "pt-3 pb-2 ph-2 box-sizing") }>
              <h3 className="light mt-0 mb-2">{ item.property_name }</h3>
              <div className={ s.info }>
                <div className={ s.el }>
                  <Icon src={ MoneyIcon } position='left'/>
                  <p className="small light m-0">
                    { item.min_price !== item.max_price ?
                      `R${ formatNumberWith(' ', item.min_price || 0) }-${ formatNumberWith(' ', item.max_price || 0) }${ item.dealType === 'toLet' ? ' per m²' : '' }`
                    :
                      `R${ formatNumberWith(' ', item.gross_price || 0) }${ item.dealType === 'toLet' ? ' per m²' : '' }`
                    }
                  </p>
                </div>
                <div className={ s.el }>
                  <Icon src={ GLAIcon } position='left'/>
                  <p className="small light m-0">
                    { item.min_gla !== item.max_gla ?
                      `GLA ${ formatNumberWith(' ', item.min_gla || 0) }m² - ${ formatNumberWith(' ', item.max_gla || 0) }m²`
                    :
                      `GLA ${ formatNumberWith(' ', item.min_gla || 0) }m²`
                    }
                  </p>
                </div>
                <div className={ s.el }>
                  <Icon src={ TagIcon } position='left'/>
                  <p className="small light m-0">{ item.property_category }</p>
                </div>
                <div className={ s.el }>
                  <Icon src={ ViewIcon } position='left'/>
                  <p className="small light m-0">View</p>
                </div>
              </div>
            </div>
          </Link>
        </div>
      </div>
    )
  })

  return (
    <div className={ cn(s.FeaturedOfferingsCarousel) }>
      { deviceType === 'desktop' ?
        <div className={ cn(s.layout, 'no-tablet no-mobile') }>
          { list.length <= 3 ?
            <div className={ s.list }>
              { mapList }
            </div>
          :
            <>
              <Slider ref={ sliderRefDesktop } { ...settings }>
                { mapList }
              </Slider>
              <div className={ cn(s.sliderControls, s.left, "cursor-pointer no-select")} onClick={ () => handleChange('prev') }>
                <Icon src={ ArrowIcon } rotate={ 270 }/>
              </div>
              <div className={ cn(s.sliderControls, s.right, "cursor-pointer no-select") } onClick={ () => handleChange('next') }>
                <Icon src={ ArrowIcon } rotate={ 90 }/>
              </div>
            </>
          }
        </div>
      :
        <div className={ cn(s.layout, 'no-desktop') }>
          <Slider key="sliderMobile" ref={ sliderRefMobile } { ...settings }>
            { mapList }
          </Slider>
          { list.length > 1 &&
            <>
              <div className={ cn(s.sliderControls, s.left, "cursor-pointer no-select") } onClick={ () => handleChange('prev') }>
                <Icon src={ ArrowIcon } rotate={ 270 }/>
              </div>
              <div className={ cn(s.sliderControls, s.right, "cursor-pointer no-select") } onClick={ () => handleChange('next') }>
                <Icon src={ ArrowIcon } rotate={ 90 }/>
              </div>
            </>
          }
        </div>
      }
    </div>
  )
}

export default FeaturedOfferingsCarousel
