import React from 'react'
import dynamic from 'next/dynamic'

import cn from 'classnames'
import s from './style.module.sass'

const TwitterTimelineEmbed = dynamic<any>(
  () => import('react-twitter-embed').then(module => module.TwitterTimelineEmbed),
  { ssr: false }
)

const TwitterTimeline: React.FC = () => {
  return (
    <div className={ cn(s.TwitterTimeline, "overflow-hidden box-sizing") }>
      <div className={ s.layout }>
        <div className={ cn(s.type, "box-sizing pv-05 ph-4") }>
          <p className="bold xx-small uppercase m-0">twitter</p>
        </div>
        <div className={ cn(s.center, "box-sizing overflow-hidden") }>
          <TwitterTimelineEmbed sourceType="profile" screenName="broll_insights"
                                autoHeight noBorders noHeader noFooter/>
        </div>
      </div>
    </div>
  )
}

export default TwitterTimeline
