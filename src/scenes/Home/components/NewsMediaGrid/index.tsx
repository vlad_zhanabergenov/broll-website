import React from 'react'
import dynamic from 'next/dynamic'
import Slider, { Settings } from 'react-slick'

import { Link, MediaCard } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

const TwitterTimeline = dynamic(
  () => import('./components/TwitterTimeline'),
  { ssr: false }
)

const settings: Settings = {
  className: "slickSlider",
  centerMode: true,
  centerPadding: "0",
  slidesToShow: 3,
  initialSlide: 1,
  speed: 500,
  responsive: [
    {
      breakpoint: 960,
      settings: {
        initialSlide: 2,
        slidesToShow: 1,
        centerPadding: "180px"
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        initialSlide: 2,
        centerPadding: "60px"
      }
    }
  ]
}

interface Props {
  list: NewsMediaItem[]
}

const NewsMediaGrid: React.FC<Props> = ({ list }) => {
  const listWithTwitter: (NewsMediaItem | { id: "twitter-timeline", type: 'twitter' })[] = [...list]
  listWithTwitter.splice(2, 0, {
    id: "twitter-timeline", type: 'twitter'
  })

  if (!list.length) {
    return null
  }

  return (
    <div className={ cn(s.NewsMediaGrid, "overflow-hidden") }>
      <div className={ cn(s.layout, "overflow-hidden") }>
        <Slider { ...settings }>
          { listWithTwitter.map(item => (
            item.type === 'twitter' ?
              <TwitterTimeline key={ item.id }/>
            :
              <Link key={ item.id } to={ `/media-centre/latest-news/${ (item as NewsMediaItem).slug }` }>
                <MediaCard isFeaturedPreview item={ item as NewsMediaItem } className='p-1'/>
              </Link>
          ))}
        </Slider>
      </div>
    </div>
  )
}

export default NewsMediaGrid
