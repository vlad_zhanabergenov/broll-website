import React, { useRef, useState } from 'react'
import Slider, { Settings } from 'react-slick'
import { useInterval } from 'hooks'
import parseRichText from 'services/parseRichText'

import { CloudImage, Icon } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrow.svg'

const settings: Settings = {
  className: "slickSlider",
  lazyLoad: 'ondemand',
  centerMode: true,
  centerPadding: "0",
  slidesToShow: 1,
  speed: 500
}

interface Props {
  list: HeroCarouselItem[]
  auto?: boolean
  timing?: number
}

const HeroImageCarousel: React.FC<Props> = ({
  list,
  auto,
  timing= 3000
}) => {
  const [transition, setTransition] = useState(false)
  const [current, setCurrent] = useState(0)
  const item = list?.[current] || null
  const sliderRef = useRef<Slider>(null)

  useInterval(() => {
    handleChange(current + 1)
  }, auto ? timing : null, [current])

  const handleChange = (num: number) => {
    const change = (num: number) => {
      if (sliderRef.current) {
        sliderRef.current.slickGoTo(num)
      }
    }

    if (num > list.length - 1) {
      change(0)
    } else if (num < 0) {
      change(list.length - 1)
    } else {
      change(num)
    }
  }

  const onSlideChange = (num: number) => {
    setTransition(true)
    setTimeout(() => {
      setCurrent(num)
      setTransition(false)
    }, 600)
  }

  if (!item) {
    return null
  }

  return (
    <div className={ cn(s.HeroImageCarousel) }>
      <div className={ cn(s.layout, "overflow-hidden") }>
        <div className={ cn(s.left, 'pl-4 box-sizing', { [s.transition]: transition }) }>
          <div className={ s.heading }>
            <h1 className="bold white m-0">{ item.heading }</h1>
            <h3 className="light white m-0">{ item.headingSubText }</h3>
          </div>
          <div className={ cn(s.content, "mt-1") }>
            { parseRichText(item.content, {
              classForAll: "white m-0",
              h4Class: "light display-inline",
              pClass: "display-inline"
            }) }
          </div>
          <div className={ cn(s.dots, "no-desktop mv-4") }>
            { list.map((item, num) =>
              <div key={ item.id } onClick={ () => handleChange(num) }
                   className={ cn("mr-2 cursor-pointer", { [s.active]: item.id === list[current].id }) }/>
            )}
          </div>
        </div>
        <div className={ cn(s.right, "overflow-hidden") }>
          <div className={ cn(s.image, "no-mobile", { [s.transition]: transition }) }>
            <CloudImage
              responsive={{
                desktopLarge: { w: '1200' },
                desktop: { w: '900' },
                tablet: { w: '700' },
                mobile: { w: '500' }
              }}
              className='contained'
              src={ item.image }
              alt="Broll"
            />
          </div>
          <div className={ cn(s.slider, "no-desktop no-tablet") }>
            <Slider ref={ sliderRef } { ...settings } beforeChange={ (_current, next) => onSlideChange(next) }>
              { list.map(item =>
                <div key={ item.id } className={ s.image }>
                  <CloudImage
                    responsive={{
                      desktopLarge: { w: '1200' },
                      desktop: { w: '900' },
                      tablet: { w: '700' },
                      mobile: { w: '500' }
                    }}
                    className='contained'
                    src={ item.image }
                    alt="Broll"
                  />
                </div>
              )}
            </Slider>
          </div>
          <div className={ cn(s.controls, "no-tablet no-mobile") }>
            <div className={ cn(s.bottom, "ph-05") }>
              <div className={ cn(s.prev, "cursor-pointer hover-opacity") } onClick={ () => handleChange(current - 1) }>
                <Icon src={ ArrowIcon } size='xs'/>
              </div>
              <div className={ cn(s.dots, "mv-1") }>
                { list.map((item, num) =>
                  <div key={ item.id } onClick={ () => handleChange(num) }
                       className={ cn("mv-05 cursor-pointer", { [s.active]: item.id === list[current].id }) }/>
                )}
              </div>
              <div className={ cn(s.next, "cursor-pointer hover-opacity") } onClick={ () => handleChange(current + 1) }>
                <Icon src={ ArrowIcon } size='xs' rotate={ 180 }/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default HeroImageCarousel
