import React from 'react'
import cn from 'classnames'

import { CloudImage, Icon } from 'components'
import Slider from '../Slider/index'

import ArrowIcon from 'assets/icons/arrow.svg'
import groupOverviewExpansion from 'assets/pictures/groupOverviewExpansion'
import s from './style.module.sass'

const expansionList = [
  {
    date: '2003',
    country: 'Namibia'
  },
  {
    date: '2004',
    country: 'Nigeria'
  },
  {
    date: '2006',
    country: 'Ghana'
  },
  {
    date: '2007',
    country: 'Malawi'
  },
  {
    date: '2013',
    country: 'Rwanda, Indian Ocean, Mauritius, Kenya, Zambia'
  },
  {
    date: '2014',
    country: 'Botswana, Zimbabwe'
  },
  {
    date: '2015',
    country: 'Ivory Coast, Mozambique, Uganda'
  },
  {
    date: '2018',
    country: 'Gabon'
  },
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ cn(s.section1, 'mt-1') }>
          <div className={ s.left }>
            <h3 className='semiBig m-0 red bold mb-2'>Our Purpose</h3>
            <p className='white small light no-mobile no-tablet letterSpacingNormal'>Our purpose is to be the leading real estate services provider and the preferred place of employment for our industry’s professionals. This purpose promotes constant innovation and service excellence, whilst providing end-to-end real estate solutions to our valued clients.</p>
            <p className='white light no-desktop letterSpacingNormal'>Our purpose is to be the leading real estate services provider and the preferred place of employment for our industry’s professionals. This purpose promotes constant innovation and service excellence, whilst providing end-to-end real estate solutions to our valued clients.</p>
            <div className={ cn(s.footer, 'mt-3') }>
              <div className={ cn(s.item, 'mb-2') }>
                <div className={ s.icon }>
                  <Icon src={ ArrowIcon } rotate={ 90 } size='xs'/>
                </div>
                <p className='x-small light white no-mobile no-tablet letterSpacingNormal'><a href='https://res.cloudinary.com/broll-za/image/upload/v1641904465/WEBSITE_RESOURCES/broll_group_overview-compressed_e9g0pq.pdf' target='_blank'><span className='red x-small medium letterSpacingNormal'>Download</span></a> our Company Profile</p>
                <p className='small light white no-desktop letterSpacingNormal'><a href='https://res.cloudinary.com/broll-za/image/upload/v1641904465/WEBSITE_RESOURCES/broll_group_overview-compressed_e9g0pq.pdf' target='_blank'><span className='red small medium letterSpacingNormal'>Download</span></a> our Company Profile</p>
              </div>
              <div className={ s.item }>
                <div className={ s.icon }>
                  <Icon src={ ArrowIcon } rotate={ 90 } size='xs'/>
                </div>
                <p className='x-small light white no-mobile no-tablet letterSpacingNormal'><a href='https://res.cloudinary.com/broll-za/image/upload/v1641904358/WEBSITE_RESOURCES/BEE_CERTIFICATE_2021_iha2ss.pdf' target='_blank'><span className='red x-small medium letterSpacingNormal'>Download</span></a> our BBBEE Verification Certificate</p>
                <p className='small light white no-desktop letterSpacingNormal'><a href='https://res.cloudinary.com/broll-za/image/upload/v1641904358/WEBSITE_RESOURCES/BEE_CERTIFICATE_2021_iha2ss.pdf' target='_blank'><span className='red small medium letterSpacingNormal'>Download</span> </a> our BBBEE Verification Certificate</p>
              </div>
            </div>
          </div>
          <div className={ s.right }>
            <h3 className='semiBig m-0 red bold'>Why choose Broll?</h3>
            <div className={ s.list }>
              <p className='big regular no-mobile no-tablet'>We <span className='bold red'>always</span> put the client's needs first</p>
              <p className='big regular no-mobile no-tablet'><span className='bold red'>Collaboration</span> across markets and service lines</p>
              <p className='big regular no-mobile no-tablet'>Focused teamwork, <span className='bold red'>innovative thinking</span> and acting practically</p>
              <p className='big regular no-mobile no-tablet'>Research led environment to ensure <span className='bold red'>competitive advantage</span></p>
              <p className='big regular no-mobile no-tablet'><span className='bold red'>"Hands on"</span> senior management and executive</p>
              <p className='big regular no-mobile no-tablet'>We do not accept any assignment that we do not feel <br /> comfortable to <span className='bold red'>deliver</span> on</p>
              <p className='big regular no-mobile no-tablet'><span className='bold red'>ISO rated</span> property management <br /> and facilities management process and procedures</p>

              <p className='big regular no-desktop'>We <span className='bold red'>always</span> put the client's needs first</p>
              <p className='big regular no-desktop'><span className='bold red'>Collaboration</span> across markets and service lines</p>
              <p className='big regular no-desktop'>Focused teamwork, <span className='bold red'>innovative thinking</span> and acting practically</p>
              <p className='big regular no-desktop'>Research led environment to ensure <span className='bold red'>competitive advantage</span></p>
              <p className='big regular no-desktop'><span className='bold red'>"Hands on"</span> senior management and executive</p>
              <p className='big regular no-desktop'>We do not accept any assignment that we do not feel <br /> comfortable to <span className='bold red'>deliver</span> on</p>
              <p className='big regular no-desktop'><span className='bold red'>ISO rated</span> property management <br /> and facilities management process and procedures</p>
            </div>
          </div>
        </div>
        <div className={cn(s.section2)}>
          <h3 className={ cn(s.title, 'semiBig m-0 red bold') }>Our Approach</h3>
          <h3 className={ cn(s.subTitle, 'semiBig regular m-0 mb-3') }>High-performance, respectful and dynamic.</h3>
          <div className={ cn(s.list, '') }>
            <div className={ cn(s.block, '') }>
              <div className={ cn(s.line, '') }></div>
              <div className={ cn(s.content, 'pl-2') }>
                <h4 className={ cn(s.title, 'bold mt-0 mb-1') }>Progressive, Cutting-edge Technologies.</h4>
                <p className={ cn(s.text, 'small light') }>We pride ourselves on our progressive and innovative culture, which is demonstrated through our incomparable operational efficiency at every level.</p>
                <p className={ cn(s.text, 'small light mb-0') }>Our industry leading patented technology platforms enhance and enrich asset values in the efforts of achieving sustainable real estate success.</p>
              </div>
            </div>
            <div className={ cn(s.block, '') }>
              <div className={ cn(s.line, '') }></div>
              <div className={ cn(s.content, 'pl-2') }>
                <h4 className={ cn(s.title, 'bold m-0 mb-1') }>End-to-end Real Estate Solutions.</h4>
                <p className={ cn(s.text, 'small light') }>We deliver strategic, fully-integrated, professional property services for both the investor and occupier markets. </p>
                <p className={ cn(s.text, 'small light mb-0') }>Our success is built on a ‘we know real estate’ culture, which stems from our tangible understanding of local markets across Africa.</p>
              </div>
            </div>
            <div className={ cn(s.block, '') }>
              <div className={ cn(s.line, '') }></div>
              <div className={ cn(s.content, 'pl-2') }>
                <h4 className={ cn(s.title, 'bold m-0 mb-1') }>Passionate and Skilled Professionals.</h4>
                <p className={ cn(s.text, 'small light') }>Our people are our assets. Their combined knowledge, unique expertise, unparalleled skills, integrity, loyalty and professionalism is our differentiator.</p>
                <p className={ cn(s.text, 'small light mb-0') }>We are passionate about long-standing relationships, internally and externally. </p>
              </div>
            </div>
          </div>
        </div>
        <div className={cn(s.section3, 'pb-2')}>
          <h3 className={ cn(s.title, 'semiBig m-0 red bold') }>Our Expansion Timeline</h3>
          <h3 className={ cn(s.subTitle, 'semiBig regular m-0 mb-3') }>Four decades of continued growth.</h3>
          <div className={ cn(s.sliderContainer, '') }>
            <Slider />
          </div>
        </div>
        <div className={cn(s.section4, ' pb-2')}>
          <h3 className={ cn(s.title, 'semiBig mb-4 red bold') }>Africa Expansion</h3>
          <div className={cn(s.content, '')}>
            <div className={cn(s.imageContainer, 'mt-2')}>
              <CloudImage
                src={ groupOverviewExpansion }
                alt="Broll"
                className="covered"
                responsive={{
                  desktop: { w: '1920' },
                }}
              />
            </div>
            <div className={cn(s.listContainer, 'ml-2')}>
              {
                expansionList.map(item => (
                  <div key={item.date} className={cn(s.itemContainer, '')}>
                    <div className={cn(s.date, 'mr-2')}>
                      <h2 className={ cn(s.title, 'm-0 small red') }>{item.date}</h2>
                    </div>
                    <div className={cn(s.country, 'pl-2')}>
                      <h3 className={ cn(s.title, 'ml-0 m-1 semiBig light') }>{item.country}</h3>
                    </div>
                  </div>
                ))
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
