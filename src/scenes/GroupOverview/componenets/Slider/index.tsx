import React, { useState } from "react";
import cn from 'classnames'

import Slider from "react-slick";
import Button from '../../../../components/Button'
import { useDeviceType } from "hooks";

import groupOverview1975 from "assets/pictures/groupOverview1975";
import groupOverview1995 from "assets/pictures/groupOverview1995";
import groupOverview2010 from "assets/pictures/groupOverview2010";
import groupOverview2018 from "assets/pictures/groupOverview2018";
import groupOverview2019 from "assets/pictures/groupOverview2019";
import groupOverview2022 from "assets/pictures/groupOverview2022";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import s from "./style.module.sass";


const sliderList = [
  {
    id: 0,
    date: "1975",
    text: "Company established with a high-quality, client need focused property management solution offering.",
    img: groupOverview1975,
  },
  {
    id: 1,
    date: "1995",
    text: "Retail leasing, Broking and Sales service lines added to the Group’s offering, broadening the range of services available to meet client’s needs.",
    img: groupOverview1995,
  },
  {
    id: 2,
    date: "2010",
    text: "Valuation and advisory, facilities management, occupier services and Intel/Research divisions were added to further contribute to the Group’s end-to-end real estate solution offering.",
    img: groupOverview2010,
  },
  {
    id: 3,
    date: "2018",
    text: "Auctions and sales service line added to business offering.",
    img: groupOverview2018,
  },
  {
    id: 4,
    date: "2019",
    text: "Reached Level 1 B-BBEE (SA) status with 51% Black Woman-Owned. Established Cushman and Wakefield affiliation with the occupier services division of Broll Property Group. Launched Internal Developers division focusing on workplace strategy.",
    img: groupOverview2019,
  },
  {
    id: 5,
    date: "2022",
    text: "Simultaneously launched our Business Solutions Specialists and Broll Risk Management service lines further contributing to our end-to-end real estate offering, covering risk mitigation and a variety of business solution offerings.",
    img: groupOverview2022,
  },
];

interface ArrowProps {
    index: number
    device: string
    onClick?: () => void;
}

const ArrowNext: React.FC<ArrowProps> = ({ index, device, onClick}) => {
    const maxLength = device !== 'mobile' ? sliderList.length - 2 : sliderList.length - 1
  
    return (
    <Button red={index !== maxLength} disabled={index === maxLength} className={ cn(s.arrow, s.next) } onClick={onClick}>
      <h4 className={cn('light m-0')}>Next</h4>
    </Button>
  );
}

const ArrowPrev: React.FC<ArrowProps> = ({ index, onClick }) => {
  return (
    <Button red={index !== 0} disabled={index === 0} className={ cn(s.arrow, s.prev) } onClick={onClick}>
      <h4 className={cn('light m-0')}>Previous</h4>
    </Button>
  );
}

const CustomPaging: React.FC<ArrowProps & {i: number}> = ({ i, index, device}) => {
  return (
    <div className={ cn(s.customPaging, `${i * 2 === index && device !== 'mobile' && s.slickActive} ${i === index && device === 'mobile' && s.slickActive}`) }
    ></div>
  );
}

function appendDots(dots: React.ReactNode) {
  return <div>{dots}</div>;
}

const SliderWrapper: React.FC = () => {
  const device = useDeviceType()
  const [index, setIndex] = useState(0);

  const itemClass = (id: number) => {
      let style
      if (device !== 'mobile') {
        style = id % 2 === 0 ? s.leftItem : s.rightItem
      }
      return style
  }

  return (
    <Slider
      slidesToShow={device === 'mobile' ? 1 : 2}
      slidesToScroll={device === 'mobile' ? 1 : 2}
      infinite={false}
      nextArrow={<ArrowNext index={index} device={device} />}
      prevArrow={<ArrowPrev index={index} device={device} />}
      dotsClass={`slick-dots slick-thumb ${s.slickDots} ${device !== 'mobile'? s.slickThumb : s.slickThumbMobile}`}
      dots={true}
      customPaging={(i) => <CustomPaging i={i} index={index} device={device} />}
      appendDots={appendDots}
      afterChange={(index) => setIndex(index)}
    >
      {sliderList.map((item) => (
        <div key={item.date}>
          <div
            className={ cn(s.itemContainer, itemClass(item.id)) }
          >
            <div className={ cn(s.textContainer, '') }>
                <h2 className={ cn(s.date, 'bold m-0 red') }>{item.date}</h2>
                <h4 className={ cn(s.text, 'semiBig light') }>{item.text}</h4>
            </div>
            <div className={ cn(s.imgBox, '') }>
                <img src={item.img} alt="" />
            </div>
          </div>
        </div>
      ))}
    </Slider>
  );
};

export default SliderWrapper;
