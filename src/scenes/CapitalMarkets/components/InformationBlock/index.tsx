import React from "react";
import { CloudImage } from "components";
import img from "../../../../assets/pictures/capitalMarkets";

import s from "./style.module.sass";
import cn from "classnames";

const topLeftList = [
  "Broll’s Capital Markets team sources buyers and sellers for South African and Sub Saharan real estate assets.",
  "We deal with clients who have an ongoing real estate requirement throughout the property investment lifecycle.",
  "We pride ourselves on undertaking mandates with great efficiency and have built a reputation for proactively identifying opportunities and first class execution.",
  "We combine our capital markets and leasing knowledge to provide proactive insights to unlock value and drive enhanced returns.",
  "Our key clients are; real estate investment trusts, developers, unlisted private funds, corporate investors, institutions and individual investors.",
];

const bottomList = [
  "Specialised advisors",
  "Exceptional Market Insight & Knowledge",
  "Innovative technology",
  "Real Estate Management",
  "Deal Sourcing",
  "Linking clients to available sources of capital",
  "Relationship Management",
  "World class reputation",
  "Top-Down Inferential Skills",
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
            Global multi-sector real estate expertise.
          </h3>
          <div className={cn(s.container, "")}>
            <div className={cn(s.left, "")}>
              <h3 className={cn(s.title, "semiBig bold red-4")}>
                Our Value Proposition
              </h3>
              {topLeftList.map((item) => (
                <p
                  className={cn(
                    s.text,
                    "white small light letterSpacingNormal mb-0"
                  )}
                >
                  {item}
                </p>
              ))}
            </div>
            <div className={cn(s.right)}>
              <CloudImage
                src={img}
                alt="Broll"
                className="covered"
                responsive={{
                  desktop: { w: "1920" },
                }}
              />
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-3")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-2")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {bottomList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
