import ArrowIcon from 'assets/icons/arrowBoldWhite.svg'
import React from 'react'


import { Banner, ContactBanner } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrollResearchPic from 'assets/pictures/brollResearch'
import ContactBannerPic from 'assets/pictures/contactBanner'

const topList = [
  'Successful retail starts with identifying the right location and creating the ideal tenant mix to suit a particular target market, at Broll this is what we can do for our clients.',
  'From conceptual phase to fully-functioning shopping centre with shops to let, Broll can add value every step of the way. In addition, we have a dedicated research department that enales us to advise and make informed recommendations in this ever-changing sector.',
]

const bottomList = [
  'We understands the dynamics of the retail environment, no one manages more shopping centres in Africa than Broll - that means a wealth of knowl edge to guide you.',
  'We can assist with feasibility studies, site assembly of land, coordinating the professional team and sourcing the right tenant mix.',
  'In addition to Greenfield projects, we are ideally suited to coordinate refur tbishments and provide input on the strategic market positioning of a particular retail centre.',
  'We are in contact with all of the major retailers and have the connections to act as letting agents for existing centres.'
]

const bannerData = {
  name: 'Theresa Terblanche',
  title: 'Divisional Director',
  img: ContactBannerPic,
  email: 'tterblanche@broll.com',
  number: '+27 87 700 8290'
}

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.section1 }>
          <div className={ s.left }>
            { topList.map((item, index) =>
              <React.Fragment key={ index }>
                <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>{ item }</p>
                <p className='light no-desktop m-0 letterSpacingNormal'>{ item }</p>
              </React.Fragment>
            )}
            <h3 className='semiBig m-0 red-2 bold mb-1'>Why choose Broll?</h3>
            { bottomList.map((item, index) =>
              <div key={ index } className={ s.item }>
                <div className={ s.icon }>
                  <img src={ ArrowIcon } alt="Broll" className='contained'/>
                </div>
                <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>{ item }</p>
                <p className='light no-desktop m-0 letterSpacingNormal'>{ item }</p>
              </div>
            )}
          </div>
          <div className={ s.right }>
            <Banner image={ BrollResearchPic } description='We understand <br/>the dynamics of <br/>this ever- <br/>changing sector.'/>
          </div>
        </div>
        <div className={ cn(s.section2, 'no-tablet no-mobile') }>
          <ContactBanner data={ bannerData } noLine/>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
