import React from "react";
import s from "./style.module.sass";
import cn from "classnames";

const leftList = [
  'Portfolio Management',
  'Lease Administration',
  'Vacancy/Sub-tenant Management',
  'Critical Date Monitoring',
  'Dilapidation Negotiations',
  'Estates Management',
  'Accounts Payable/Receivable',
  'Payment Reconciliations & Recovery',
  'Budgeting/Forecasting',
  'Cash Flow Planning']

  const rightList = [
    'Financial Reporting',
    'Data Management Solutions',
    'Location Analysis & Network Planning']

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <div className={cn(s.left, "p-4")}>
            <h3 className={cn(s.title, "semiBig light red-4")}>
              Our Value Proposition
            </h3>
            <p className={cn(s.text, "white small light no-mobile no-tablet letterSpacingNormal")}>
              We deliver integrated real estate portfolio services including
              advisory and transactions services, Estates management, data
              management and finance management for large Occupiers with
              numerous leasehold/freehold interests. <br /> Our well-established
              technology platforms, processes and skilled account teams ensure
              the seamless integration of multi-disciplinary real estate
              services. This allows us to support all our client`s business
              challenges and priorities linked to the property lifecycle. <br />
              We understand the end-to-end real estate management process and
              leverage this knowledge and expertise to ensure our clients`
              portfolios are fit for purpose, compliant and as efficient as
              possible.
            </p>
          </div>
          <div className={cn(s.right)}>
            <div className={cn(s.block, "pl-4 pr-4 pt-4 box-sizing")}>
              <div className={cn(s.titleBox)}>
                <h3 className={cn(s.title, "regular")}>Key Clients:</h3>
              </div>
            </div>
            <div className={cn(s.block, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.titleBox)}>
                <h3 className={cn(s.title, "regular")}>ABSA</h3>
              </div>
              <p className={cn(s.text, "small light no-mobile no-tablet letterSpacingNormal")}>
                Account Management, Strategy Management, Transaction Management,
                Estates Management, Finance Management and Data Management
              </p>
            </div>
            <div className={cn(s.block, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.titleBox)}>
                <h3 className={cn(s.title, "regular")}>Shell</h3>
              </div>
              <p className={cn(s.text, "small light no-mobile no-tablet letterSpacingNormal")}>
                Account Management, Strategy Management, Transaction Management,
                Estates Management, Finance Management and Data Management
              </p>
            </div>
            <div className={cn(s.block, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.titleBox)}>
                <h3 className={cn(s.title, "regular")}>Hollard</h3>
              </div>
              <p className={cn(s.text, "small light no-mobile no-tablet letterSpacingNormal")}>
                Account Management, Strategy Management, Transaction Management,
                Estates Management, Finance Management and Data Management
              </p>
            </div>
            <div className={cn(s.block, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.titleBox)}>
                <h3 className={cn(s.title, "regular")}>CHEP</h3>
              </div>
              <p className={cn(s.text, "small light no-mobile no-tablet letterSpacingNormal")}>
                Transaction Management and Account Management
              </p>
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-3")}>
          <h3 className={cn(s.title, "semiBig light red-4 ml-4 mt-0 mb-3")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
              { leftList.map(item =>
                <p key={ item } className='x-small regular no-mobile no-tablet'>{ item }</p>
              )}
              </div>
            </div>
            <div className={cn(s.right, "pl-3 pr-3 box-sizing")}>
            <div className={cn(s.list)}>
              { rightList.map(item =>
                <p key={ item } className='x-small regular no-mobile no-tablet'>{ item }</p>
              )}
              </div>
            </div>
          </div>
        </div>

        <div className={cn(s.section3, "grey-2 pb-4")}>
          <div className={cn(s.block, " box-sizing")}>
            <p className={cn(s.title, "light red-4 ml-2")}>
              Leases under management
            </p>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "regular m-0")}>19,000+</h2>
                <p className={cn(s.text, "small light")}>leases managed (SA)</p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <p className={cn(s.title, "light red-4 ml-2")}>
              Leases under management
            </p>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "regular m-0")}>600+</h2>
                <p className={cn(s.text, "small light")}>
                  leases managed (RoA)
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <p className={cn(s.title, "light red-4 ml-2")}>
              Leases under management
            </p>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "regular m-0")}>$183m</h2>
                <p className={cn(s.text, "small light")}>anual payments</p>
                <h2 className={cn(s.title, "regular m-0")}>$65m</h2>
                <p className={cn(s.text, "small light")}>anual receipts</p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <p className={cn(s.title, "light red-4 ml-2")}>Data Management</p>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <p className={cn(s.text, "small light m-0")}>Closure of</p>
                <h2 className={cn(s.title, "regular m-0")}>~8,600</h2>
                <p className={cn(s.text, "small light")}>data gaps</p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <p className={cn(s.title, "light red-4 ml-2")}>
              Strategy & Transaction
            </p>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "regular m-0")}>80+</h2>
                <p className={cn(s.text, "small light")}>
                  business cases per year
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <p className={cn(s.title, "light red-4 ml-2")}>
              Strategy & Transaction
            </p>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "regular m-0")}>1,000+</h2>
                <h2 className={cn(s.title, "regular m-0")}>300,000+</h2>
                <h2 className={cn(s.title, "regular m-0")}>m²</h2>
                <p className={cn(s.text, "small light")}>
                  deals concluded per year
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <p className={cn(s.title, "light red-4 ml-2")}>Data Management</p>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "regular m-0")}>$3.5bn</h2>
                <p className={cn(s.text, "small light")}>
                  assets under management
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <p className={cn(s.title, "light red-4 ml-2")}>
              Strategy & Transaction
            </p>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "regular m-0")}>~40%</h2>
                <p className={cn(s.text, "small light")}>
                  Our consolidation solutions can achieve cost savings and
                  reduce regional footprint by up to
                </p>
                <h2 className={cn(s.title, "regular m-0")}>75%</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
