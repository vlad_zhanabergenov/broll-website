import React from "react";
import CloudImage from "components/CloudImage";
import img from '../../../../assets/pictures/transactionServices'
import s from "./style.module.sass";
import cn from "classnames";

const topLeftList = [
  "The transaction services division specialises in the office, industrial, retail and data centre markets.  ",
  "This team assists users with new lease negotiations, purchases, disposals, renewals, lease restructures, subleasing and rental benchmarking aimed at achieving the most favourable outcomes, aligned with an organisation’s long-term strategy and workplace goals.",
  "We leverage the excellence of our local brokerage teams with the processes and efficiencies of our transaction management teams to deliver consistent results, easier client oversight and better control.",
  "With our unparalleled track record and experience we focus on every opportunity to reduce occupancy costs and improve flexibility across competitive and changing markets.",
];

const leftList = [
  "Brokerage",
  "Acquisitions & disposals",
  "Lease optimisation",
  "Renewals",
  "Sub-leasing",
  "Lease restructuring (re-gears)",
];

const rightList = [
  "Turnkey lease documentation completion",
  "Lease exit negotiation",
  "Portfolio optimisation",
  "Extensive market analysis",
  "Customised technology based workflow tracking",
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
            Skilled Negotiators. Excellent Service Delivery.
          </h3>
          <div className={s.container}>
            <div className={cn(s.left, "")}>
              <h3 className={cn(s.title, "semiBig bold red-4")}>
                Our Value Proposition
              </h3>
              {topLeftList.map((item) => (
                <p key={item} className={cn(s.text,"white small light letterSpacingNormal mb-0")}>
                  {item}
                </p>
              ))}
            </div>
            <div className={cn(s.right)}>
              <CloudImage
                src={img}
                alt="Broll"
                className="covered"
                responsive={{
                  desktop: { w: "1920" },
                }}
              />
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-4")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-1")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {leftList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
            <div className={cn(s.right, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {rightList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className={cn(s.section3, "grey-2 pb-4")}>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>
                  Value to fee ratio
                </p>
                <h2 className={cn(s.title, "small bold m-0")}>16:1</h2>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>
                  Deals concluded p/a
                </p>
                <h2 className={cn(s.title, "small bold m-0")}>300,000m²</h2>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>
                  Negotiated savings
                </p>
                <h2 className={cn(s.title, "small bold m-0")}>$63m</h2>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>
                  Largest brokerage network in
                </p>
                <h2 className={cn(s.title, "small bold m-0")}>Africa</h2>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0 mt-1")}>Research</h2>
                <p className={cn(s.text, "small light")}>based analytics</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
