import React from 'react'

import s from './style.module.sass'
import cn from 'classnames'

const data = [
  { id: 1, year: '2020', info: [
      { id: 1, title: 'Broll Property Group', description: 'Advisors and Consultants for Agency - Letting/sales - 1st place in Middle East – North Africa - Euromoney Real Estate Survey <br/>Advisors and Consultants for research - 1st place in Middle East – North Africa - Euromoney Real Estate Survey <br/>3rd overall in Middle East – North Africa - Euromoney Real Estate Survey <br/>PMR Gold Arrow Award - National Survey on External Facilities Management Companies' },
      { id: 2, title: 'Broll Facilities Management', description: 'PMR Gold Arrow Award - National Survey on External Facilities Management Companies' },
      { id: 3, title: 'Broll Nigeria', description: 'Advisors and Consultants - 1st place in Nigeria - Euromoney Real Estate Survey <br/>Advisors and Consultants for Agency - Letting/sales - 1st place in Nigeria - Euromoney Real Estate Survey <br/>Advisors and Consultants for Research - 1st place in Nigeria - Euromoney Real Estate Survey' },
    ],
  },
  { id: 2, year: '2019', info: [
      { id: 1, title: 'Broll Property Group', description: 'PMR Gold Arrow Award - National Survey on External Facilities Management Companies' },
      { id: 2, title: 'Broll Facilities Management', description: 'PMR Gold Arrow Award - National Survey on External Facilities Management Companies' },
      { id: 3, title: 'Broll Namibia', description: 'Great Place To Work - Small Business Category - 4th Place' },
    ],
  },
  { id: 3, year: '2018', info: [
      { id: 1, title: 'Broll Property Group', description: 'South Africa Facilities Management Young Achiever of the Year - Gold - Chanté Jordaan <br/>Kenya Facilities Management Young Achiever of the Year - Gold - Jacklyn Mwangi <br/>Woman\'s Property Network - Professional of the Year - Jess Cleland <br/>Admitted as an Advocater - Fathima Karolia <br/>Real Estate Service Company of the Year - Broll Nigeria <br/>Winners of the Redefine trip to Dubai and Abu Dhabi - Jaclyn Baiocchi, Marischen Stone, Ivo Nestel and Giles Balmer <br/>Top Leasing Brokers in Kwazulu-Natal - Winners of a Trip to the Northern Lights in Iceland - Anthon van Weers and Elizabeth Morgan <br/>Appointed as the Vice-President of the Black Business Council - Nkuli Bogopa' },
      { id: 2, title: 'Broll Namibia', description: 'Operational Excellence – 3rd Place - 12 Seasons Awards <br/>O&L Culture – 2nd Place - 12 Seasons Awards <br/>PMR Diamond Arrow Award – 1st Place' },
    ],
  },
  { id: 4, year: '2017', info: [
      { id: 1, title: 'Broll Property Group', description: 'PMR Silver Arrow Award - National Survey on External Facilities Management Companies' },
      { id: 2, title: 'Broll Namibia', description: 'Overall Best Performing Operating Companies - 3rd Place - 12 Seasons Awards <br/>Other Significant Achievements – 2nd Place - 12 Seasons Awards <br/>Agency Of The Year – 2nd Runner-up - FNB Realtors Club' },
    ],
  },
  { id: 5, year: '2016', info: [
      { id: 1, title: 'Broll Property Group', description: 'Advisors and Consultants –1st place in Africa - Euromoney Awards <br/>Advisors and Consultants for Agency/Letting – 1st place - Euromoney Awards <br/>Advisors and Consultants for Research – 1st place - Euromoney Awards' },
      { id: 2, title: 'Broll South Africa', description: 'Advisors and Consultants –1st place in Africa - Euromoney Awards <br/>Advisors and Consultants for Agency/Letting – 1st place - Euromoney Awards <br/>Advisors and Consultants for Research – 1st place - Euromoney Awards' },
      { id: 3, title: 'Broll Ghana', description: 'Real Estate Advisors and Consultants – 1st place - Euromoney Awards <br/>Advisors and Consultants for Agency/Letting – 1st place - Euromoney Awards <br/>Advisors and Consultants for Valuation – 1st place - Euromoney Awards <br/>Advisors and Consultants for Research – 1st place - Euromoney Awards <br/>Property Management Firm of the Year - Ghana Property Awards' },
      { id: 4, title: 'Broll Nigeria', description: 'Advisors and Consultants for Agency/Letting – 1st place - Euromoney Awards <br/>Advisors and Consultants for Research – 1st place - Euromoney Awards' },
      { id: 5, title: 'Broll Namibia', description: 'Financial Performance – 1st Place - 12 Seasons Awards <br/>Outstanding Performance – 3rd place - 12 Seasons Awards <br/>Diamond Arrow - PMR Award' },
    ],
  },
  { id: 6, year: '2015', info: [
      { id: 1, title: 'Broll South Africa', description: 'Advisors and Consultants for Agency/Letting –1st place in Africa - Euromoney Awards <br/>Consultants for Research –1st place in Africa - Euromoney Awards <br/>Real Estate Advisors and Consultants –1st place in Africa - Euromoney Awards' },
      { id: 2, title: 'Broll Ghana', description: 'Advisors and Consultants for Agency/Letting – 1st place - Euromoney Awards <br/>Advisors and Consultants for Research – 1st place - Euromoney Awards <br/>Property Management Company of the year 2015– Ghana Property Awards <br/>Real Estate Advisors and Consultants – 1st place - Euromoney Awards <br/>Best Valuation Firm <br/>Best Property Management Company <br/>Valuation Firm of the year 2015 – Ghana Property Awards' },
      { id: 3, title: 'Broll Namibia', description: 'PMR Golden Arrow Award – 1st place Property Management Company <br/>Top Performer overall winner award - 12 Seasons Awards <br/>Other Significant Achievements – 2nd Place - 12 Seasons Awards <br/>Financial Performance – 1st place - 12 Seasons Awards <br/>Creating Excellence Trust Relationship With Customers – 3rd place - 12 Seasons Awards <br/>Property Management Company – 1st Place - PMR Golden Awards' },
      { id: 4, title: 'Broll Nigeria', description: 'Advisors and Consultants for Agency/Letting – 1st place - Euromoney Awards <br/>Advisors and Consultants for Research – 1st place - Euromoney Awards <br/>Real Estate Advisors and Consultants – 1st place - Euromoney Awards' },
    ],
  },
  { id: 7, year: '2014', info: [
      { id: 1, title: 'Broll South Africa', description: 'PMR Golden Arrow Award – Bronze Facilities Excellence <br/>Real Estate Advisors and Consultants –1st place in Africa - Euromoney Award <br/>Real Estate Advisors and Consultants – Overall 1st place in Africa - Euromoney Award <br/>Real Estate Agency/Letting – 1st place in Africa - Euromoney Award <br/>Real Estate Valuation – 1st place in Africa - Euromoney Award <br/>Real Estate Research Services – 1st place in Africa - Euromoney Award' },
      { id: 2, title: 'Broll Ghana', description: 'Best Valuation Firm of the year - Ghana Property Awards <br/>Best Brokerage Firm of the year - Ghana Property Awards <br/>Commercial Property Management Award – Ghana Property Awards <br/>Valuation Firm of the Year <br/>Brokerage Company of the Year' },
      { id: 3, title: 'Broll Namibia', description: 'PMR Golden Arrow Award – 1st place Property Management Company <br/>Trust Relationship with customers overall winner award - 12 Seasons Awards' },
      { id: 4, title: 'Broll Nigeria', description: 'Real Estate Advisors and Consultants – 1st place in Africa - Euromoney Award <br/>Best Real Estate Support Service Company – Real Estate Unite Awards <br/>Best Commercial Property: Ikeja City Mall – Real Estate unite Awards - Broll managed' }
    ],
  },
  { id: 8, year: '2013', info: [
      { id: 1, title: 'Broll South Africa', description: 'PMR Golden Arrow Award - Gold for Facilities Excellence' },
      { id: 2, title: 'Broll Ghana', description: 'Property Management Company of the Year' },
      { id: 3, title: 'Broll Namibia', description: 'PMR Golden Arrow Award - 1st place Property Management Company' },
    ],
  },
  { id: 9, year: '2012', info: [
      { id: 1, title: 'Broll Ghana', description: 'Facilities Management Award' },
      { id: 2, title: 'Broll Namibia', description: 'Top Performer overall winner award - 12 Seasons Awards' },
    ],
  },
  { id: 10, year: '2011', info: [
      { id: 1, title: 'Broll Ghana', description: 'Facilities Management and Maintenance Award' },
      { id: 2, title: 'Broll Namibia', description: 'Top Performer overall winner award - 12 Seasons Awards' },
    ],
  },
  { id: 11, year: '2010', info: [
      { id: 1, title: 'Broll Ghana', description: 'Facilities Management and Maintenance Award' },
      { id: 2, title: 'Broll Namibia', description: 'Commercial Building Management Award <br/>Financial Performance overall winner award - 12 Seasons Awards' },
    ],
  },
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        { data.map(item =>
          <div className={ s.item } key={ item.id }>
            <div className={ s.heading }>
              <div className={ s.line }/>
              <h4 className='m-0'>{ item.year }</h4>
            </div>
            { item.info.map(i =>
              <div className={ s.i } key={ i.id }>
                <div className={ s.left }>
                  <p className='large bold m-0 red-2 letterSpacingNormal'>{ i.title }</p>
                </div>
                <div className={ s.right }>
                  <p className='small light letterSpacingNormal m-0 no-tablet no-mobile' dangerouslySetInnerHTML={{ __html: i.description }}/>
                  <p className='light letterSpacingNormal m-0 no-desktop no-mobile'>{ i.description.replace(/<br\/>/gi, '') }</p>
                  <p className='light small letterSpacingNormal m-0 no-desktop no-tablet'>{ i.description.replace(/<br\/>/gi, '') }</p>
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  )
}

export default InformationBlock
