import React, { useState } from 'react'

import { CountryFilter, DownloadModal, Query, Results, GroupResults } from './components'

import s from './style.module.sass'
import cn from 'classnames'

export type Form = {
  query: string
  country: string
}

interface Props {
  values: Form
  change: (data: { name: string, value: any }) => void
  onReset: () => void
}

const InformationBlock: React.FC<Props> = ({ values, change, onReset }) => {
  const [publicationItem, setPublicationItem] = useState<PublicationItem | null>(null)

  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.left }>
          <div className={ s.top }>
            <Query value={ values.query } onChange={ change }/>
          </div>
          <div className={ s.bottom }>
            <div className={ s.header }>
              <div className={ s.line }/>
              <h4 className='m-0 bold'>Group Publications</h4>
            </div>
            <div className={ cn(s.list, 'scrollbar-hidden') }>
              <GroupResults onReset={ onReset } onClick={ (i) => setPublicationItem(i) } query={ values.query }/>
            </div>
          </div>
        </div>
        <div className={ s.right }>
          <div className={ s.top }>
            <CountryFilter value={ values.country } onChange={ change }/>
          </div>
          <div className={ s.bottom }>
            <div className={ s.header }>
              <div className={ s.line }/>
              <h4 className='m-0 bold'>{ values.country } Publications</h4>
            </div>
            <div className={ cn(s.list, 'scrollbar-hidden') }>
              <Results onReset={ onReset } onClick={ (i) => setPublicationItem(i) }/>
            </div>
          </div>
        </div>
      </div>
      { publicationItem &&
        <DownloadModal onClose={ () => setPublicationItem(null) } downloadUrl={ publicationItem.downloadUrl }/>
      }
    </div>
  )
}

export default InformationBlock
