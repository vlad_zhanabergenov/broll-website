import React, { useEffect, useState } from 'react'
import { useReactiveVar } from '@apollo/client'
import { appVar } from 'cache/vars'
import { HitsProvided } from 'react-instantsearch-core'
import { connectHits } from 'react-instantsearch-dom'
import parsePublicationItem from 'services/parsePublicationItem'

import { Icon } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrow.svg'

interface Props {
  onReset: () => void
  onClick: (item: PublicationItem) => void
}

const Results: React.FC<Props & HitsProvided<any>> = ({ onReset, onClick, hits }) => {
  const { algoliaReady } = useReactiveVar(appVar)
  const [list, setList] = useState<PublicationItem[]>([])

  useEffect(() => {
    setList(hits.map(i => parsePublicationItem(i.fields)))
  }, [hits])

  if (!algoliaReady) {
    return null
  }

  return (
    <div className={ cn(s.Results) }>
      <div className={ s.layout }>
        { hits.length ?
          <div className={ s.list }>
            { list.map(item =>
              <div className={ s.item } key={ item.id }>
                <div className={ cn(s.icon, 'cursor-pointer') } onClick={ () => onClick(item) }>
                  <Icon src={ ArrowIcon } rotate={ 90 } size='xs'/>
                </div>
                <p className='small light m-0 no-tablet no-mobile letterSpacingNormal cursor-pointer' onClick={ () => onClick(item) }>{ item.title }</p>
                <p className='light m-0 no-desktop letterSpacingNormal cursor-pointer' onClick={ () => onClick(item) }>{ item.title }</p>
              </div>
            )}
          </div>
        :
          <h3 className="medium mv-10 text-center">Your search returned no results. Please <span className='cursor-pointer bold red' onClick={ onReset }>reset</span> your filter and try again.</h3>
        }
      </div>
    </div>
  )
}

export default connectHits(Results)
