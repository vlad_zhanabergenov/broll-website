import { useForm } from 'hooks'
import { pickBy } from 'lodash'
import React, { useState } from 'react'
import ReCAPTCHA from 'react-google-recaptcha'

import { Button, Input, Modal, Link } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  onClose: () => void
  downloadUrl: string
}

interface Form {
  name: string
  email: string
}

const SearchFiltersModal: React.FC<Props> = ({ onClose, downloadUrl }) => {
  const [loading, setLoading] = useState(false)
  const [message, setMessage] = useState('')
  const [captcha, setCaptcha] = useState<string | null>(null)


  const handleCaptcha = (token: string | null) => {
    setCaptcha(token)
  }

  const handleSubmit = (_values: Form, valid: boolean, errors: { [key in keyof Form]: string | null }) => {
    if (valid) {
      setLoading(true)
      window.open(downloadUrl, "_blank" )
      onClose()
    } else {
      const errorsList = Object.values(pickBy(errors))
      const firstError = errorsList.includes('Field is required') ? 'Field is required' : errorsList[0]

      if (firstError) {
        const text = firstError === 'Field is required' ? "Please fill the required fields." : firstError
        setMessage(text)
      }
    }
  }

  const { values, change, submit } = useForm<Form>({
    fields: {
      name: { initialValue: '', required: true, validator: 'min2' },
      email: { initialValue: '', required: true, validator: 'email' },
    }, handleSubmit
  })

  return (
    <Modal onClose={ onClose } secondary>
      <div className={ s.DownloadModal }>
        <div className={ s.layout }>
          <h3 className='red-2 semiBig m-0 mb-2'>Document Download Form</h3>
          <div className={ s.fields }>
            <Input secondary name="name" placeholder="Full Name"
                   onChange={ change } required className='mb-05' value={ values.name } noBorder/>
            <Input secondary name="email" placeholder="Email Address"
                   onChange={ change } required value={ values.email } noBorder/>
            <p className={ cn({ 'no-pointer-events no-select opacity-0': !message }) }>{ message || "_" }</p>
          </div>
          <div className={ cn(s.captcha, 'mb-1') }>
            <ReCAPTCHA
              sitekey={ process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY || '' }
              onChange={ handleCaptcha }
            />
          </div>
          <h4 className='light letterSpacingNormal m-0 mb-2'>
            We will communicate real estate related marketing information and related
            services. We respect your privacy. See our <Link to='/privacy'>Privacy Policy</Link>
          </h4>
          <div className={ cn(s.button) }>
            <Button onClick={ submit } loading={ loading } disabled={ !captcha } darkBackground full>Download</Button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default SearchFiltersModal
