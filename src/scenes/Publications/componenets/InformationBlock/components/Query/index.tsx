import { useForm } from 'hooks'
import _ from 'lodash'
import React, { useEffect } from 'react'
import { SearchBoxProvided } from 'react-instantsearch-core'
import { connectSearchBox } from 'react-instantsearch-dom'

import { Input } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  value: string
  onChange: (data: { name: string, value: string }) => void
}

type Form = {
  query: string
}

const QueryRaw: React.FC<Props & SearchBoxProvided> = ({
  value, onChange,
  currentRefinement, refine
}) => {
  useEffect(() => {
    if (!_.isEqual(currentRefinement, value)) {
      refine(value)
    }
  }, [value])

  const { values, change } = useForm<Form>({
    fields: {
      query: { initialValue: value }
    },
    onChange: (name, value) => {
      onChange({ name, value })
    }
  }, [value])

  return (
    <div className={ cn(s.Query) }>
      <Input name="query" type="search" label="Filter by Keyword:" labelDark value={ values.query } onChange={ change }
             textBeforeSmall="Search" placeholder="eg: Finance" noAutofill withFullFillIndicator textBeforeRight/>
    </div>
  )
}

const QueryConnected = connectSearchBox(QueryRaw)

const Query: React.FC<Props> = (props) => {
  return (
    <QueryConnected { ...props }/>
  )
}

export default Query
