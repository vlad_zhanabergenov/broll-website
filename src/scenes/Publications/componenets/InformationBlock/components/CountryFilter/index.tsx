import React, { useEffect } from 'react'
import { useForm } from 'hooks'
import _ from 'lodash'
import { MenuProvided } from 'react-instantsearch-core'
import { connectMenu } from 'react-instantsearch-dom'

import { Select } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  value: string
  onChange: (data: { name: string, value: string }) => void
}

type Form = {
  country: string
}

type OptionHit = {
  count: number
  isRefined: boolean
  label: string
  value: string
}

const CountryFilterRaw: React.FC<Props & MenuProvided> = ({
  value, onChange, currentRefinement, refine,
  items
}) => {
  const sortedItems = _.sortBy(items, ['-count'])
  const countries = sortedItems.map(item => item.label)

  useEffect(() => {
    if (currentRefinement !== value) {
      refine(value)
    }
  }, [value])

  const { values, change } = useForm<Form>({
    fields: {
      country: { initialValue: value }
    },
    onChange: (name, value) => {
      onChange({ name, value })
    }
  }, [value])

  return (
    <div className={ cn(s.CountryFilter) }>
      <Select name="country" options={ countries } value={ values.country }
              onChange={ change } label="Filter by Country:" labelDark fontLarge/>
    </div>
  )
}

const CountryFilterConnected = connectMenu(CountryFilterRaw)

const removeGroupOption = (items: OptionHit[]) => items.filter(item => item.value !== 'Group')

const CountryFilter: React.FC<Props> = (props) => (
  <CountryFilterConnected transformItems={removeGroupOption} attribute='fields.country.en-US' { ...props } limit={ 1000 }/>
)

export default CountryFilter
