import { useForm } from 'hooks'
import React from 'react'
import { Configure, Index } from 'react-instantsearch-core'

import { Breadcrumbs, SearchBar } from 'components'
import { InformationBlock } from './componenets'

import { Form } from './componenets/InformationBlock'

import s from './style.module.sass'
import cn from 'classnames'

const PublicationsRaw = () => {
  const { values, change, defaultForm } = useForm<Form>({
    fields: {
      query: { initialValue: "" },
      country: { initialValue: "South Africa" }
    }
  })

  const handleReset = () => {
    defaultForm({ query: "", country: "South Africa" })
  }

  return (
    <div className={ s.PublicationsScene }>
      <div className={ s.layout }>
        <div className={ cn(s.section1, 'pt-2 ph-17 pb-4  box-sizing') }>
          <Breadcrumbs items={ ['Media Centre', 'Publications'] } className="mb-2"/>
          <div className={ cn(s.searchBar, 'pl-1 pr-3 box-sizing') }>
            <SearchBar/>
          </div>
        </div>
        <div className={ cn(s.section2, 'pl-14 pr-12 pb-4 box-sizing') }>
          <h2 className={ cn('thin m-0 mb-3') }>Publications</h2>
          <InformationBlock values={ values } change={ change } onReset={ handleReset }/>
        </div>
      </div>
    </div>
  )
}

const PublicationsScene: React.FC = () => {
  return (
    <Index indexName="Publication" indexId="publications_full">
      <Configure hitsPerPage={ 1000 } distinct={ 1 } />
      <PublicationsRaw/>
    </Index>
  )
}

export default PublicationsScene
