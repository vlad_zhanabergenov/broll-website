import { useRouter } from 'next/router'
import React, { useEffect, useRef, useState } from 'react'
import { sortBy, startCase } from 'lodash'
import slugParser from 'services/slugParser'

import {
  ContactForm,
  CounterButton,
  ImageCarousel,
  NavigationButtons,
  Accordion,
  UnitsList,
  RelatedProperties,
  InformationBlock,
  Gallery
} from './components'
import { CloudImage, Breadcrumbs, Disclaimer, HeroSearch, SpecialOffer, Mission, Link } from 'components'
import { Props as BreadcrumbsProps } from 'components/Breadcrumbs'

import NoPropertyImage from 'assets/pictures/noPropertyImage'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  propertyList: AnyProperty[]
  relatedProperties?: AnyProperty[]
  isUnit?: boolean
}

const descriptionLinesLimit = 8

const Property: React.FC<Props> = ({
  isUnit,
  propertyList,
  relatedProperties
}) => {
  const router = useRouter()

  const item = isUnit ? propertyList.find(i => slugParser.parse('unit_slug' in i ? i.unit_slug! : i.property_slug!).unit_id === router.query.property?.[5])! : propertyList[0]
  const itemImages = isUnit && 'unit_images' in item ? (item.unit_images || []) : (item.property_images || [])
  const specialDeals = (propertyList.filter(i => i.dealType === 'toLet' && i.special_deals !== null) as PropertyToLet[]).map(i => ({
    unit_id: i.unit_id,
    text: i.special_deals
  }))
  const unitSpecialDeal = specialDeals.find(i => i.unit_id === item.unit_id)?.text || null

  const [descriptionHasShowMore, setDescriptionHasShowMore] = useState(false)
  const [descriptionShowMore, setDescriptionShowMore] = useState(false)
  const descriptionRef = useRef<HTMLParagraphElement>(null)
  const descriptionGhostRef = useRef<HTMLParagraphElement>(null)

  useEffect(() => {
    const el = descriptionRef.current
    const ghost = descriptionGhostRef.current

    const getLinesCount = (el: HTMLParagraphElement) => {
      const elHeight = el.offsetHeight
      const lineHeight = parseInt(getComputedStyle(el).getPropertyValue('line-height'))
      return elHeight / lineHeight
    }

    if (el && ghost) {
      if (getLinesCount(el) < getLinesCount(ghost)) {
        setDescriptionHasShowMore(true)
      }
    }
  }, [])

  const scrollToPropertyContactForm = () => {
    const boundingClientRect = document.getElementById('propertyContactForm')?.offsetTop
    boundingClientRect && window.scrollTo({ top: boundingClientRect, behavior: 'smooth' })
  }

  const getBreadcrumbsMap = (): BreadcrumbsProps['items'] => {
    let map: BreadcrumbsProps['items'] = [
      [`${ startCase(item.dealType) }`, `/search?deal=${ startCase(item.dealType).replace( /\\s/g, ' ') }`],
      [item.property_category!, `&category=${ item.property_category }`],
      [item.city!, `&city=${ item.city }`],
      [item.suburb!, `&suburb=${ item.suburb }`],
      item.property_name!
    ]

    if (!isUnit) {
      return [
        map[0],
        [map[1][0], `${ map[0][1] }${ map[1][1] }`],
        [map[2][0], `${ map[0][1] }${ map[1][1] }${ map[2][1] }`],
        [map[3][0], `${ map[0][1] }${ map[1][1] }${ map[2][1] }${ map[3][1] }`],
        map[4]
      ]
    } else {
      map[1] = [item.unit_category! || item.property_category!, `&category=${ item.unit_category || item.property_category }`]
      map[4] = [item.property_name!, { to: 'property', data: { property: item } }]
      map[5] = item.unit_id!

      return [
        map[0],
        [map[1][0], `${ map[0][1] }${ map[1][1] }`],
        [map[2][0], `${ map[0][1] }${ map[1][1] }${ map[2][1] }`],
        [map[3][0], `${ map[0][1] }${ map[1][1] }${ map[2][1] }${ map[3][1] }`],
        map[4],
        map[5]
      ]
    }
  }

  return (
    <div className={ s.Property }>
      <div className={ s.layout }>
        <div className={ cn(s.section1, 'pt-2 pl-19 pr-17 box-sizing') }>
          <Breadcrumbs items={ getBreadcrumbsMap() } className="mb-2"/>
          <HeroSearch theme='dark' mobileFullOnly/>
        </div>
        <div className={ cn(s.section2, 'ph-15 pt-6 box-sizing') }>
          <div className={ cn(s.top) }>
            <div className={ cn(s.left, 'pt-2 pb-6 box-sizing') }>
              <div className={ cn(s.wrapper, 'box-sizing') }>
                <CounterButton onClick={ scrollToPropertyContactForm }/>
                <div className={ s.overview }>
                  <h2 className={ cn('thin mt-3', isUnit ? 'mb-05' : 'mb-2') }>
                    { isUnit ? item.unit_id : item.property_name }
                  </h2>
                  <h4 className='regular m-0'>
                    { isUnit ? <Link to='property' data={{ property: item }}>{ item.property_name }</Link> : item.street_address }
                  </h4>
                  { isUnit && unitSpecialDeal &&
                    <div className={ s.unitSpecialOffer }>
                      <p className='bold red uppercase m-0'>Special offer:</p>
                      <p className='bold small m-0'>{ unitSpecialDeal }</p>
                    </div>
                  }
                  <div className={ cn(s.description, 'mv-1 pr-2 box-sizing') }>
                    <p ref={ descriptionRef } className={ cn('light small m-0', { [`limit-string-${ descriptionLinesLimit }`]: !descriptionShowMore }) }>
                      { item.marketing?.property_marketing_description || "No description provided" }
                    </p>
                    { descriptionHasShowMore &&
                      <span className={ cn(s.showMore, 'light small underline cursor-pointer') } onClick={ () => setDescriptionShowMore(!descriptionShowMore) }>
                        { descriptionShowMore ? "show less" : "show more" }
                      </span>
                    }
                    <p ref={ descriptionGhostRef } className={ cn(s.ghost, 'light small m-0 opacity-0 no-pointer-events no-user-select') }>
                      { item.marketing?.property_marketing_description || "No description provided" }
                    </p>
                  </div>
                </div>
              </div>
              { item.dealType === 'toLet' && isUnit && propertyList.length > 1 ?
                  <div className={ cn(s.navigation, 'box-sizing') }>
                    <NavigationButtons properties={ propertyList } isUnit={ item.unit_id }/>
                  </div>
                : !!relatedProperties?.length &&
                  <div className={ cn(s.navigation, 'box-sizing') }>
                    <NavigationButtons properties={ relatedProperties }/>
                  </div>
              }
            </div>
            <div className={ s.right }>
              { !isUnit && itemImages.length ?
                <ImageCarousel
                  images={ itemImages }
                  video={ item.dealType === 'toLet' ? item.video : 'property_video' in item ? item.property_video : undefined }
                />
              :
                <CloudImage
                  className='covered'
                  src={ item.best_image || 'unit_images' in item && item.unit_images?.[0]?.image_path_url  || NoPropertyImage }
                  alt={ item.property_name }
                  responsive={{
                    desktopLarge: { w: '800', ar: '1' },
                    desktop: { w: '600', ar: '1' },
                    tablet: { w: '500', ar: '1' },
                    mobile: { w: '400', ar: '1' }
                  }}
                />
              }
              { ((!isUnit && specialDeals.length) || (isUnit && unitSpecialDeal)) &&
                <SpecialOffer className={ cn(s.special) }/>
              }
            </div>
          </div>
          <div className={ cn(s.bottom, 'pr-3 box-sizing') }>
            { !isUnit ?
              <Accordion
                map={{ latitude: item.latitude, longitude: item.longitude }}
                onClickToContactForm={ scrollToPropertyContactForm }
                property={{
                  ...item,
                  min_gla: sortBy(propertyList, ['min_gla'])[0].min_gla,
                  max_gla: sortBy(propertyList, ['max_gla'])[propertyList.length - 1].max_gla
                }}
                unitsCount={ propertyList.length }
              />
            :
              <InformationBlock property={ item }/>
            }
          </div>
        </div>
        { !isUnit && item.dealType !== 'forSale' ?
          <div className={ cn(s.section3, 'pv-4 pl-14 pr-16 box-sizing') }>
            <UnitsList list={ propertyList }/>
          </div>
        :
          <div className={ cn(s.section4, 'pv-4 ph-15 box-sizing') }>
            <Gallery
              images={ itemImages }
              video={ item.video }
              map={ isUnit ? { latitude: item.latitude, longitude: item.longitude } : undefined }
            />
          </div>
        }
        <div className={ cn(s.section4, 'pv-4 ph-16 box-sizing') } id='propertyContactForm'>
          <ContactForm
            people={ isUnit && 'unit_responsibility' in item ? item.unit_responsibility : item.property_responsibility }
            dealType={ item.dealType }
            property={ item }
            isUnit={isUnit}
          />
        </div>
        <div className={ cn(s.section5, 'mb-2') }>
          { !!relatedProperties?.length &&
            <RelatedProperties list={ relatedProperties }/>
          }
        </div>
        { isUnit &&
          <div className={ cn(s.section6) }>
            <Mission/>
          </div>
        }
      </div>
      <Disclaimer/>
    </div>
  )
}

export default Property
