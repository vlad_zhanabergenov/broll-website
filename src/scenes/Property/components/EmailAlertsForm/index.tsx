import React, { useEffect, useState } from 'react'
import { useForm } from 'hooks'
import { useForm as useFormspree } from '@formspree/react'

import { Button, Input } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Form {
  name: string
  email: string
}

const EmailAlertsForm: React.FC = () => {
  const [loading, setLoading] = useState(false)
  const [message, setMessage] = useState('')

  const [formspreeState, handleFormspreeSubmit] = useFormspree(process.env['NEXT_PUBLIC_FORMSPREE_EMAIL_ALERTS_ID']!)

  useEffect(() => {
    if (formspreeState.succeeded) {
      setLoading(false)
      change({ name: 'email', value: '' })
      change({ name: 'name', value: '' })
      setMessage("Your message has been sent successfully.")
    } else if (formspreeState.errors.length) {
      setLoading(false)
      setMessage("Something went wrong, please try again later.")
    }
  }, [formspreeState])

  const handleSubmit = (values: Form, valid: boolean) => {
    if (valid) {
      setLoading(true)
      handleFormspreeSubmit(values)
    }
  }

  const { values, errors, change, submit } = useForm<Form>({
    fields: {
      name: { initialValue: '', required: true },
      email: { initialValue: '', validator: 'email', required: true }
    }, handleSubmit
  })

  return (
    <div className={ cn(s.EmailAlertsForm) }>
      <div className={ cn(s.left, 'box-sizing pr-1') }>
        <h3 className='big white regular mt-0 mb-05'>Get Email Alerts</h3>
        <p className={ cn(s.text, 'light white m-0') }>
          Sign-up and receive property email alerts for offices to let in Bryanston.
        </p>
      </div>
      <div className={ cn(s.right, 'box-sizing pl-1') }>
        <Input
          name='name'
          placeholder='Name'
          value={ values.name }
          onChange={ change }
          onPressEnter={ submit }
          error={ errors.name }
          message={ message }
          className='mb-05'
        />
        <Input
          name='email'
          placeholder='Email Address'
          value={ values.email }
          onChange={ change }
          onPressEnter={ submit }
          error={ errors.email }
          message={ message }
          className='mb-1'
        />
        <Button className={ cn(s.button) } secondary white onClick={ submit } loading={ loading }>Submit</Button>
      </div>
    </div>
  )
}

export default EmailAlertsForm
