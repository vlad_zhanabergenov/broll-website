import React, { useState } from 'react'
import formatNumberWith from 'services/formatNumberWith'

import { CloudImage, Icon, Link } from 'components'

import cn from 'classnames'
import ArrowIcon from 'assets/icons/arrow.svg'
import NoPropertyImage from 'assets/pictures/noPropertyImage'

import s from './style.module.sass'

interface Props {
  list: AnyProperty[]
}

const maxUnitsToShow = 6

const UnitsList: React.FC<Props> = ({ list }) => {
  const [limit, setLimit] = useState(true)
  const units = list.slice(0, limit ? maxUnitsToShow : list.length)

  return (
    <div className={ cn(s.UnitsList) }>
      <div className={ s.layout }>
        <h3 className='black bold'>Units Available</h3>
        <div className={ s.units }>
          { units.map((item, num) => <div key={ `${ item.objectID }-${ num }` } className={ cn(s.unit, 'pv-1 box-sizing') }>
              <div className={ cn(s.wrapper, { [s.wrapperSpecial]: 'special_deals' in item && !!item.special_deals }) }>
                <div className={ cn(s.line, { [s.lineSpecial]: 'special_deals' in item && !!item.special_deals }) }/>
                <div className={ s.img }>
                  { ('unit_images' in item && item.unit_images?.length) ?
                      <CloudImage
                        className='covered'
                        src={ item.unit_images[0].image_path_url || NoPropertyImage }
                        alt={ `${ item.property_name }, ${ item.marketing?.unit_marketing_heading }` }
                        responsive={{
                          desktopLarge: { w: '300', ar: '1.1' },
                          desktop: { w: '200', ar: '1.1' },
                          tablet: { w: '180', ar: '1.1' },
                          mobile: { w: '120', ar: '1.1' }
                        }}
                      />
                    :
                      <CloudImage
                        className='covered'
                        src={ item.best_image || NoPropertyImage }
                        alt={ `${ item.property_name }, ${ item.marketing?.unit_marketing_heading }` }
                        responsive={{
                          desktopLarge: { w: '300', ar: '1.1' },
                          desktop: { w: '200', ar: '1.1' },
                          tablet: { w: '180', ar: '1.1' },
                          mobile: { w: '120', ar: '1.1' }
                        }}
                      />
                  }
                  <p className={ cn(s.specialOffer, 'uppercase small lightRed bold mv-0') }>
                    { 'special_deals' in item && !!item.special_deals && 'Special offer' }
                  </p>
                </div>
                <div className={ cn(s.data) }>
                  <div className={ cn(s.col, 'pr-05 box-sizing') }>
                    <h4 className='black medium limit-string-1 mb-05'>Unit</h4>
                    <h4 className='greyBlue-3 light limit-string-1 mt-1 mb-05'>{ item.unit_id }</h4>
                  </div>
                  <div className={ cn(s.col, 'pr-05 box-sizing') }>
                    <h4 className='black medium limit-string-1 mb-05'>Price</h4>
                    <h4 className='greyBlue-3 light limit-string-1 mt-1 mb-05'>
                      { !item.gross_price ?
                          "POA"
                        :
                          `R${ formatNumberWith(" ", item.gross_price) }/m²`
                      }
                    </h4>
                  </div>
                  <div className={ cn(s.col, 'pr-05 box-sizing') }>
                    <h4 className='black medium limit-string-1 mb-05'>GLA</h4>
                    <h4 className='greyBlue-3 light limit-string-1 mt-1 mb-05'>
                      { `${ formatNumberWith(' ', item.max_gla || 0) }m²` }
                    </h4>
                  </div>
                </div>
                <div className={ s.actions }>
                  <Link to='property' data={{ isUnit: true, property: item }} className={ s.view }>
                    <p className='medium x-small red-2 uppercase mv-0 mr-05 no-mobile'>view</p>
                    <div className={ s.icon }>
                      <Icon src={ ArrowIcon } rotate={ 90 } size='xs'/>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          )}
        </div>
        { (limit && list.length > maxUnitsToShow) &&
          <div className={ cn(s.loadMore, 'pt-2 pb-1 box-sizing') }>
            <h4 className="bold uppercase cursor-pointer text-center m-0" onClick={ () => setLimit(false) }>
             + load more
            </h4>
          </div>
        }
      </div>
    </div>
  )
}

export default UnitsList
