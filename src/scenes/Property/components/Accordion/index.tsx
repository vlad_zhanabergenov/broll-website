import React, { useState } from 'react'
import formatNumberWith from 'services/formatNumberWith'
import cn from 'classnames'

import { Icon, Button } from 'components'
import { GoogleMap, PDFDownloadButton } from '../index'

import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrow.svg'
import ShareIcon from 'assets/icons/share.png'
import DownloadIcon from 'assets/icons/download.png'
import ContactIcon from 'assets/icons/contact.png'


type AccordionItem = {
  id: string
  name: string
  type: 'info' | 'location' | 'contact' | 'share'
}

type ShareItem = 'pdf' | 'email' | 'share'

interface IProps {
  map?: {
    latitude?: number
    longitude?: number
  }
  onClickToContactForm: () => void
  property: AnyProperty
  unitsCount: number
}

const items: AccordionItem[] = [
  {
    id: "01",
    name: "Information",
    type: 'info'
  },
  {
    id: "02",
    name: "Location",
    type: 'location',
  },
  {
    id: "03",
    name: "Contact",
    type: 'contact',
  },
  {
    id: "04",
    name: "Share",
    type: 'share'
  }
]

const Accordion: React.FC<IProps> = ({
  onClickToContactForm,
  map,
  property,
  unitsCount
}) => {
  const [current, setCurrent] = useState(items[0].id)
  const [shareType, setShareType] = useState<ShareItem>('pdf')

  const handleChange = (id: AccordionItem['id']) => {
    setCurrent(id)
  }

  const handleShareChange = (item: ShareItem) => {
    setShareType(item)
  }

  const handleEmailClick = () => {
    const subject = 'Have a look at this property online'
    const body = location.href
    window.open(`mailto:?subject=${ subject }&body=${ body }`)
  }

  const handleShareClick = () => {
    const template = document.createElement('input')
    const currentLink = location.href
    document.body.appendChild(template)
    template.value = currentLink
    template.select()
    document.execCommand('copy')
    document.body.removeChild(template)
  }

  const pdfItem: PDFItem = property.dealType === 'toLet' ? 'propertyToLet' : 'propertyForSale'

  const shareTypeToProps: Record<ShareItem, any> = {
    pdf: {
      icon: DownloadIcon,
      content: (
        <>
          <p className='small light no-pointer-events grey text-center mv-05'>
            Click the button below to download the { property.property_name } brochure.
          </p>
          <PDFDownloadButton data={ [property] } item={ pdfItem } className={ s.shareButton }/>
        </>
      )
    },
    email: {
      icon: ShareIcon,
      content: (
        <>
          <p className='small light no-pointer-events grey text-center mv-05'>
            Click the button below to send an email with the page link.
          </p>
          <Button onClick={ handleEmailClick } className={ cn(s.shareButton) } rounded>Send email</Button>
        </>
      )
    },
    share: {
      icon: ShareIcon,
      content: (
        <>
          <p className='small light no-pointer-events grey text-center mv-05'>
            Click the button below to copy the above link to share this page.
          </p>
          <Button onClick={ handleShareClick } className={ cn(s.shareButton) } rounded>Copy link</Button>
        </>
      )
    }
  }

  return (
    <div className={ cn(s.Accordion) }>
      <div className={ s.layout }>
        { items.map(i =>
          <div key={ i.id } className={ cn(s.item, { [s.expanded]: i.id === current }) }>
            <div className={ cn(s.layout, "pv-2 box-sizing") }>
              <div className={ cn(s.top, "no-tablet no-mobile ml-2") }>
                <p className="red-2 mt-0 mb-05">{ i.id }</p>
                <div className={ s.line }/>
              </div>
              <div className={ cn(s.center, "mt-2 ph-2 box-sizing") }>
                <div className={ s.line }/>
                <h4 className={ cn("mt-0 mb-1", { 'red-2': i.id === current }) }>{ i.name }</h4>
                <div className={ cn(s.content, { "no-pointer-events": i.id !== current }) }>
                  { i.type === 'info' ?
                      <div className={ cn(s.info) }>
                        <div className={ cn(s.infoItem) }>
                          <p className="small light pre-line mv-05">Street address</p>
                          <p className="small semiBold pre-line mv-05">{ property.street_address }</p>
                        </div>
                        <div className={ cn(s.infoItem) }>
                          <p className="small light pre-line mv-05">Price</p>
                          <p className="small semiBold pre-line mv-05">
                            { !property.gross_price ?
                                "POA"
                              : property.dealType === 'toLet' ?
                                unitsCount > 1 ?
                                  `from R${ formatNumberWith(" ", property.gross_price) } per m²`
                                :
                                  `R${ formatNumberWith(" ", property.gross_price) } per m²`
                              :
                                `R${ formatNumberWith(" ", property.gross_price) }`
                            }
                          </p>
                        </div>
                        <div className={ cn(s.infoItem) }>
                          <p className="small light pre-line mv-05">Suburb</p>
                          <p className="small semiBold pre-line mv-05">{ property.suburb }</p>
                        </div>
                        <div className={ cn(s.infoItem) }>
                          <p className="small light pre-line mv-05">GLA</p>
                          <p className="small semiBold pre-line mv-05">
                            { property.min_gla !== property.max_gla ?
                                `${ formatNumberWith(' ', property.min_gla || 0) }m² - ${ formatNumberWith(' ', property.max_gla || 0) }m²`
                              :
                                `${ formatNumberWith(' ', property.min_gla || 0) }m²`
                            }
                          </p>
                        </div>
                        <div className={ cn(s.infoItem) }>
                          <p className="small light pre-line mv-05">City</p>
                          <p className="small semiBold pre-line mv-05">{ property.city }</p>
                        </div>
                        <div className={ cn(s.infoItem) }>
                          <p className="small light pre-line mv-05">Category</p>
                          <p className="small semiBold pre-line mv-05">{ property.property_category }</p>
                        </div>
                      </div>
                    : i.type === 'location' ?
                      <div className={ cn(s.map) }>
                        <GoogleMap latitude={ map?.latitude } longitude={ map?.longitude }/>
                      </div>
                    : i.type === 'contact' ?
                      <div className={ cn(s.contactContainer) }>
                        <Icon className="no-tablet no-mobile mb-1" src={ ContactIcon } size='xxxxl'/>
                        <Icon className="no-desktop mb-0" src={ ContactIcon } size='xxl'/>
                        <Button className={ cn(s.contactButton) } onClick={ onClickToContactForm } rounded>Get in touch</Button>
                      </div>
                    :
                      <div className={ s.shareContainer }>
                        <div className={ cn(s.left, "pl-1 pr-2 box-sizing") }>
                          <p className={cn('large bold cursor-pointer', { [s.shareActive]: shareType === 'pdf' }) }
                             onClick={ () => handleShareChange('pdf') }>
                            Download PDF
                          </p>
                          <p className={cn('large bold cursor-pointer', { [s.shareActive]: shareType === 'email' }) }
                             onClick={ () => handleShareChange('email') }>
                            Email
                          </p>
                          <p className={cn('large bold cursor-pointer', { [s.shareActive]: shareType === 'share' }) }
                             onClick={ () => handleShareChange('share') }>
                            Share
                          </p>
                        </div>
                        <div className={ cn(s.right, "pr-4 box-sizing") }>
                          <Icon className="no-tablet no-mobile" src={ shareTypeToProps[shareType].icon } size='xxxxl'/>
                          <Icon className="no-desktop" src={ shareTypeToProps[shareType].icon } size='xxl'/>
                          { shareTypeToProps[shareType].content }
                        </div>
                      </div>
                  }
                </div>
              </div>
              <div className={ cn(s.bottom, "cursor-pointer hover-opacity", { "no-pointer-events": i.id === current }) }
                   onClick={ () => handleChange(i.id) }>
                <p className="medium x-small red-2 uppercase mv-0 mr-05">View</p>
                <div className={ cn(s.icon, { [`ml-05 ${ s.animated }`]: i.type === 'location' }) }>
                  <Icon src={ ArrowIcon } rotate={ 90 } size='xs'/>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default Accordion
