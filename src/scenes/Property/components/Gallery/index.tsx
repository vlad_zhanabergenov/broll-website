import React, {ReactNode, useState} from 'react'
import cn from 'classnames'

import { ImageCarousel, GoogleMap } from '../index'

import s from './style.module.sass'

interface IProps {
  images?: Image[]
  video?: string
  map?: {
    latitude?: number
    longitude?: number
  }
}

enum EGalleryItem {
  Exterior = 'Exterior',
  Interior = 'Interior',
  Video = 'Video',
  FloorPlans = 'Floor Plans',
  Map = 'Map'
}

const Gallery: React.FC<IProps> = ({
  images,
  video,
  map
}) => {
  const exteriorImages = images?.filter(i => i.type === 'aerial') || []
  const interiorImages = images?.filter(i => i.type === 'interior') || []
  const floorPlans = images?.filter(i => i.type === 'floor_plan') || []

  const gallery = [exteriorImages, interiorImages, video, floorPlans, map]
  const enumValues = Object.values(EGalleryItem)
  const result = gallery.reduce((acc, curr, index) => {
    if (Array.isArray(curr)) {
      if (!curr.length) return acc
    } else {
      if (!Boolean(curr)) return acc
    }

    return {
      ...acc,
      [enumValues[index]]: curr
    }
  }, {} as Record<EGalleryItem, any>)
  const availableItems = Object.keys(result) as EGalleryItem[]

  const [galleryItem, setGalleryItem] = useState(availableItems[0])

  const NoContent: React.FC<{ message: string }> = ({ message }) => (
    <div className={ cn(s.empty) }>
      <p className='uppercase bold large'>{ message }</p>
    </div>
  )

  const galleryItems: Record<EGalleryItem, ReactNode> = {
    [EGalleryItem.Exterior]: exteriorImages.length ? <ImageCarousel images={ exteriorImages }/> : <NoContent message="No images"/>,
    [EGalleryItem.Interior]: interiorImages.length ? <ImageCarousel images={ interiorImages }/> : <NoContent message="No images"/>,
    [EGalleryItem.Video]: video ? (
      <iframe
        className={ cn(s.iframe) }
        src={ `https://www.youtube.com/embed/${video}` }
        title='YouTube video player' frameBorder='0'
        allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
        allowFullScreen
      />
    ) : (
      <NoContent message="No video"/>
    ),
    [EGalleryItem.FloorPlans]: floorPlans.length ? <ImageCarousel images={ floorPlans }/> : <NoContent message="No images"/>,
    [EGalleryItem.Map]: map ? (
      <GoogleMap latitude={ map?.latitude } longitude={ map?.longitude }/>
    ) : (
      <NoContent message="No map data"/>
    )
  }

  return (
    <div className={ cn(s.Gallery, 'box-sizing') }>
      <div className={ cn(s.main) }>
        <h3 className={ cn(s.title, 'red-2 mt-1 mb-3 bold') }>Gallery</h3>
        <div className={ cn(s.tabs) }>
          { availableItems.map(item =>
              <div
                key={ item }
                className={ cn('cursor-pointer', { [s.current]: galleryItem === item }) }
                onClick={ () => setGalleryItem(item as EGalleryItem) }
              >
                <h4 className={ cn(s.tab, 'bold') }>{ item }</h4>
              </div>
          ) }
        </div>
      </div>
      <div className={ cn(s.content) }>
        { galleryItems[galleryItem] }
      </div>
    </div>
  )
}

export default Gallery
