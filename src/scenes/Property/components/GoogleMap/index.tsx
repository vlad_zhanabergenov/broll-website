import React from 'react'
import { GoogleMap as GoogleMapComponent, Marker, LoadScript } from '@react-google-maps/api'

import cn from 'classnames'
import s from './style.module.sass'
import MapMarkerIcon from 'assets/icons/mapMarker'

interface Props {
  latitude?: number
  longitude?: number
}

const config = {
  apiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY || ""
}

const GoogleMap: React.FC<Props> = ({ latitude, longitude }) => {
  return (
    <div className={ cn(s.GoogleMap) }>
      <div className={ s.layout }>
        <LoadScript googleMapsApiKey={ config.apiKey }>
          <GoogleMapComponent
            options={{
              mapTypeControl: false,
              fullscreenControl: false,
              panControl: false,
              streetViewControl: false,
              controlSize: 25
            }}
            mapContainerStyle={{ width: '100%', height: '100%' }}
            center={{ lat: latitude || 0, lng: longitude || 0 }}
            zoom={ 18 }
          >
            <Marker position={{ lat: latitude || 0, lng: longitude || 0 }} icon={ MapMarkerIcon }/>
          </GoogleMapComponent>
        </LoadScript>
      </div>
    </div>
  )
}

export default GoogleMap
