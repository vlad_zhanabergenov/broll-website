import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import initialState from 'cache/initialState'
import { pickBy } from 'lodash'
import { useForm } from 'hooks'
import { useForm as useFormspree } from '@formspree/react'
import formatPhoneNumber from 'services/formatPhoneNumber'
import searchStateParser from 'services/searchStateParser'
import ReCAPTCHA from 'react-google-recaptcha'

import { CloudImage, Link, Button, Input, Textarea, Icon } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import NoAvatar from 'assets/pictures/noAvatar'
import LinkedIn from 'assets/icons/linkedIn.svg'
import Twitter from 'assets/icons/twitter.svg'
import Logo from 'assets/pictures/logo.svg'

interface Props {
  people?: Responsibility[]
  property: AnyProperty
  dealType?: AnyProperty['dealType']
  isUnit?: boolean
}

interface Form {
  name: string
  contactNumber: string
  email: string
  message: string
}

const ContactForm: React.FC<Props> = ({ people, property, dealType, isUnit }) => {
  const router = useRouter()

  const getInitialMessage = () => {
    if (isUnit) {
      return `Please get in touch with me about leasing ${ property.unit_id } in ${ property.property_name } (Webref: ${ property.web_ref })`
    }

    if (dealType === 'toLet') {
      return `Please get in touch with me about leasing a space in ${ property.property_name }`
    } else {
      return `Please get in touch with me about purchasing ${ property.property_name } (Webref: ${ property.web_ref })`
    }
  }

  const [loading, setLoading] = useState(false)
  const [formSent, setFormSent] = useState(false)
  const [message, setMessage] = useState('')
  const [captcha, setCaptcha] = useState<string | null>(null)

  const list: Responsibility[] = people ? people.slice(0, 2) : []

  const handleCaptcha = (token: string | null) => {
    setCaptcha(token)
  }

  const [formspreeState, handleFormspreeSubmit] = useFormspree(process.env['NEXT_PUBLIC_FORMSPREE_PROPERTY_CONTACT_ID']!)

  useEffect(() => {
    if (formspreeState.succeeded) {
      setLoading(false)
      setFormSent(true)
      setMessage('Your message has been sent successfully.')
    } else if (formspreeState.errors.length) {
      setLoading(false)
      setMessage('Something went wrong, please try again later.')
    }
  }, [formspreeState])

  const handleSubmit = (values: Form, valid: boolean, errors: { [key in keyof Form]: string | null }) => {
    if (valid) {
      setLoading(true)
      handleFormspreeSubmit({
        property_id: property.objectID,
        name: values.name,
        contactNumber: values.contactNumber,
        email: values.email,
        message: values.message
      })
    } else {
      const errorsList = Object.values(pickBy(errors))
      const firstError = errorsList.includes('Field is required') ? 'Field is required' : errorsList[0]

      if (firstError) {
        const text = firstError === 'Field is required' ? "Please fill the required fields." : firstError
        setMessage(text)
      }
    }
  }

  const { values, change, submit } = useForm<Form>({
    fields: {
      name: { initialValue: '', required: true, validator: 'min2' },
      contactNumber: { initialValue: '', required: true, validator: 'phone' },
      email: { initialValue: '', required: true, validator: 'email' },
      message: { initialValue: '' }
    }, handleSubmit
  })

  useEffect(() => {
    change({ name: 'message', value: getInitialMessage() })
  }, [router.asPath])

  return (
    <div className={ cn(s.ContactForm) }>
      <div className={ s.layout }>
        { !!list.length ? (
          <div className={ cn(s.left, 'pr-6 box-sizing') }>
            { list.map((i, num) =>
              <div key={ `${ i.gmaven_contact_key }-${ num }` } className={ cn(s.item, 'mb-2') }>
                <div className={ cn(s.avatar, 'mb-2 overflow-hidden') }>
                  <CloudImage
                    className="covered"
                    src={ i.image || NoAvatar }
                    alt={ i.name }
                    responsive={{
                      desktopLarge: { w: '300', ar: '1' },
                      desktop: { w: '200', ar: '1' },
                      tablet: { w: '180', ar: '1' },
                      mobile: { w: '140', ar: '1' }
                    }}
                  />
                  <div className={ s.border }/>
                </div>
                <div className={ cn(s.info, 'text-center') }>
                  <p className='semiBold m-0'>{ i.name }</p>
                  <p className='light mt-0 mb-05' >{ i.role }</p>
                  { i.cell_number &&
                    <p className='light small mv-05'>{ formatPhoneNumber(`+${ i.cell_number! }`) }</p>
                  }
                  { i.email &&
                    <a className='light small mv-05' href={ `mailto:${i.email}` } target='_blank' rel='noopener noreferrer'>{ i.email }</a>
                  }
                  <Link to={ `/search?${ searchStateParser.encode({ ...initialState.search, broker: [i.name!] }) }` }>
                    <p className='light red-2 small mv-05 cursor-pointer'>View my listings</p>
                  </Link>
                  <div className={ cn(s.social, 'mt-05') }>
                    { i.linkedin_profile && <Link className='mh-05 cursor-pointer' newTab to={ `https://www.linkedin.com/in/${i.linkedin_profile}` }><Icon src={ LinkedIn } size='xl'/></Link> }
                    { i.twitter_profile && <Link className='mh-05 cursor-pointer' newTab to={ `https://twitter.com/${i.twitter_profile}` }><Icon src={ Twitter } size='xl'/></Link> }
                  </div>
                </div>
              </div>
            )}
          </div>
        ) : (
          <div className={ cn(s.left, 'pr-6 box-sizing mb-1') }>
            <div className={ cn(s.noBrokerContainer) }>
              <img className={ cn(s.logo, 'contained') } src={ Logo } alt="Broll logo"/>
              <div className='mt-05'>
                <p className='mv-05'><span className='light lightRed'>Tel</span><span className='light'> +27 11 441 4000</span></p>
                <p className='mv-05'><span className='light lightRed'>Fax</span><span className='light'> +27 11 441 4452</span></p>
                <p className='mv-05'>
                  <span className='light lightRed'>Email</span>
                  <a className='light' href='mailto:info@broll.com' target='_blank' rel='noopener noreferrer'> info@broll.com</a>
                </p>
              </div>
              <div className={ cn(s.social, 'mt-05') }>
                <Link className='mh-05 cursor-pointer' newTab to='https://www.linkedin.com/company/broll-property-group'>
                  <Icon src={ LinkedIn } size='xl'/>
                </Link>
                <Link className='mh-05 cursor-pointer' newTab to='https://twitter.com/broll_insights'>
                  <Icon src={ Twitter } size='xl'/>
                </Link>
              </div>
            </div>
          </div>
        ) }
        <div className={ s.right }>
          <div className={ s.fields }>
            <Input secondary name="name" placeholder="Name" disabled={ formSent }
                    onChange={ change } required className='mb-1' value={ values.name }/>
            <Input secondary name="contactNumber" placeholder="Contact Number" disabled={ formSent }
                    onChange={ change } required className='mb-1' value={ values.contactNumber }/>
            <Input secondary name="email" placeholder="Email Address" disabled={ formSent }
                    onChange={ change } required value={ values.email }/>
            <Textarea secondary name="message" rows={ 5 } required onChange={ change } value={ values.message } disabled={ formSent }/>
            <p className={ cn({ 'no-pointer-events no-select opacity-0': !message }) }>{ message || "_" }</p>
          </div>
          <div className={ cn(s.footer) }>
            <div className={ cn(s.captcha, 'mb-05') }>
              <ReCAPTCHA
                sitekey={ process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY || '' }
                onChange={ handleCaptcha }
              />
            </div>
            <Button onClick={ submit } loading={ loading } disabled={ !captcha || formSent }>Send</Button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ContactForm
