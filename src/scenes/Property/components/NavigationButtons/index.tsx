import React from 'react'

import { Link, Icon } from 'components'

import cn from 'classnames'
import groupPropertiesByGmavenKey from 'services/groupPropertiesByGmavenKey'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrowBlack.svg'

interface Props {
  properties: AnyProperty[]
  isUnit?: string
}

const NavigationButtons: React.FC<Props> = ({ properties, isUnit }) => {
  if (properties.length < 1) {
    return null
  }

  let prevProperty: AnyProperty | undefined
  let nextProperty: AnyProperty | undefined

  if (!isUnit) {
    const list = groupPropertiesByGmavenKey(properties)

    prevProperty = list[0]
    nextProperty = list[1]
  } else {
    const current = properties.findIndex(i => i.unit_id === isUnit)

    prevProperty = properties[current - 1]
    nextProperty = properties[current + 1]
  }

  return (
    <div className={ s.NavigationButtons }>
      { prevProperty &&
        <Link
          to='property'
          data={{ property: prevProperty, isUnit: !!isUnit || undefined }}
          className={ cn(s.previous, 'cursor-pointer') }
        >
          <Icon src={ ArrowIcon } rotate={ 270 } size='s'/>
          <p className='small semiBold mv-0 ml-1'>{ !isUnit ? prevProperty.property_name : prevProperty.unit_id }</p>
        </Link>
      }
      { nextProperty &&
        <Link
          to='property'
          data={{ property: nextProperty, isUnit: !!isUnit || undefined }}
          className={ cn(s.next, 'cursor-pointer') }
        >
          <p className='small semiBold mv-0 mr-1'>{ !isUnit ? nextProperty.property_name : nextProperty.unit_id }</p>
          <Icon src={ ArrowIcon } rotate={ 90 } size='s'/>
        </Link>
      }
    </div>
  )
}

export default NavigationButtons
