import React  from 'react'
import axios, { AxiosResponse } from 'axios'
import cn from 'classnames'

import { Button } from 'components'

import s from './style.module.sass'

interface IProps {
  data: AnyProperty[]
  item: PDFItem
  className?: string
}

class PDFDownloadButton extends React.Component<IProps> {
  state = {
    enabled: false,
    loading: false,
    success: false,
  }

  handleResponse = (response: AxiosResponse) => {
    if (typeof window !== 'undefined' && typeof document !== 'undefined') {
      const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', `${this.props.data[0].property_name}.pdf`)
      document.body.appendChild(link)
      link.click()
      this.setState({
        enabled: true,
        success: true,
        loading: false
      })
    }
  }

  //Property to let
  createPropLetPdf = () => {
    const unitIds = this.props.data.map(unit => unit.gmaven_mapped_key).join(',')

    this.setState({
        enabled: true,
        success: false,
        loading: true
      })

    axios.post('https://www.gmaven.com/api/data/report/v2/LeasingBrochure',
      {
        propertyUnitIds: unitIds,
        propertyDomainKey: this.props.data[0].property_gmaven_key,
        reportType: process.env.NEXT_PUBLIC_GATSBY_PROP_LEASING_BROCHURE,
        userDomainKey: this.props.data?.[0]?.property_responsibility?.[0]?.gmaven_contact_key || null
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'gmaven.apiKey': process.env.NEXT_PUBLIC_GATSBY_GMAVEN_API
        },
        responseType: 'blob',
      }
    )
      .then(this.handleResponse)
      .catch(error => {
        this.setState({ loading: false })
        console.error(error)
      })
  }

  //Unit to let
  createUnitLetPdf = () => {
    this.setState({
        enabled: true,
        success: false,
        loading: true
      })

    axios.post('https://www.gmaven.com/api/data/report/v2/UnitLeasingBrochure',
      {
        propertyDomainKey: this.props.data[0].property_gmaven_key,
        propertyUnitId: this.props.data[0].gmaven_mapped_key,
        reportType: process.env.NEXT_PUBLIC_GATSBY_UNIT_LEASING_BROCHURE,
        userDomainKey: (this.props?.data?.[0] as PropertyToLet)?.unit_responsibility?.[0]?.gmaven_contact_key || null
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'gmaven.apiKey': process.env.NEXT_PUBLIC_GATSBY_GMAVEN_API
        },
        responseType: 'blob',
      }
    )
      .then(this.handleResponse)
      .catch(error => {
        this.setState({ loading: false })
        console.error(error)
      })
  }

  //Property for sale
  createPropSalePdf = () => {
    this.setState({
      enabled: true,
      success: false,
      loading: true
    })

    axios.post('https://www.gmaven.com/api/data/report/v2/SalesBrochure',
      {
        reportName: 'SalesBrochure',
        text: 'Sales brochure',
        includeYieldOnAcuisition: true,
        includeAskingPerSqm: true,
        includeVacantGla: true,
        includeStreetAddress: true,
        propertyDomainKey: this.props.data[0].gmaven_mapped_key,
        reportType: process.env.NEXT_PUBLIC_GATSBY_PROP_SALES_BROCHURE,
        userDomainKey: this.props.data[0].property_responsibility && this.props.data[0].property_responsibility.length > 0 ? this.props.data[0].property_responsibility[0].gmaven_contact_key : null
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'gmaven.apiKey': process.env.NEXT_PUBLIC_GATSBY_GMAVEN_API
        },
        responseType: 'blob',
      }
    )
      .then(this.handleResponse)
      .catch(error => {
        this.setState({ loading: false })
        console.error(error)
      })
  }

  //template selection
  typeToPdfCreator: Record<PDFItem, () => void> = {
    propertyToLet: this.createPropLetPdf,
    propertyUnitToLet: this.createUnitLetPdf,
    propertyForSale: this.createPropSalePdf
  }

  render() {
    const { loading } = this.state
    const { item, className } = this.props

    return (
      <Button rounded className={ cn(s.PDFDownloadButton, className) } onClick={ this.typeToPdfCreator[item] } disabled={ loading }>
        { loading ? 'Loading' : 'Download' }
      </Button>
    )
  }
}

export default PDFDownloadButton
