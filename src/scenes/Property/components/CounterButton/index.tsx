import React from 'react'
import cn from 'classnames'

import { Icon } from 'components'

import s from './style.module.sass'
import EmailIcon from 'assets/icons/email.png'

interface IProps {
  onClick: () => void
}

const CounterButton: React.FC<IProps> = ({ onClick }) => {

  return (
    <div className={ cn(s.CounterButton, "box-sizing cursor-pointer no-mobile no-tablet") } onClick={ onClick }>
      <div className={ s.icon }>
        <Icon src={ EmailIcon }/>
      </div>
      <p className='white light'>Email the broker</p>
      <div className={ s.counter }>
        <p className='white m-0 semiBold'>1</p>
      </div>
    </div>
  )
}

export default CounterButton
