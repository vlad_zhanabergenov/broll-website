import React, { ReactNode, useState } from 'react'
import cn from 'classnames'

import { Button, Icon } from 'components'
import formatNumberWith from 'services/formatNumberWith'
import { PDFDownloadButton } from '../index'

import s from './style.module.sass'

import ShareArrowIcon from 'assets/icons/shareArrow.svg'
import DownloadIconMini from 'assets/icons/downloadMini.svg'
import ShareIcon from 'assets/icons/share.png'
import DownloadIcon from 'assets/icons/download.png'
import ArrowIcon from 'assets/icons/arrow.svg'

enum View {
  Share = 'Share',
  Download = 'Download',
  Information = 'Information'
}

interface IProps {
  property: AnyProperty
}

const InformationBlock: React.FC<IProps> = ({ property }) => {
  const [viewType, setViewType] = useState<View>(View.Information);

  const handleViewTypeChange = (type: View) => {
    setViewType(type)
  }

  const handleShareClick = () => {
    const template = document.createElement('input')
    const currentLink = location.href
    document.body.appendChild(template)
    template.value = currentLink
    template.select()
    document.execCommand('copy')
    document.body.removeChild(template)
  }

  const viewTypeToContent: Record<View, ReactNode> = {
    [View.Information]: (
      <>
        <div className={ cn(s.item, 'pb-2') }>
          <p className={ cn(s.itemTitle, 'uppercase bold') }>Address</p>
          <p className='mv-0'>{ property.street_address }</p>
          <p className='mv-0'>{ property.suburb }</p>
          <p className='mv-0'>{ property.city }</p>
        </div>
        <div className={ cn(s.item, 'pb-2') }>
          <p className={ cn(s.itemTitle, 'uppercase bold') }>Province</p>
          <p className='mv-0'>{ property.province }</p>
        </div>
        <div className={ cn(s.item, 'pb-2') }>
          <p className={ cn(s.itemTitle, 'uppercase bold') }>Leasing Info</p>
          <p className='mv-0'>
            { !property.gross_price ?
              "Price: POA"
              : property.dealType === 'toLet' ?
                `Price: R${ formatNumberWith(" ", property.gross_price) }/m²`
                :
                `Price: R${ formatNumberWith(" ", property.gross_price) }`
            }
          </p>
          <p className='mv-0'>
            { `GLA: ${ formatNumberWith(' ', property.max_gla || 0) }m²` }
          </p>
        </div>
        <div className={ cn(s.item, 'pb-2') }>
          <p className={ cn(s.itemTitle, 'uppercase bold') }>Share</p>
          <div className={ cn(s.share, 'cursor-pointer mb-1') } onClick={ () => handleViewTypeChange(View.Share) }>
            <Icon src={ ShareArrowIcon } size='m' className='mr-05'/>
            <p className='mv-0'>Share this unit</p>
          </div>
          { (property as PropertyForSale).property_type !== 'property_unit' && (
            <div className={ cn(s.share, 'cursor-pointer') } onClick={ () => handleViewTypeChange(View.Download) }>
              <Icon src={ DownloadIconMini } size='m' className='mr-05'/>
              <p className='mv-0'>Download</p>
            </div>
          ) }
        </div>
      </>
    ),
    [View.Share]: (
      <div className={ cn(s.shareContent) }>
        <Icon className="no-tablet no-mobile mb-2" src={ ShareIcon } size='xxxl'/>
        <Icon className="no-desktop mb-2" src={ ShareIcon } size='xxl'/>
        <Button onClick={ handleShareClick } className={ cn(s.shareButton) } rounded>Copy link</Button>
      </div>
    ),
    [View.Download]: (
      <div className={ cn(s.shareContent) }>
        <Icon className="no-tablet no-mobile mb-2" src={ DownloadIcon } size='xxxl'/>
        <Icon className="no-desktop mb-2" src={ DownloadIcon } size='xxl'/>
        <PDFDownloadButton
          className={ cn(s.shareButton) }
          data={ [property] }
          item={ property.dealType === 'toLet' ? 'propertyUnitToLet' : 'propertyForSale' }
        />
      </div>
    )
  }

  return (
    <div className={ cn(s.InformationBlock, "pt-2 pb-4") }>
      <div className={ cn(s.layout, "pl-2 pr-4") }>
        <h3 className={ cn(s.title, 'red-2 mt-3 mb-1 bold') }>{ viewType }</h3>
        <div className={ cn(s.content, 'box-sizing') }>
          { viewTypeToContent[viewType] }

          { viewType !== View.Information && (
            <div onClick={ () => setViewType(View.Information) } className={ cn(s.backButton, 'cursor-pointer hover-opacity') }>
              <p className='xx-small bold m-0 pr-1 uppercase cursor-pointer'>back</p>
              <div className={ s.icon }>
                <Icon src={ ArrowIcon } rotate={ 270 } size='s'/>
              </div>
            </div>
          ) }
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
