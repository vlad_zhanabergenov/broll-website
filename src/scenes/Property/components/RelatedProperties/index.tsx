import React from 'react'
import groupPropertiesByGmavenKey from 'services/groupPropertiesByGmavenKey'

import { RelatedPropertiesCarousel } from './components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  list: AnyProperty[]
}

const RelatedProperties: React.FC<Props> = ({ list }) => {
  const items = groupPropertiesByGmavenKey(list)

  return (
    <div className={ cn(s.RelatedProperties) }>
      <div className={ cn(s.layout, "overflow-hidden") }>
        <div className={ cn(s.heading, 'mb-1 ph-10 box-sizing') }>
          <h3 className="bold m-0">Related Properties</h3>
        </div>
        <div className={ cn(s.carousel, "overflow-hidden") }>
          <RelatedPropertiesCarousel list={ items }/>
        </div>
      </div>
    </div>
  )
}

export default RelatedProperties
