import React from "react";
import { CloudImage } from "components";

import TopImage from "../../../../assets/pictures/capabilitiesTop";
import LogoPic from "../../../../assets/pictures/logo.svg";
import Icon1 from "../../../../assets/pictures/ourCapabilitiesIcon1";
import Icon2 from "../../../../assets/pictures/ourCapabilitiesIcon2";
import Icon3 from "../../../../assets/pictures/ourCapabilitiesIcon3";
import Icon4 from "../../../../assets/pictures/ourCapabilitiesIcon4";
import Icon5 from "../../../../assets/pictures/ourCapabilitiesIcon5";
import Icon6 from "../../../../assets/pictures/ourCapabilitiesIcon6";
import Icon7 from "../../../../assets/pictures/ourCapabilitiesIcon7";
import Icon8 from "../../../../assets/pictures/ourCapabilitiesIcon8";
import Icon9 from "../../../../assets/pictures/ourCapabilitiesIcon9";
import Icon10 from "../../../../assets/pictures/ourCapabilitiesIcon10";
import Icon11 from "../../../../assets/pictures/ourCapabilitiesIcon11";

import s from "./style.module.sass";
import cn from "classnames";

const list = [
  { text: "$2.5bn-Assets under Facilities Management", icon: Icon1 },
  { text: "$4bn-Assets under Occupier Managemen", icon: Icon2 },
  { text: "$6bn-Total value of properties valued", icon: Icon3 },
  { text: "$17bn-Total value of assets under management", icon: Icon4 },
  { text: "7,000-Vendor relationship", icon: Icon5 },
  {
    text: "3 countries-Operating in countries across Africa Operating in countries across Africa",
    icon: Icon6,
  },
  {
    text: "43mil m² Space under management",
    icon: Icon7,
  },
  { text: "51 % Black-women owned", icon: Icon8 },
  { text: "2000+ Group Employees", icon: Icon9 },
  { text: "Broll Property Group Established copy", icon: Icon10 },
  { text: "Broll Property Group Established", icon: Icon11 },
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={cn(s.section1, "mt-1")}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 mb-4")}>
            World-class services leading to sustainable <br /> real estate
            success.
          </h3>
          <div className={cn(s.topImage, "")}>
            <div className={cn(s.waterMark, "")}>
              <div className={cn(s.left, "")}>
                <h3 className={cn(s.text, "white light mb-1")}>
                  Over the years we have built a high-performance, respectful
                  and dynamic culture that enables professional real estate
                  services, driven through trusted internal and external
                  relationships.
                </h3>
              </div>
              <div className={cn(s.right, "")}>
                <CloudImage
                  src={LogoPic}
                  alt="Broll"
                  className="covered"
                  responsive={{
                    desktop: { w: "1920" },
                  }}
                />
              </div>
            </div>
            <CloudImage
              src={TopImage}
              alt="Broll"
              className="covered"
              responsive={{
                desktop: { w: "1920" },
              }}
            />
          </div>
        </div>
        <div className={cn(s.section2, "mt-1")}>
          <h3 className={cn(s.title, "semiBig m-0 red bold")}>Our Approach</h3>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 mb-3")}>
            We’re committed to improving the built <br />
            environment for people and the planet.
          </h3>
          <div className={cn(s.list, "mt-1")}>
            <div className={cn(s.circle, "")}></div>
            {list.map((item) => (
              <div className={cn(s.block, "")} key={item.text}>
                <div className={cn(s.iconContainer, "")}>
                  <div className={cn(s.icon, "")}>
                    <CloudImage
                      src={item.icon}
                      alt="Broll"
                      className="covered"
                      responsive={{
                        desktop: { w: "320" },
                      }}
                    />
                  </div>
                </div>
                <div className={cn(s.textContainer, "")}>
                  <div>
                    <h3 className={cn(s.title, "medium mt-1 mb-1")}>
                      {item.text}
                    </h3>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
