import React from 'react'

import s from './style.module.sass'
import cn from 'classnames'

import { CloudImage } from 'components'
import TopImage from '../../../../assets/pictures/propertyManagementTop'

const topRightList = [
  'Estate Agency Affairs Board (EAAB)',
  'South African Council of Shopping Centre (SACSC)',
  'South African Property Owners Association (SAPOA)',
  'South African Institute of Black Property Professionals (SAIBPP)',
  'Institute of Real Estate Management (IREM)',
  'Green Building Council of South Africa (GBCSA)'
]

const leftList = [
  'Professional property management',
  'Retail leasing and consulting',
  'Lease renewal    negotiations',
  'Vacant space management',
  'Project management',
  'Tenant co-ordination',
  'Manage buildings for sustainability',
  'Carbon intelligence advisory',
  'Energy consumption management and efficiencies',
  'Insurance compliance',
]

  const rightList = [
    'Financial accounting and management',
    'Rent recoveries and accounts payment',
    'Building sales and feasibilities',
    'Datamart licences for asset management',
    'Service provider accreditation, procurement and management',
    'Commercial, residential and leisure management services',
    'Budgeting and forecasting maintenance management risk and compliance',
    'Sectional title property management',
    'Communication, Marketing, Research and Brand Campaigns'
  ]

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
          Robust range of investor services designed to drive <br /> real estate performance and asset value.
          </h3>
          <div className={ cn(s.topImage, 'mt-1') }>
            <CloudImage
              src={ TopImage }
              alt="Broll"
              className='covered'
              responsive={{
                desktop: { w: '1920' },
              }}
            />
          </div>
          <div className={cn(s.container, "")}>
          <div className={cn(s.left, "")}>
            <h3 className={cn(s.title, "semiBig bold red-4")}>
              Our Value Proposition
            </h3>
            <p className={cn(s.text, "white small light letterSpacingNormal mb-0")}>
              No other independent service provider has Broll’s expertise and proven track record when it comes to property management in South Africa. We deliver superior returns - something that we've consistently achieved even in tough market conditions. <br />That’s why some of Africa’s largest portfolios, both listed and private have entrusted us with their assets. 
            </p>
          </div>
          <div className={cn(s.right)}>
            <h3 className={cn(s.title, "semiBig bold mt-0")}>
              Professional Industry Associations
            </h3>
            <div className={cn(s.list)}>
              { topRightList.map(item =>
                <p key={ item } className='x-small regular'>{ item }</p>
              )}
              </div>
          </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-3")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-1")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
              { leftList.map(item =>
                <p key={ item } className='x-small regular'>{ item }</p>
              )}
              </div>
            </div>
            <div className={cn(s.right, "pl-4 pr-4 box-sizing")}>
            <div className={cn(s.list)}>
              { rightList.map(item =>
                <p key={ item } className='x-small regular'>{ item }</p>
              )}
              </div>
            </div>
          </div>
        </div>

        <div className={cn(s.section3, "grey-2 pb-4")}>
          <div className={cn(s.block, " box-sizing")}>
           
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0")}>17</h2>
                <p className={cn(s.text, "small light")}>Managing assets for listed funds.</p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
           
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0")}>900</h2>
                <p className={cn(s.text, "small light")}>
                Managing assets for listed funds.
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0")}>1200</h2>
                <p className={cn(s.text, "small light")}>Properties under management.</p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0")}>ISO</h2>
                <h2 className={cn(s.title, "small bold m-0")}>9001:</h2>
                <h2 className={cn(s.title, "small bold m-0")}>2015</h2>
                <p className={cn(s.text, "small light")}>Certified</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
