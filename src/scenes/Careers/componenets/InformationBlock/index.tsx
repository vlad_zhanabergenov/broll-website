import React from 'react'
import { CloudImage, Link } from 'components'
import s from './style.module.sass'
import cn from 'classnames'
import BrollAcademyPic from 'assets/pictures/brollAcademy'

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.header }>
          <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>Click 
            <Link to=" https://broll.simplify.hr/" newTab>
              <span className="bold red-2 small cursor-pointer"> here </span> 
            </Link> to see the latest job vacancies and/or email your CV to <a href="mailto:hr@broll.com" className="small red-2 bold">hr@broll.com</a></p>
          <p className='light no-desktop m-0 letterSpacingNormal'>Click
            <Link to=" https://broll.simplify.hr/" newTab>
              <span className="bold red-2 light cursor-pointer"> here </span>
            </Link> to see the latest job vacancies and/or email your CV to <a href="mailto:hr@broll.com" className="light red-2 bold">hr@broll.com</a></p>
        </div>
        <div className={ s.section1 }>
          <div className={ s.image }>
            <CloudImage
              src={ BrollAcademyPic }
              alt="Broll Broking"
              className='covered'
              responsive={{
                desktop: { w: '1920' }
              }}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
