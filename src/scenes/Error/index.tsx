import s from './style.module.sass'

const ErrorScene = () => {
  return (
    <div className={ s.ErrorScene }>
      <h2 className="white mt-0 mb-5">Page not found</h2>
    </div>
  )
}

export default ErrorScene
