import React from "react";
import CloudImage from "components/CloudImage";
import img from '../../../../assets/pictures/workplaceSolutions'
import s from "./style.module.sass";
import cn from "classnames";

const topLeftList = [
  "We’re 360 workplace strategists, a team of multi-disciplinary consultants. We consider where people work and how people work – and how that relates to business culture, efficiency, and profitability. We know that the implementation of a comprehensive workplace strategy – beyond just design and build – can exponentially affect the success of an organisation.",
  "We take a total view of an organisation, and spend time studying its people, future plans, and brand ambitions. This is workplace strategy as it should be done: a data-driven, research-led process that results in creatively relevant, profit-generating solutions. Your space is an investment.",
  "Through our workplace services teams we can fulfil all the typical roles of the project team, including project management, interior design, building engineering services, cost consultancy and capital allowances.  ",
];

const leftList = [
  {
    title: 'Workplace Assessment',
    list: ['Staff experience surveys', 'Test fit layouts', 'Elemental costs estimates']
  },
  {
    title: 'Workplace Strategy',
    list: ['Leadership visioning', 'Departmental profiling', 'Occupancy planning']
  },
  {
    title: 'Workplace Design',
    list: ['Integrated space planning', 'Conceptual design', 'Design development']
  },
  {
    title: 'Workplace Projects',
    list: ['Project management', 'Cost management', 'Engineering services', 'Procurement management & tender valuations']
  },
  {
    title: 'Workplace Change',
    list: ['Change management', 'Transition management', 'Communication strategy']
  },
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
          Internal Developers. We’re here to optimise the world
of work.
          </h3>
          <div className={s.container}>
            <div className={cn(s.left, "")}>
              <h3 className={cn(s.title, "semiBig bold red-4")}>
                Our Value Proposition
              </h3>
              {topLeftList.map((item) => (
                <p
                  className={cn(
                    s.text,
                    "white small light letterSpacingNormal mb-0"
                  )}
                >
                  {item}
                </p>
              ))}
            </div>
            <div className={cn(s.right)}>
              <CloudImage
                src={img}
                alt="Broll"
                className="covered"
                responsive={{
                  desktop: { w: "1920" },
                }}
              />
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-0")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-1")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-0 box-sizing")}>
              <div className={cn(s.list)}>
                {leftList.map((item) => (
                  <div className={cn(s.block, " box-sizing")}>
                    <h4 className={cn(s.title, "bold red-4 m-0")}>{item.title}</h4>
                    {item.list.map(ListItem => (
                      <p key={ListItem} className="x-small regular">{ListItem}</p>
                ))}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>

        
      </div>
    </div>
  );
};

export default InformationBlock;
