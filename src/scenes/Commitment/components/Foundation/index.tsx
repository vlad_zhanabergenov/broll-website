import React from 'react'


import { CloudImage } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrollFoundationPic from 'assets/pictures/brollFoundation'
import BrollFoundationIcon from 'assets/icons/brollFoundation.png'

const topList = [
  'Founded in 2002, the Broll Foundation represents the Group’s corporate social responsibility arm. We are as passionate about property as we are about the lives we touch in the process and the communities in which we operate.',
  'We do this through involvement and support of various organisations with the aim to improve the quality of life of the less fortunate in the communities in which we operate in.'
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.section1 }>
          <div className={ s.right }>
            <div className={ s.image }>
              <div className={ s.icon }>
                <img src={ BrollFoundationIcon } alt="broll" className='contained'/>
              </div>
              <CloudImage
                src={ BrollFoundationPic }
                alt="Broll"
                className='covered'
                responsive={{
                  desktop: { w: '1920' }
                }}
              />
            </div>
          </div>
          <div className={ s.left }>
            <h3 className='semiBig m-0 red bold mb-2'>Broll Foundation</h3>
            { topList.map((item, index) =>
              <React.Fragment key={ index }>
                <p className='light m-0 letterSpacingNormal'>{ item }</p>
              </React.Fragment>
            )}
            <p className='light m-0 letterSpacingNormal'>
              Contact the Broll Foundation: <a className='light red-2 m-0 letterSpacingNormal' href='mailto:brollfoundation@broll.com' target='_blank' rel='noopener noreferrer'>brollfoundation@broll.com</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
