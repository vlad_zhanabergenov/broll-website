import React from 'react'

import Academy from '../Academy/index'
import Foundation from '../Foundation/index'
import SocioEconomic from '../SocioEconomic/index'

import s from './style.module.sass'
import cn from 'classnames'

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.section1 }>
            <SocioEconomic />
        </div>
        <div className={ s.section2 }>
            <Foundation />
        </div>
        <div className={ s.section3 }>
            <Academy />
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
