import React from 'react'

import { CloudImage } from 'components'

import s from './style.module.sass'
import cn from 'classnames'

import BrollOurCommitment from 'assets/pictures/brollOurCommitment'

const topList = [
    'Black-women ownership 51% (Group).',
    'Level 1 contributor to B-BBEE (SA).',
    'BEE Procurement recognition level 135%.',
    'An active participant on the Property Sector Charter, sampling for research conducted annually to measure transformation in the property sector.',
    'Focused on Enterprise Development, Socio-Economic Development, Youth Employment, Employment Equity, Female Empowerment and Enterprise Supplier Development.',
    'Broll promotes sustainable economic growth and transformation through the support of black-owned entities in the mainstream of the South African Facilities Management sector'
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.section1 }>
          <div className={ s.left }>
            <h3 className='semiBig m-0 red bold mb-2'>To socio-economic sustainability</h3>
            { topList.map((item, index) =>
              <React.Fragment key={ index }>
                <p className='light m-0 letterSpacingNormal'>{ item }</p>
              </React.Fragment>
            )}
          </div>
          <div className={ s.right }>
            <div className={ s.image }>
              <CloudImage
                src={ BrollOurCommitment }
                alt="Broll"
                className='covered'
                responsive={{
                  desktop: { w: '1920' }
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
