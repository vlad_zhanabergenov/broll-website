import React from 'react'
import s from './style.module.sass'

interface Props {
  title: string
}

const TestScene: React.FC<Props> = ({ title }) => {
  return (
    <div className={ s.TestScene }>
      <h1 className="white text-center mt-0 mb-5 ph-3 box-sizing">{ title }</h1>
    </div>
  )
}

export default TestScene
