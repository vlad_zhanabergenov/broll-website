import React from 'react'


import { CloudImage } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrollFoundationPic from 'assets/pictures/brollFoundation'
import BrollFoundationIcon from 'assets/icons/brollFoundation.png'

const topList = [
  'Founded in 2002, the Broll Foundation represents the Group’s corporate social responsibility arm. We are as passionate about property as we are about the lives we touch in the process and the communities in which we operate.',
  'We do this through involvement and support of various organisations with the aim to improve the quality of life of the less fortunate in the communities in which we operate in.'
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.section1 }>
          <div className={ s.left }>
            { topList.map((item, index) =>
              <React.Fragment key={ index }>
                <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>{ item }</p>
                <p className='light no-desktop m-0 letterSpacingNormal'>{ item }</p>
              </React.Fragment>
            )}
            <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>
              Contact the Broll Foundation: <a className='small light red-2 m-0 letterSpacingNormal' href='mailto:brollfoundation@broll.com' target='_blank' rel='noopener noreferrer'>brollfoundation@broll.com</a>
            </p>
            <p className='light no-desktop m-0 letterSpacingNormal'>
              Contact the Broll Foundation: <a className='light red-2 m-0 letterSpacingNormal' href='mailto:brollfoundation@broll.com' target='_blank' rel='noopener noreferrer'>brollfoundation@broll.com</a>
            </p>
          </div>
          <div className={ s.right }>
            <div className={ s.image }>
              <div className={ s.icon }>
                <img src={ BrollFoundationIcon } alt="broll" className='contained'/>
              </div>
              <CloudImage
                src={ BrollFoundationPic }
                alt="Broll"
                className='covered'
                responsive={{
                  desktop: { w: '1920' }
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
