import ArrowIcon from 'assets/icons/arrowBoldWhite.svg'
import React from 'react'

import { CloudImage } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrollBusinessSolutionsSpecialistsPic from 'assets/pictures/brollBusinessSolutionsSpecialists'

const data = [
  { id: 1, title: 'Core Services', description: [
      'Cleaning and Specialised Cleaning', 'Hygiene, Deep Cleaning and Sanitising Services',
      'Landscaping, Horticulture and Garden Maintenance', 'Integrated Pest Management - Indoor Plants',
      'Advisory Technology Solutions', 'Accounts Payable Solutions'
    ]
  },
  { id: 2, title: 'Ancillary Services', description: [
      'Property Management', 'Facility Management', 'Broking', 'Auctions and Sales',
      'Occupier Services', 'Valuation and Advisory Services', 'Internal Developers (Workplace Strategy)',
      'Accounts Payable Management', 'Procurement', 'Security, Risk and Management Services',
      'Purpose Built Technology', 'Health, Safety and Security Risk Management'
    ]
  },
  { id: 3, title: 'Managed Allied Services', description: [
      'Confidential Document Shredding', 'Security Services', 'High Level Window Cleaning',
      'Canteen, Catering and Beverage Services', 'Waste Management', 'Vending Services'
    ]
  }
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.section1 }>
          <div className={ s.left }>
            <p className='white large light no-mobile no-tablet m-0 mt-7 letterSpacingNormal'>Business Solutions Specialists (BSS) is a multi-disciplinary, integrated and bundled Soft & Professional Services provider dedicated to the ever-changing needs of our clients.</p>
            <p className='white light no-desktop m-0 mt-7 letterSpacingNormal'>Business Solutions Specialists (BSS) is a multi-disciplinary, integrated and bundled Soft & Professional Services provider dedicated to the ever-changing needs of our clients.</p>
          </div>
          <div className={ s.right }>
            <div className={ s.image }>
              <CloudImage
                src={ BrollBusinessSolutionsSpecialistsPic }
                alt="Broll"
                className='covered'
                responsive={{
                  desktop: { w: '1920' }
                }}
              />
            </div>
          </div>
        </div>
        <div className={ s.section2 }>
          { data.map(item =>
            <div className={ s.item } key={ item.id }>
              <div className={ s.heading }>
                <div className={ s.line }/>
                <h4 className='m-0'>{ item.title }</h4>
              </div>
              { item.description.map((i, index) =>
                <div className={ s.i } key={ index }>
                  <div className={ s.icon }>
                    <img src={ ArrowIcon } alt="Broll" className='contained'/>
                  </div>
                  <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>{ i }</p>
                  <p className='light no-desktop m-0 letterSpacingNormal'>{ i }</p>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
