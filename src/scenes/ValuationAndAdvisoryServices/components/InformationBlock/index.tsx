import React from "react";
import s from "./style.module.sass";
import cn from "classnames";

import { CloudImage } from "components";
import TopImage from '../../../../assets/pictures/valuationAndAdvisoryServicesTop'

const topLeftList = [
  "We offer a highly professional service for the valuation of land and buildings, which includes real rights, servitudes and usufructs as well as plant and machinery assets. Valuations are also provided for sales and acquisitions, financial reporting, rental valuations, rating appeals and for insurance purposes. Additional services include due-diligence audits for property acquisitions and development appraisals.",
  "Our team includes Chartered Surveyors and Professional Valuers registered with both the South African Council for the Property Valuers Profession and the South African Institute of Valuers. We comply with international best practice as set out in the RICS Red Book and with the guidelines adopted by the International Valuation Standards Committee. ",
];

const topRightList = [
  "Royal Institution of Chartered Surveyors (RICS)",
  "International Valuations Standards Council (IVSC)",
  "South African Institution of Valuers (SAIV) ",
  "South African Council of Shopping Centre (SACSC)",
  "South African Property Owners Association (SAPOA)",
];

const leftList = [
  {
    title: "What we value",
    list: [
      "Offices",
      "Retail Centres",
      "Hotels",
      "Industrial",
      "Residential",
      "Development land",
      "Fuel Stations",
      "Specialised Properties",
    ],
  },
  {
    title: "Why we value",
    list: [
      "Sales and Acquisitions",
      "Financial reporting",
      "Objections",
      "Due Diligences",
      "Decision making",
      "Secured lending",
      "Processional Property Advise",
    ],
  },
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
            Valuation & Advisory Services.
          </h3>
          <div className={ cn(s.topImage, "mt-1") }>
            <CloudImage
              src={ TopImage }
              alt="Broll"
              className="covered"
              responsive={{
                desktop: { w: '1920' },
              }}
            />
          </div>
          <div className={s.container}>
            <div className={cn(s.left, "")}>
              <h3 className={cn(s.title, "semiBig bold red-4")}>
                Our Value Proposition
              </h3>
              {topLeftList.map((item) => (
                <p
                  className={cn(
                    s.text,
                    "white small light letterSpacingNormal mb-0"
                  )}
                >
                  {item}
                </p>
              ))}
            </div>
            <div className={cn(s.right)}>
              <h3 className={cn(s.title, "semiBig bold m-0 mb-1")}>
                Professional Industry Associations
              </h3>
              {topRightList.map((item, i) => (
                <p key={i} className="x-small regular">
                  {item}
                </p>
              ))}
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-4")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-1")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {leftList.map((item) => (
                  <div className={cn(s.block, " box-sizing")}>
                    <h4 className={cn(s.title, "bold red-4 m-0")}>
                      {item.title}
                    </h4>
                    {item.list.map((ListItem) => (
                      <p key={ListItem} className="x-small regular">
                        {ListItem}
                      </p>
                    ))}
                  </div>
                ))}
              </div>
            </div>
            <div className={cn(s.right, "pl-4 pr-4 box-sizing")}></div>
          </div>
        </div>

        <div className={cn(s.section3, "grey-2 pb-4")}>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>
                  Total value of properties valued
                </p>
                <h2 className={cn(s.title, "small bold m-0 mt-1")}>$6bn</h2>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0 mt-1")}>450</h2>
                <p className={cn(s.text, "small light")}>
                  Real estate valuation and advisory assignments p/a
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
