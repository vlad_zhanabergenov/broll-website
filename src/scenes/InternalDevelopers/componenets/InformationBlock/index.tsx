import ContactBannerPic from 'assets/pictures/contactBanner'
import React from 'react'

import { ContactBanner, CloudImage } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrollInternalDevelopersBannerPic from 'assets/pictures/brollInternalDevelopersBanner'
import Icon1 from 'assets/icons/InternalDevelopersIcon1.png'
import Icon2 from 'assets/icons/InternalDevelopersIcon2.png'
import Icon3 from 'assets/icons/InternalDevelopersIcon3.png'
import Icon4 from 'assets/icons/InternalDevelopersIcon4.png'
import Icon5 from 'assets/icons/InternalDevelopersIcon5.png'


const firstData = [
  { id: 1, title: "Workplace <br/>Assessment", icon: Icon1  },
  { id: 2, title: "Workplace Strategy", icon: Icon2  },
  { id: 3, title: "Workplace <br/>Design", icon: Icon3  },
  { id: 4, title: "Workplace Projects", icon: Icon4  },
  { id: 5, title: "Workplace Change", icon: Icon5  }
]

const bannerData = {
  name: 'Lianie Minny',
  title: 'Managing Director, Internal Developers',
  img: ContactBannerPic,
  email: 'lminny@id.work',
  number: '+27 87 700 8290'
}


const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ cn(s.section1) }>
          <div className={ s.image }>
            <CloudImage
              src={ BrollInternalDevelopersBannerPic }
              alt="Broll Broking"
              className='covered'
              responsive={{
                desktop: { w: '1920' }
              }}
            />
          </div>
          <div className={ s.line }/>
        </div>
        <div className={ s.section2 }>
          <p className='light small m-0 no-mobile no-tablet letterSpacingNormal'>We are 360 workplace strategists: a team of multi-disciplinary consultants. We consider where and how people work; and how it relates to their business culture, efficiency, and profitability and how to best optimise their real estate. Our combined knowledge from the real estate and built-environment provide our clients with data- led information to develop a future-proof workplace strategy - whether it be stay-vs-go, optimisation, consolidation or expansion. Our team can manage workplace projects of any scale for commercial and retail premises refurbishment, fit-out, or renovations throughout South Africa. We deliver a total end-to-end business solution from inception to implementation. <br/>For more on Internal Developers visit: https://id.work/</p>
          <p className='light m-0 no-desktop letterSpacingNormal'>We are 360 workplace strategists: a team of multi-disciplinary consultants. We consider where and how people work; and how it relates to their business culture, efficiency, and profitability and how to best optimise their real estate. Our combined knowledge from the real estate and built-environment provide our clients with data- led information to develop a future-proof workplace strategy - whether it be stay-vs-go, optimisation, consolidation or expansion. Our team can manage workplace projects of any scale for commercial and retail premises refurbishment, fit-out, or renovations throughout South Africa. We deliver a total end-to-end business solution from inception to implementation. <br/>For more on Internal Developers visit: https://id.work/</p>
          <h3 className='semiBig red-2 bold mt-4 mb-0'>Our services include</h3>
          <div className={ s.list }>
            { firstData.map(item =>
                <div className={ s.item } key={ item.id }>
                  <div className={ s.wrapper }>
                    <div className={ s.icon }>
                      <img src={ item.icon } alt="broll" className='contained'/>
                    </div>
                  </div>
                  <div className={ s.title }>
                    <p className={ cn('small medium no-mobile no-tablet m-0 letterSpacingNormal') } dangerouslySetInnerHTML={{ __html: item.title }}/>
                    <p className={ cn('medium no-desktop m-0 letterSpacingNormal') } dangerouslySetInnerHTML={{ __html: item.title }}/>
                  </div>
                </div>
            )}
          </div>
        </div>
        <div className={ cn(s.section3, 'no-tablet no-mobile') }>
          <ContactBanner data={ bannerData } noLine/>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
