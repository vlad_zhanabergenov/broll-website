import React from "react";
import CloudImage from "components/CloudImage";
import img from '../../../../assets/pictures/strategyAndConsalting'
import s from "./style.module.sass";
import cn from "classnames";

const topLeftList = [
  "In recent years the utilisation of real estate has evolved for people and institutions from being seen as a facilitator of economic activity, to one that is a crafter of wealth and can be used to achieve a range of purposes. Market requirements are increasingly complex, requiring a combination of solutions that dictate a high degree of business acumen to every assignment.",
  "This growing service provides cross-functional integration in crafting bespoke solutions for investors and occupiers. Our strategic delivery is aimed at delivering the most favourable outcomes, aligned with an organisation’s long-term strategy and workplace goals. We unite the excellence of our daily deal-making, market intelligence and innovative strategies to deliver portfolio solutions that enable easier client oversight, better control and optimise value.",
  "With our unparalleled track record and experience, we focus on every opportunity to increase asset performance.",
  "Our established methodology manages risk and facilitates team integration through technology, facilitating real estate solutions that direct your decision-making.",
];

const leftList = [
  "Portfolio optimisation & transformation",
  "Global portfolio expansion",
  "Balance sheet restructures using real estate",
  "B-BBEE property strategies",
  "Site selection/location advisory services",
  "Tenant retail strategy",
  "Network planning and location pricing",
  "Asset management strategies",
];

const rightList = [
  "Asset investment committee advisory",
  "Portfolio performance analysis",
  "Capital expenditure business cases",
  "Buy - and sell - side due diligence",
  "Financial Reporting",
  "Data Management Solutions",
  "Location Analysis & Network Planning",
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
            Unparalleled market intelligence.
          </h3>
          <div className={s.container}>
            <div className={cn(s.left, "")}>
              <h3 className={cn(s.title, "semiBig bold red-4")}>
                Our Value Proposition
              </h3>
              {topLeftList.map((item) => (
                <p
                  className={cn(
                    s.text,
                    "white small light letterSpacingNormal mb-0"
                  )}
                >
                  {item}
                </p>
              ))}
            </div>
            <div className={cn(s.right)}>
              <CloudImage
                src={img}
                alt="Broll"
                className="covered"
                responsive={{
                  desktop: { w: "1920" },
                }}
              />
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-4")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-1")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {leftList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
            <div className={cn(s.right, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {rightList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className={cn(s.section3, "grey-2 pb-4")}>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>
                  Trusted partners on strategy and asset management across
                </p>
                <h2 className={cn(s.title, "small bold m-0")}>Africa</h2>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>
                  Advisory on over
                </p>
                <h2 className={cn(s.title, "small bold m-0")}>1.5m m²</h2>
                <p className={cn(s.text, "small light")}>portfolios annually</p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>
                  Strategies realising in savings
                </p>
                <h2 className={cn(s.title, "small bold m-0")}>$704m</h2>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 pt-1 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0")}>+100</h2>
                <p className={cn(s.text, "small light")}>
                  Business cases approved p/a
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>Up to</p>
                <h2 className={cn(s.title, "small bold m-0")}>90%</h2>
                <p className={cn(s.text, "small light")}>
                  footprint optimisations achieved
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light m-0 mt-1")}>Up to</p>
                <h2 className={cn(s.title, "small bold m-0")}>52%</h2>
                <p className={cn(s.text, "small light")}>
                  increase in portfolio income achieved
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
