import React from 'react'

import { Breadcrumbs, SearchBar } from 'components'
import { InformationBlock } from './components'

import s from './style.module.sass'
import cn from 'classnames'

const StrategyConsultingScene = () => {
  return (
    <div className={ s.StrategyConsultingScene }>
      <div className={ s.layout }>
        <div className={ cn(s.section1, 'pt-2 ph-17 pb-4  box-sizing') }>
          <Breadcrumbs items={ ['Services', 'Strategy Consulting'] } className="mb-2"/>
          <div className={ cn(s.searchBar, 'pl-1 pr-3 box-sizing') }>
            <SearchBar/>
          </div>
        </div>
        <div className={ cn(s.section2, 'pl-14 pr-12 pb-8 box-sizing') }>
          <h2 className={ cn('thin m-0 mb-1') }>Strategy & Consulting</h2>
          <InformationBlock/>
        </div>
      </div>
    </div>
  )
}

export default StrategyConsultingScene
