import React from 'react'

import { CloudImage } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrokingPic from 'assets/pictures/broking'

const brokingList = [
  "Access to a wide range of value-adding services",
  "Comprehensive in-house research and analysis capabilities",
  "Knowledge-driven team-based approach to solving your property needs",
  "National and Sub-Saharan Africa footprints",
  "Unmatched database of high quality office and industrial properties available to let",
  "Working relationships with major listed and unlisted property funds"
]

const infoList = [
  {
    id: 1,
    title: 'Industrial leasing',
    descriptions: [
      'Finding the right industrial property or space often involves a combination of complex technical considerations which include electricity supply, access to distribution hubs, airports and rail links, turning circles for large vehicles, load-bearing floors and roller shutter doors and high eaves among others.',
      'All have to be taken into account and finding the property with the perfect combination of elements can get very complicated.',
      'Save time and let the Broll team of industrial experts find the right property for you.'
    ]
  },
  {
    id: 2,
    title: 'Investment broking',
    descriptions: [
      "Our dedicated team of investment property professionals can assist with properties within and beyond South Africa through our global network partners.",
      "If you're looking to expand or reduce your property portfolio, Broll has the market knowledge and connections to help you.",
      "Broll has an in-depth understanding of the commercial property market, the experience and the knowledge needed to correctly manage investment property.",
      "Our property management skills are well respected within the industry."
    ]
  },
  {
    id: 3,
    title: 'Office leasing',
    descriptions: [
      "Finding the right offices to let is not just about square metres and parking ratios.",
      "It's more about the office space meeting the tenant's total business needs. With an experienced team and comprehensive database of available rental properties for commercial use and commercial properties for sale, Broll definitely has the resources to make that match for you",
      "Some of the world's leading multinational corporations trust us to act as their letting agents, shouldn't you?"
    ]
  }
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.top }>
          <div className={ s.left }>
            <h3 className='bold semiBig m-0 red-4'>Broking</h3>
            <p className='small white light m-0 no-mobile no-tablet letterSpacingNormal'>When it comes to commercial property broking, we always put our clients' needs first. We have:</p>
            <p className='white light m-0 no-desktop letterSpacingNormal'>When it comes to commercial property broking, we always put our clients' needs first. We have:</p>
            <div className={ s.list }>
              { brokingList.map(item =>
                <React.Fragment key={ item }>
                  <p className='small white light mt-0 no-mobile no-tablet letterSpacingNormal'>{ item }</p>
                  <p className='white light mt-0 no-desktop letterSpacingNormal'>{ item }</p>
                </React.Fragment>
              )}
            </div>
            <p className='small light white m-0 no-mobile no-tablet letterSpacingNormal'>
              Our <span className='light small red-2 letterSpacingNormal'>services</span> include those highlighted below:
            </p>
            <p className='light white m-0 no-desktop letterSpacingNormal'>Our <span className='light red-2 letterSpacingNormal'>services</span> include those highlighted below:</p>
          </div>
          <div className={ s.right }>
            <div className={ s.image }>
              <CloudImage
                src={ BrokingPic }
                alt="Broll Broking"
                className='covered'
                responsive={{
                  desktop: { w: '1920' }
                }}
              />
            </div>
          </div>
        </div>
        <div className={ s.bottom }>
          { infoList.map(item =>
            <div className={ s.item } key={ item.id }>
              <div className={ s.header }>
                <div className={ s.line }/>
                <h4 className='m-0 bold'>{ item.title }</h4>
              </div>
              <div className={ s.list }>
                { item.descriptions.map(item =>
                  <React.Fragment key={ item }>
                    <p className='small light m-0 no-tablet no-mobile letterSpacingNormal'>{ item }</p>
                    <p className='light m-0 no-desktop letterSpacingNormal'>{ item }</p>
                  </React.Fragment>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
