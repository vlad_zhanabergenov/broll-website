import React from 'react'

import { Breadcrumbs, SearchBar, ContactCard } from 'components'
import { InformationBlock } from './componenets'

import s from './style.module.sass'
import cn from 'classnames'

import Pic from 'assets/pictures/noPropertyImage'

const data = [
  { id: 1, name: 'Rodney Luntz', title: 'Head of Commercial Broking Gauteng', img: Pic, email: 'rluntz@broll.com', number: '+27 87 700 8290' },
  { id: 2, name: 'Sean Berowsky', title: 'Head of Broking South Africa', img: Pic, email: 'berowsky@broll.com', number: '+27 87 700 8290' },
  { id: 3, name: 'Anthon van Weers', title: 'Senior Industrial Broker KwaZulu-Natal', img: Pic, email: 'avanweers@broll.com', number: '+27 87 700 8290' },
  { id: 4, name: 'Sean Berowsky', title: 'Head of Broking South Africa', img: Pic, email: 'berowsky@broll.com', number: '+27 87 700 8290' }
]

const BrokingScene = () => {
  return (
    <div className={ s.BrokingScene }>
      <div className={ s.layout }>
        <div className={ cn(s.section1, 'pt-2 ph-17 pb-4  box-sizing') }>
          <Breadcrumbs items={ ['Services', 'Broking'] } className="mb-2"/>
          <div className={ cn(s.searchBar, 'pl-1 pr-3 box-sizing') }>
            <SearchBar/>
          </div>
        </div>
        <div className={ cn(s.section2, 'pl-14 pr-12 pb-4 box-sizing') }>
          <h2 className={ cn('thin m-0 mb-3') }>Broking</h2>
          <InformationBlock/>
          <div className={ cn(s.cards, 'no-mobile no-tablet') }>
            { data.map(item =>
              <ContactCard data={ item } key={ item.id }/>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default BrokingScene
