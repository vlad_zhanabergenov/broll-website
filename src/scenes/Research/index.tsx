import React from 'react'

import { Breadcrumbs, SearchBar } from 'components'
import { InformationBlock } from './componenets'

import s from './style.module.sass'
import cn from 'classnames'

const ResearchScene = () => {
  return (
    <div className={ s.ResearchScene }>
      <div className={ s.layout }>
        <div className={ cn(s.section1, 'pt-2 ph-17 pb-4  box-sizing') }>
          <Breadcrumbs items={ ['Services', 'Research'] } className="mb-2"/>
          <div className={ cn(s.searchBar, 'pl-1 pr-3 box-sizing') }>
            <SearchBar/>
          </div>
        </div>
        <div className={ cn(s.section2, 'pl-14 pr-12 pb-8 box-sizing') }>
          <h2 className={ cn('thin m-0 mb-3') }>Research</h2>
          <InformationBlock/>
        </div>
      </div>
    </div>
  )
}

export default ResearchScene
