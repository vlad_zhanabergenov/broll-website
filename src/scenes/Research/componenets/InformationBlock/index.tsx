import React from 'react'


import { Banner, ContactBanner } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrollResearchPic from 'assets/pictures/brollResearch'
import ContactBannerPic from 'assets/pictures/contactBanner'

const list = [
  { id: 1, title: 'Knowledge is power', description: [
      'As we move towards our goal of being a leading property services provider in Sub-Saharan Africa, we support research on an African scale. Locally, we will continue to service the research needs of the real estate sector through innovative storytelling while embracing technology in various aspects of our business.',
      'Our research department focuses on providing our clients with knowledge base research about the retail, office and industrial property sectors.',
      'Broll research specialises in converting property data into market knowledge.',
      'We pride ourselves on the fact that we can add value to our clients’ portfolios, hence empowering them to make well informed decisions and improve the performance of their investments.',
      'We use the comprehensive database of buildings under our management as well as the Broll Broking database of actual deals concluded in various markets as the basis of all our research.'
    ]
  },
  { id: 2, title: 'Who needs property research?', description: [
      'Anyone involved in the property industry, from landlords to tenants and investors to property professionals can benefit from the superior information that research provides.'
    ]
  }
]

const bannerData = {
  name: 'Elaine Wilson',
  title: 'Head of Research',
  img: ContactBannerPic,
  email: 'ewilson@broll.com',
  number: '+27 87 700 8290'
}

const  InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.section1 }>
          <div className={ s.left }>
            { list.map(item =>
              <div className={ s.item } key={ item.id }>
                <h3 className='semiBig m-0 red-2 bold mb-1'>{ item.title }</h3>
                { item.description.map((item, index) =>
                  <React.Fragment key={ index }>
                    <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>{ item }</p>
                    <p className='light no-desktop m-0 letterSpacingNormal'>{ item }</p>
                  </React.Fragment>
                )}
              </div>
            )}
          </div>
          <div className={ s.right }>
            <Banner image={ BrollResearchPic } description='We convert <br/>property data <br/>into market <br/>knowledge.'/>
          </div>
        </div>
        <div className={ cn(s.section2, 'no-tablet no-mobile') }>
          <ContactBanner data={ bannerData } noLine/>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
