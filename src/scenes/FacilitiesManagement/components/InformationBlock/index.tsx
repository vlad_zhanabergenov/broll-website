import React from "react";
import s from "./style.module.sass";
import cn from "classnames";

import { CloudImage } from "components";
import TopImage from '../../../../assets/pictures/facilitiesManagementTop'

const topLeftList = [
  "To boost profitability and achieve new cost efficiencies in today’s competitive marketspace, companies have to focus more on their core business functions and outsource their non-core activities to facilities management service providers. Integrated facilities management, is one of the best ways to consolidate all services under a single management team and contract.  ",
  "Integrating with your systems and business processes, we deliver a solution that saves costs, improves asset utilisation and overall service delivery. Whilst providing accredited specialists and Broll personnel to perform the specific services required.",
];

const topRightList = [
  "South African Facilities Management Association (SAFMA)",
  "South African Council of Shopping Centre (SACSC)",
  "South African Property Owners Association (SAPOA)",
  "Green Buildings South Africa (GBSA)",
];

const leftList = [
  "Integrated Technology Platform",
  "ISO certified Facilities Management processes",
  "Hard Services:",
  "HVAC/Generators/UPS/",
  "Electrical/Plumbing",
  "Professional Services",
  "Occupational Health and Safety Consulting Services",
];

const rightList = [
  "Call Centre and 24/7 Help Desk",
  "Occupational Health and Safety Consulting Services",
  "Statutory Compliance Audits",
  "Asset Condition Audits and Total Lifecycle Management",
  "Asset Tagging and Verification",
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
            Comprehensive solution for all onsite facilities related activities.
          </h3>
          <div className={ cn(s.topImage, "mt-1") }>
            <CloudImage
              src={ TopImage }
              alt="Broll"
              className="covered"
              responsive={{
                desktop: { w: '1920' },
              }}
            />
          </div>
          <div className={s.container}>
            <div className={cn(s.left, "")}>
              <h3 className={cn(s.title, "semiBig bold red-4")}>
                Our Value Proposition
              </h3>
              {topLeftList.map((item) => (
                <p
                  className={cn(
                    s.text,
                    "white small light letterSpacingNormal mb-0"
                  )}
                >
                  {item}
                </p>
              ))}
            </div>
            <div className={cn(s.right)}>
              <h3 className={cn(s.title, "semiBig bold m-0 mb-1")}>
                Professional Industry Associations
              </h3>
              {topRightList.map((item, i) => (
                <p key={i} className="x-small regular">
                  {item}
                </p>
              ))}
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-4")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-1")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {leftList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
            <div className={cn(s.right, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {rightList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className={cn(s.section3, "grey-2 pb-4")}>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0 mt-1")}>Broll Vantage</h2>
                <p className={cn(s.text, "small light")}>
                  Supplier Portal, Online Vetting Process, RFQs, Invoicing,
                  Approvals, Integrated to finance system, reporting.
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0 mt-1")}>4,000</h2>
                <p className={cn(s.text, "small light")}>
                  Help Centre Calls p/m
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0")}>7,000</h2>
                <p className={cn(s.text, "small light")}>Suppliers</p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light mt-1")}>Certified TÜV Rheinland</p>
                <h2 className={cn(s.title, "small bold m-0")}>ISO QMS, OHS & EMS</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
