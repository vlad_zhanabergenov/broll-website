import { resetFields } from 'cache/mutations/search'
import { useDeviceType } from 'hooks'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { useReactiveVar } from '@apollo/client'
import { searchVar } from 'cache/vars'
import searchStateParser from 'services/searchStateParser'

import { Breadcrumbs, Disclaimer, Button, SearchBar, Pagination } from 'components'
import { SearchFilters, SearchFiltersModal, Results } from './components'

import cn from 'classnames'
import s from './style.module.sass'
import FiltersIcon from 'assets/icons/filters.svg'

const SearchScene: React.FC = () => {
  const deviceType = useDeviceType()
  const router = useRouter()
  const searchState = useReactiveVar(searchVar)
  const [mount, setMount] = useState(false)
  const [showFiltersModal, setShowFiltersModal] = useState(false)

  useEffect(() => {
    if (router.isReady && (!mount || !showFiltersModal)) { // "|| !showFiltersModal" is 1st part fix for filters unmount on mobile
      if (Object.keys(router.query).length) {
        const searchStateFromParams = searchStateParser.decode(router.query)
        resetFields(searchStateFromParams)
      }
      setMount(true)
    }
  }, [router, showFiltersModal])

  useEffect(() => {
    if (mount) {
      const newUrl = `${ window.location.pathname }?${ searchStateParser.encode(searchState) }`

      if (router.asPath !== newUrl) { // this is 2nd part fix for filters unmount on mobile
        router.push(newUrl, undefined, { shallow: true })
      }
    }
  }, [searchState])

  return (
    <div className={ s.SearchScene }>
      <div className={ cn(s.layout, "pt-2 ph-content box-sizing") }>
        <div className={ cn(s.searchBar, "ph-10 mb-1 box-sizing") }>
          <Breadcrumbs items={ searchState.dealType ? [searchState.dealType] : ['Search'] }/>
        </div>
        { deviceType !== 'mobile' &&
          <div className={ cn(s.searchBar, "ph-10 box-sizing no-mobile") }>
            <SearchBar/>
          </div>
        }
        { deviceType === 'mobile' &&
          <div className={ cn(s.searchBar, "ph-10 box-sizing no-desktop no-tablet") }>
            <SearchBar/>
            { showFiltersModal &&
              <SearchFiltersModal onClose={ () => setShowFiltersModal(false) }/>
            }
          </div>
        }
        <div className={ cn(s.content, "pt-3") }>
          <div className={ cn(s.sidebar, "pt-3 pb-2 no-mobile") }>
            <SearchFilters/>
          </div>
          <div className={ cn(s.results) }>
            <Results/>
            <Pagination className="mt-3"/>
          </div>
        </div>
        { deviceType === 'mobile' &&
          <div className={ cn(s.filterButtonContainer, "no-desktop no-tablet ph-8 box-sizing") }>
            <div className={ s.layer }/>
            <Button className={ cn(s.filterButton) } rounded icon={ FiltersIcon } iconSize='l'
                    onClick={ () => setShowFiltersModal(true) }>
              Filter
            </Button>
          </div>
        }
      </div>
      <Disclaimer/>
    </div>
  )
}

export default SearchScene
