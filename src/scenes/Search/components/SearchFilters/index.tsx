import React from 'react'
import { useReactiveVar } from '@apollo/client'
import { setField, resetFields } from 'cache/mutations/search'
import { appVar, searchVar } from 'cache/vars'
import { useForm } from 'hooks'
import { connectStateResults } from 'react-instantsearch-core'
import compareSearchFilters from 'services/compareSearchFilters'
import { searchIndexes, searchOptions } from 'utilities/constants'
import initialState from 'cache/initialState'

import { CategoryFilter, CityFilter, SuburbFilter, BrokerFilter } from './components'
import { Radio, PriceFilter, MinGLAFilter, MaxGLAFilter } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'dealType'>

const SearchFilters: React.FC = () => {
  const { algoliaReady, algoliaStats } = useReactiveVar(appVar)
  const searchState = useReactiveVar(searchVar)
  const dealTypeCounts = [algoliaStats.nbHits[searchIndexes.Buy], algoliaStats.nbHits[searchIndexes.Rent]]
  const isFiltersDefault = compareSearchFilters(searchState, initialState.search)

  const onChange = (_name: keyof Options, value: any) => {
    setField('dealType', value)
  }

  const { values, change } = useForm<Options>({
    fields: {
      dealType: { initialValue: searchState.dealType }
    }, onChange
  }, [searchState.dealType])

  const handleResetFilters = () => {
    if (!isFiltersDefault) {
      resetFields({ ...initialState.search, dealType: searchState.dealType })
    }
  }

  if (!algoliaReady) {
    return null
  }

  return (
    <div className={ cn(s.SearchFilters) }>
      <div className={ s.layout }>
        <h3 className="no-mobile big bold mt-0 mb-1">Filter</h3>
        {/*<SearchStats/>*/}
        <div className={ cn(s.resetButton, "no-mobile cursor-pointer display-inline mv-1", { [s.disabled]: isFiltersDefault }) }>
          <p className="light m-0" onClick={ handleResetFilters }>Reset</p>
        </div>
        <div key={ values.dealType } className={ cn(s.form, "mv-2") }>
          <Radio name="dealType" className="no-mobile" options={ searchOptions.dealType } textAfter={ dealTypeCounts }
                 value={ values.dealType } onChange={ change }/>
          <div className={ cn(s.divider, "no-mobile mv-2") }/>
          <CategoryFilter/>
          <CityFilter/>
          <SuburbFilter/>
          <PriceFilter secondary/>
          <div className={ cn(s.divider, "mv-1") }/>
          <MinGLAFilter secondary className="mb-05"/>
          <MaxGLAFilter secondary/>
          <div className={ cn(s.divider, "mv-1") }/>
          <BrokerFilter/>
        </div>
      </div>
    </div>
  )
}

export default connectStateResults(SearchFilters)
