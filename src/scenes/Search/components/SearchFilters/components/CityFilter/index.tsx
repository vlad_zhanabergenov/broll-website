import { useReactiveVar } from '@apollo/client'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { useForm } from 'hooks'
import { isEqual, sortBy } from 'lodash'
import React, { useEffect, useState } from 'react'
import { RefinementListProvided } from 'react-instantsearch-core'
import { connectRefinementList } from 'react-instantsearch-dom'

import { Input, Radio } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'query' | 'city'>

const maxCitiesToShow = 4

const CityFilterRaw: React.FC<RefinementListProvided> = ({
  items,
  searchForItems,
  currentRefinement, refine
}) => {
  const searchState = useReactiveVar(searchVar)
  const { city } = searchState

  const [limit, setLimit] = useState(true)
  const sortedItems = sortBy(items, ['count']).reverse()
  const cities = sortedItems.slice(0, limit ? maxCitiesToShow : sortedItems.length).map(item => item.label)
  const citiesCounts = sortedItems.map(item => item.count)

  useEffect(() => {
    if (!currentRefinement || !isEqual(currentRefinement, city)) {
      refine(city)
    }
  }, [searchState])

  const onChange = (name: string, value: any) => {
    if (name === 'query') {
      searchForItems(value)
    } else {
      setField('city', value)
    }
  }

  const searchForm = useForm<Pick<Options, 'query'>>({
    fields: {
      query: { initialValue: "" }
    }, onChange
  }, [city])

  const { values, change } = useForm<Options>({
    fields: {
      city: { initialValue: city }
    }, onChange
  }, [city])

  return (
    <div className={ cn(s.CityFilter) }>
      <Input secondary name="query" className="mv-2" placeholder="Search by city" type="search"
             value={ searchForm.values.query } onChange={ searchForm.change }/>
      <Radio multi className="mb-2" name="city" options={ cities } textAfter={ citiesCounts }
             value={ values.city } onChange={ change }/>
      { items.length > maxCitiesToShow &&
        <p className="bold cursor-pointer text-center mt-2 mb-0" onClick={ () => setLimit(!limit) }>
          { cities.length < items.length ? "View more" : "Show fewer" }
        </p>
      }
      <div className={ cn(s.divider, "mt-05 mb-2") }/>
    </div>
  )
}

const CityFilterConnected = connectRefinementList(CityFilterRaw)

const CityFilter: React.FC = () => (
  <CityFilterConnected attribute='city' limit={ 1000 }/>
)

export default CityFilter
