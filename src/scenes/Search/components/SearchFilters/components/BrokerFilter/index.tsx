import { useReactiveVar } from '@apollo/client'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { useForm } from 'hooks'
import { isEqual, sortBy } from 'lodash'
import React, { useEffect, useState } from 'react'
import { RefinementListProvided } from 'react-instantsearch-core'
import { connectRefinementList } from 'react-instantsearch-dom'

import { Input, Radio } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'query' | 'broker'>

const maxBrokersToShow = 4

const BrokerFilterRaw: React.FC<RefinementListProvided> = ({
  items,
  searchForItems,
  currentRefinement, refine
}) => {
  const searchState = useReactiveVar(searchVar)
  const { broker } = searchState

  const [limit, setLimit] = useState(true)
  const sortedItems = sortBy(items, ['count']).reverse()
  const brokers = sortedItems.slice(0, limit ? maxBrokersToShow : sortedItems.length).map(item => item.label)
  const brokerUnitCount = sortedItems.map(item => item.count)

  useEffect(() => {
    if (!currentRefinement || !isEqual(currentRefinement, broker)) {
      refine(broker)
    }
  }, [searchState])

  const onChange = (name: string, value: any) => {
    if (name === 'query') {
      searchForItems(value)
    } else {
      setField('broker', value)
    }
  }

  const searchForm = useForm<Pick<Options, 'query'>>({
    fields: {
      query: { initialValue: "" }
    }, onChange
  }, [broker])

  const { values, change } = useForm<Options>({
    fields: {
      broker: { initialValue: broker }
    }, onChange
  }, [broker])

  return (
    <div className={ cn(s.BrokerFilter) }>
      <Input secondary name="query" className="mv-2" placeholder="Search by broker" type="search"
             value={ searchForm.values.query } onChange={ searchForm.change }/>
      <Radio multi className="mb-2" name="broker" options={ brokers } textAfter={ brokerUnitCount }
             value={ values.broker } onChange={ change }/>
      { items.length > maxBrokersToShow &&
        <p className="bold cursor-pointer text-center mt-2 mb-0" onClick={ () => setLimit(!limit) }>
          { brokers.length < items.length ? "View more" : "Show fewer" }
        </p>
      }
      <div className={ cn(s.divider, "mt-05 mb-2") }/>
    </div>
  )
}

const BrokerFilterConnected = connectRefinementList(BrokerFilterRaw)

const BrokerFilter: React.FC = () => (
  <BrokerFilterConnected attribute='property_responsibility.name' limit={ 1000 }/>
)

export default BrokerFilter
