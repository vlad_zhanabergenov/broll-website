import { useReactiveVar } from '@apollo/client'
import { searchVar } from 'cache/vars'
import React from 'react'
import initialState from 'cache/initialState'
import { resetFields } from 'cache/mutations/search'
import compareSearchFilters from 'services/compareSearchFilters'

import { SearchFilters } from '../../components'
import { SearchBar, Button, Modal } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  onClose: () => void
}

const SearchFiltersModal: React.FC<Props> = ({ onClose }) => {
  const searchState = useReactiveVar(searchVar)

  const isFiltersDefault = compareSearchFilters(searchState, initialState.search)

  const handleResetFilters = () => {
    resetFields()
  }

  return (
    <Modal onClose={ onClose }>
      <div className={ s.SearchFiltersModal }>
        <div className={ s.layout }>
          <div className={ s.wrapper }>
            <SearchBar reverse/>
            <SearchFilters/>
            <div className={ cn(s.buttons, "mt-1 ph-4 box-sizing") }>
              <Button rounded onClick={ onClose }>Search</Button>
              <Button rounded secondary onClick={ handleResetFilters } disabled={ isFiltersDefault }>Reset</Button>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default SearchFiltersModal
