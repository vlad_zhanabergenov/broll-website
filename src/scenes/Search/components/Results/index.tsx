import { useReactiveVar } from '@apollo/client'
import React, { useEffect, useState } from 'react'
import { HitsProvided } from 'react-instantsearch-core'
import { connectHits } from 'react-instantsearch-dom'
import { appVar, searchVar } from 'cache/vars'
import { resetFields } from 'cache/mutations/search'
import initialState from 'cache/initialState'
import addDealTypeField from 'services/addDealTypeField'
import groupPropertiesByGmavenKey from 'services/groupPropertiesByGmavenKey'

import { PropertyCard } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

const Results: React.FC<HitsProvided<AnyProperty>> = ({ hits }) => {
  const { algoliaReady } = useReactiveVar(appVar)
  const { dealType } = useReactiveVar(searchVar)
  const [list, setList] = useState<AnyProperty[]>([])

  useEffect(() => {
    setList(groupPropertiesByGmavenKey(addDealTypeField(hits)))
  }, [hits])


  const handleResetFilters = () => {
    resetFields({ ...initialState.search, dealType })
  }

  if (!algoliaReady) {
    return null
  }

  return (
    <div className={ cn(s.Results) }>
      <div className={ s.layout }>
        { hits.length ?
          <div className={ s.list }>
            { list.map(item =>
              <PropertyCard key={ item.objectID } data={ item }/>
            )}
          </div>
        :
          <h3 className="medium mv-10 text-center">Your search returned no results. Please <span className='cursor-pointer bold red' onClick={ handleResetFilters }>reset</span> your filter and try again.</h3>
        }
      </div>
    </div>
  )
}

export default connectHits(Results)
