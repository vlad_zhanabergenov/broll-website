import { useRouter } from 'next/router'
import React from 'react'
import { ExperimentalConfigureRelatedItems, Index } from 'react-instantsearch-core'

import { Breadcrumbs, SearchBar, Post, Icon } from 'components'
import { RelatedNews } from './components'

import s from './style.module.sass'
import cn from 'classnames'
import ArrowIcon from 'assets/icons/arrow.svg'

interface Props {
  newsPost: NewsMediaItem
}

const NewsPostRaw: React.FC<Props> = ({ newsPost }) => {
  const router = useRouter()

  const handleBack = () => {
    router.push('/media-centre/latest-news')
  }

  return (
    <div className={ s.NewsPostScene }>
      <div className={ s.layout }>
        <div className={ cn(s.section1, 'pt-2 ph-17 pb-4 box-sizing') }>
          <Breadcrumbs items={ ['Media Centre', 'Latest news'] } className="mb-2"/>
          <div className={ cn(s.searchBar, 'pl-1 pr-3 box-sizing') }>
            <SearchBar/>
          </div>
        </div>
        <div className={ cn(s.section2, 'ph-content pb-2 box-sizing') }>
          <h2 className={ cn('thin m-0 mb-2') }>Latest News</h2>
          <div className={ cn(s.backButton, 'cursor-pointer hover-opacity') } onClick={ handleBack }>
            <div className={ s.icon }>
              <Icon src={ ArrowIcon } rotate={ 270 } size='s'/>
            </div>
            <p className='small m-0 pl-1'><span className='red bold'>Back</span> to news results</p>
          </div>
        </div>
        <div className={ cn(s.section3, 'ph-content box-sizing') }>
          <Post data={ newsPost }/>
        </div>
        <div className={ s.section4 }>
          <h2 className={ cn('thin m-0 mb-3') }>Related</h2>
          <RelatedNews/>
        </div>
      </div>
    </div>
  )
}


const NewsPostScene: React.FC<Props> = (props) => {
  return (
    <Index indexName='News' indexId='NewsPostScene'>
      <ExperimentalConfigureRelatedItems
        hit={{
          fields: {
            articleSummary: { 'en-US': props.newsPost.articleSummary },
            category: { 'en-US': props.newsPost.category },
            featuredArticle: { 'en-US': false }
          }
        }}
        matchingPatterns={{
          "fields.category.en-US": { score: 1 },
          "fields.articleSummary.en-US": { score: 2 }
        }}
        distinct={ 1 }
      />
      <NewsPostRaw { ...props }/>
    </Index>
  )
}

export default NewsPostScene
