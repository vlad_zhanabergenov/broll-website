import { useReactiveVar } from '@apollo/client'
import { appVar } from 'cache/vars'
import React, { useEffect, useState } from 'react'
import { useDeviceType } from 'hooks'
import { HitsProvided } from 'react-instantsearch-core'
import { connectHits } from 'react-instantsearch-dom'
import Slider, { Settings } from 'react-slick'
import parseMediaItem from 'services/parseMediaItem'

import { Link, MediaCard } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

const settings: Settings = {
  className: "slickSlider",
  centerMode: true,
  centerPadding: "0",
  slidesToShow: 3,
  initialSlide: 1,
  speed: 500,
  responsive: [
    {
      breakpoint: 960,
      settings: {
        initialSlide: 2,
        slidesToShow: 1,
        centerPadding: "168.5px"
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        initialSlide: 2,
        centerPadding: "60px"
      }
    }
  ]
}

const RelatedNews: React.FC<HitsProvided<any>> = ({ hits }) => {
  const deviceType = useDeviceType()
  const { algoliaReady } = useReactiveVar(appVar)
  const [list, setList] = useState<NewsMediaItem[]>([])

  useEffect(() => {
    setList(hits.map(i => parseMediaItem(i.fields, 'news')))
  }, [hits])

  if (!algoliaReady) {
    return null
  }

  if (!list.length) {
    return null
  }

  const mapList = list.map(i =>
    <Link key={ i.id } to={ `/media-centre/latest-news/${ i.slug }` }>
      <MediaCard item={ i } className='card'/>
    </Link>
  )

  return (
    <div className={ cn(s.RelatedNews, "overflow-hidden") }>
      <div className={ cn(s.layout, "overflow-hidden") }>
        { deviceType === 'desktop' ?
            list.length <= 3 ?
              <div className={ s.list }>
                { mapList }
              </div>
            :
              <Slider { ...settings }>
                { mapList }
              </Slider>
          :
            <Slider { ...settings }>
              { mapList }
            </Slider>
        }
      </div>
    </div>
  )
}

export default connectHits(RelatedNews)
