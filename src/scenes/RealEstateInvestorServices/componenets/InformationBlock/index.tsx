import Pic from 'assets/pictures/noPropertyImage'
import React from 'react'

import { Banner, CloudImage, ContactCard } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrollAcademyPic from 'assets/pictures/brollAcademy'
import BrollBrokingPic from 'assets/pictures/broking'
import ManagingAgentsPic from 'assets/icons/managingAgents.png'
import CircleIcon from 'assets/icons/circle.png'
import Icon1 from 'assets/icons/investorServicesIcon1.png'
import Icon2 from 'assets/icons/investorServicesIcon2.png'
import Icon3 from 'assets/icons/investorServicesIcon3.png'
import Icon4 from 'assets/icons/investorServicesIcon4.png'
import Icon5 from 'assets/icons/investorServicesIcon5.png'
import Icon6 from 'assets/icons/investorServicesIcon6.png'
import Icon7 from 'assets/icons/investorServicesIcon7.png'
import Icon8 from 'assets/icons/investorServicesIcon8.png'
import Icon9 from 'assets/icons/investorServicesIcon9.png'
import Icon10 from 'assets/icons/investorServicesIcon10.png'
import Icon11 from 'assets/icons/investorServicesIcon11.png'
import Icon12 from 'assets/icons/investorServicesIcon12.png'
import Icon13 from 'assets/icons/investorServicesIcon13.png'
import Icon14 from 'assets/icons/investorServicesIcon14.png'
import Icon15 from 'assets/icons/investorServicesIcon15.png'
import Icon16 from 'assets/icons/investorServicesIcon16.png'
import Icon17 from 'assets/icons/investorServicesIcon17.png'
import Icon18 from 'assets/icons/investorServicesIcon18.png'
import Icon19 from 'assets/icons/investorServicesIcon19.png'


const firstData = [
  { id: 1, title: "Scheme Financial <br/>Management", icon: Icon1  },
  { id: 2, title: "Body Corporate <br/>Administration <br/>Support", icon: Icon2  },
  { id: 3, title: "Building & Site <br/>Maintenance", icon: Icon3  },
  { id: 4, title: "Facilities <br/>Management", icon: Icon4  },
  { id: 5, title: "OHSA <br/>Expertise", icon: Icon5  },
  { id: 6, title: "Debt <br/>Collections", icon: Icon6  },
  { id: 7, title: "Procurement,\n <br/>Supplier Compliance <br/>& Payments", icon: Icon7  },
  { id: 8, title: "Tax <br/>Consultation", icon: Icon8  },
  { id: 9, title: "Insurance & <br/>Risk Management", icon: Icon9  },
  { id: 10, title: "10 Year <br/>Maintenance Plan", icon: Icon10  }
]

const secondData = [
  { id: 1, title: "We currently <br/>manage", description: "30 Sectional Title <br/>Buildings", icon: Icon11 },
  { id: 2, title: "Gross lettable <br/>area of more than", description: "178,000m² (Coastal) <br/>236,000m² (Inland) <br/>Since 2011", icon: Icon12 },
  { id: 3, title: "Nationally, <br/>over", description: "740 Owners", icon: Icon13 }
]

const thirdData = [
  { id: 1, title: "Commercial <br/>Office Parks", icon: Icon14 },
  { id: 2, title: "Industrial Business <br/>& Office Parks", icon: Icon15 },
  { id: 3, title: "Townhouse <br/>Complexes", icon: Icon16 },
  { id: 4, title: "Residential <br/>Complexes", icon: Icon17 },
  { id: 5, title: "Golf Estates & <br/>Gated Residential <br/>Complexes", icon: Icon18 },
  { id: 6, title: "Mixed Used <br/>Developments <br/>& Office Parks", icon: Icon19 },
]

const cardData = [
  { id: 1, name: 'Rodney Luntz', title: 'Head of Commercial Broking Gauteng', img: Pic, email: 'rluntz@broll.com', number: '+27 87 700 8290' },
  { id: 2, name: 'Sean Berowsky', title: 'Head of Broking South Africa', img: Pic, email: 'berowsky@broll.com', number: '+27 87 700 8290' }
]


const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ cn(s.section1) }>
          <div className={ cn(s.heading, 'no-desktop') }>
            <p className='light no-desktop m-0 letterSpacingNormal'>At Broll Property Group we offer more than just a secretarial service, we offer a fully comprehensive operational, financial and administrative function with the core objective of
              growing and protecting your asset. We have a team of property experts equipped to unlock the true potential and long term value of your property. We align the interests of you
              as the property investor, your tenants and the management using a blend of expertise, experience, knowledge, trusted relationships and operational efficiency to assist you with
              all your sectional title and estate management requirements.
            </p>
          </div>
          <Banner image={ BrollAcademyPic } secondaryTitle='Sectional title <br/>management <br/>with distinctive <br/>excellence' description='Our service and <br/>expertise are our <br/>greatest assets.'/>
          <div className={ s.line }/>
        </div>
        <div className={ s.section2 }>
          <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>At Broll Property Group we offer more than just a secretarial service, we offer a fully comprehensive operational, financial and administrative function with the core objective of
            growing and protecting your asset. We have a team of property experts equipped to unlock the true potential and long term value of your property. We align the interests of you
            as the property investor, your tenants and the management using a blend of expertise, experience, knowledge, trusted relationships and operational efficiency to assist you with
            all your sectional title and estate management requirements.
          </p>
          <h3 className='semiBig red-2 bold mt-1 mb-0'>We Provide</h3>
          <div className={ s.list }>
            { firstData.map((item, index) =>
                <div className={ s.item } key={ item.id }>
                  <div className={ s.wrapper }>
                    { index === 0 &&
                      <div className={ s.circle }>
                        <img src={ CircleIcon } alt="Broll"/>
                      </div>
                    }
                    <div className={ s.icon }>
                      <img src={ item.icon } alt="broll" className='contained'/>
                    </div>
                  </div>
                  <div className={ s.title }>
                    <p className={ cn('small medium no-mobile no-tablet m-0 letterSpacingNormal', { 'red-2': index === 0 }) } dangerouslySetInnerHTML={{ __html: item.title }}/>
                    <p className={ cn('medium no-desktop m-0 letterSpacingNormal', { 'red-2': index === 0 }) } dangerouslySetInnerHTML={{ __html: item.title }}/>
                  </div>
                </div>
            )}
          </div>
        </div>
        <div className={ s.section3 }>
          <div className={ s.image }>
            <CloudImage
              src={ BrollBrokingPic }
              alt="Broll Broking"
              className='covered'
              responsive={{
                desktop: { w: '1920' }
              }}
            />
          </div>
          <div className={ s.line }/>
        </div>
        <div className={ s.section4 }>
          <div className={ s.list }>
            { secondData.map((item, index) =>
              <div className={ s.item } key={ item.id }>
                <div className={ s.wrapper }>
                  { index === 0 &&
                    <div className={ s.circle }>
                      <img src={ CircleIcon } alt="Broll"/>
                    </div>
                  }
                  <div className={ s.icon }>
                    <img src={ item.icon } alt="broll" className='contained'/>
                  </div>
                </div>
                <div className={ cn(s.title, 'mb-05') }>
                  <p className={ cn('small bold no-mobile no-tablet m-0 letterSpacingNormal', { 'red-2': index === 0 }) } dangerouslySetInnerHTML={{ __html: item.title }}/>
                  <p className={ cn('bold no-desktop m-0 letterSpacingNormal', { 'red-2': index === 0 }) } dangerouslySetInnerHTML={{ __html: item.title }}/>
                </div>
                <div className={ s.description }>
                  <p className={ cn('small medium no-mobile no-tablet m-0 letterSpacingNormal', { 'red-2': index === 0 }) } dangerouslySetInnerHTML={{ __html: item.description }}/>
                  <p className={ cn('medium no-desktop m-0 letterSpacingNormal', { 'red-2': index === 0 }) } dangerouslySetInnerHTML={{ __html: item.description }}/>
                </div>
              </div>
            )}
          </div>
          <div className={ s.wrapper }>
            <div className={ s.left }>
              <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>With a track record of 99% residential and 100% commercial, industrial and retail
                levy collections, our highly skilled staff support owners and trustees to manage
                sectional title complexes efficiently, profitably and in line with the Sectional Titles
                Schemes Management Act No 8 of 2011 2016. We are a registered member of the
                Estate Agency Affairs Board
              </p>
              <p className='light no-desktop m-0 letterSpacingNormal'>With a track record of 99% residential and 100% commercial, industrial and retail
                levy collections, our highly skilled staff support owners and trustees to manage
                sectional title complexes efficiently, profitably and in line with the Sectional Titles
                Schemes Management Act No 8 of 2011 2016. We are a registered member of the
                Estate Agency Affairs Board
              </p>
            </div>
            <div className={ s.right }>
              <img src={ ManagingAgentsPic } alt="Broll" className='contained'/>
            </div>
          </div>
        </div>
        <div className={ s.line }/>
        <div className={ s.section5 }>
          <h3 className='semiBig red-2 bold m-0'>We Manage</h3>
          <div className={ s.list }>
            { thirdData.map((item, index) =>
              <div className={ s.item } key={ item.id }>
                <div className={ s.wrapper }>
                  { index === 0 &&
                    <div className={ s.circle }>
                      <img src={ CircleIcon } alt="Broll"/>
                    </div>
                  }
                  <div className={ s.icon }>
                    <img src={ item.icon } alt="broll" className='contained'/>
                  </div>
                </div>
                <div className={ s.title }>
                  <p className={ cn('small medium no-mobile no-tablet m-0 letterSpacingNormal', { 'red-2': index === 0 }) } dangerouslySetInnerHTML={{ __html: item.title }}/>
                  <p className={ cn('medium no-desktop m-0 letterSpacingNormal', { 'red-2': index === 0 }) } dangerouslySetInnerHTML={{ __html: item.title }}/>
                </div>
              </div>
            )}
          </div>
        </div>
        <div className={ s.section6 }>
          { cardData.map(item =>
            <ContactCard data={ item } key={ item.id }/>
          )}
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
