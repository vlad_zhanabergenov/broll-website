import React from "react";

import { Breadcrumbs, SearchBar } from "components";

import s from "./style.module.sass";
import cn from "classnames";

const Privacy: React.FC = () => {
  return (
    <div className={s.Privacy}>
      <div className={s.layout}>
        <div className={cn(s.section1, "pt-2 ph-17 pb-4 box-sizing")}>
          <Breadcrumbs items={["Privacy Policy"]} className="mb-2" />
          <div className={cn(s.searchBar, "pl-1 pr-3 box-sizing")}>
            <SearchBar />
          </div>
        </div>
        <div className={cn(s.section2, "pb-6 box-sizing")}>
          <h2 className={cn("thin m-0 mb-3")}>Privacy Policy</h2>
          <div className={cn(s.content, "mv-2")}>
            <h4 className="small semiBold red-2 mb-1">
              PROTECTION OF PERSONAL INFORMATION
            </h4>
            <p className="small light letterSpacingNormal m-0">
              In line with the Protection of Personal Information Act, Broll is
              committed to protecting the privacy of personal information of our
              data subjects. The information you share with us as a data subject
              allows us to provide you with the best experience with our
              products and services, or as a stakeholder. The company has
              dedicated policies and procedures in place to protect all personal
              information collected and processed by us. We will never sell your
              personal information. Please read below for more information on
              what is personal information, how we collect, process, use and
              disclose personal information.
            </p>
            <ol className={cn(s.numberedList, "m-0")}>
              <li>
                <p className="small light letterSpacingNormal mv-0 ml-1 mt-1">
                  Personal information includes any information by which you can
                  be identified and which relates to you as an identifiable
                  person such as your name, email address, physical and postal
                  addresses, race, sex, age and/or internet address of the
                  domain from which you are visiting.
                </p>
              </li>
              <li>
                <p className="small light letterSpacingNormal mv-0 ml-1">
                  We use different methods to collect information from and about
                  you. Your personal information may either be collected by us
                  or provided by you. We will only collect your personal
                  information by lawful and fair means and, where appropriate
                  with your knowledge or consent.
                </p>
              </li>
              <li>
                <p className="small light letterSpacingNormal mv-0 ml-1">
                  We process personal information for various reasons. Before or
                  at the time of collecting your personal information, we will
                  identify the purpose(s) for which the information is being
                  collected. Personal information is used as is appropriate in
                  the normal course of business to provide the products and
                  services. We may retain any information for purposes of
                  ongoing business relationships or to communicate directly with
                  you. The reasons for processing of information is including
                  but not limited to the following.
                </p>
                <ul className={s.bulletList}>
                  <li>
                    <p className="small light letterSpacingNormal mv-0 ml-1">
                      To manage information, products and/or services requested
                      by data subjects;
                    </p>
                  </li>
                  <li>
                    <p className="small light letterSpacingNormal mv-0 ml-1">
                      To help us identify data subjects when they contact us;
                    </p>
                  </li>
                  <li>
                    <p className="small light letterSpacingNormal mv-0 ml-1">
                      To improve the quality of our services;
                    </p>
                  </li>
                  <li>
                    <p className="small light letterSpacingNormal mv-0 ml-1">
                      Marketing purposes
                    </p>
                  </li>
                </ul>
              </li>
              <li>
                <p className="small light letterSpacingNormal mv-0 ml-1">
                  Any and all information collected will be kept strictly
                  confidential. We will not disclose your personal information
                  to anyone, unless we obtain your consent, or unless it
                  required or permitted by law or regulatory authority. It will
                  not be sold, loaned or otherwise disclosed to any
                  organisation.
                </p>
              </li>
              <li>
                <p className="small light letterSpacingNormal mv-0 ml-1">
                  We may retain any information for purposes of ongoing business
                  relationships or to communicate directly with you. We will
                  store and keep your personal information according to the
                  retention (holding) periods defined by law for legitimate
                  business purposes and will take reasonably practicable steps
                  to make sure that it is kept up to date and deleted and
                  archived according to our defined retention schedules.
                </p>
              </li>
              <li>
                <p className="small light letterSpacingNormal mv-0 ml-1">
                  Upon request we will furnish you with details of the personal
                  information we hold about you. You may submit your request
                  using Form2 Correction or Deletion of Personal Information
                  which can be sourced using our PAIA page www.broll.com/paia.
                  Should you believe that any information we hold about you is
                  incorrect, please inform us using Form 2 and we will correct
                  it.
                </p>
              </li>
              <li>
                <p className="small light letterSpacingNormal mv-0 ml-1">
                  We may transfer to, and store personal information we collect
                  about you, in countries other than South Africa, if the
                  relevant business transactions or situation requires
                  trans-border processing. These countries may not have the same
                  data protection laws as South Africa, and in this instance we
                  will only transfer the information if we have consent from
                  you, or it is necessary for the performance or conclusion of a
                  contract between us.
                </p>
              </li>
              <li>
                <p className="small light letterSpacingNormal mv-0 ml-1">
                  Where there are reasonable grounds to believe that the
                  personal information of a data subject has been accessed or
                  acquired by any unauthorised person, Broll Property Group
                  (Pty) Ltd shall notify:
                </p>
                <ul className={s.alphabeticalList}>
                  <li>
                    <p className="small light letterSpacingNormal mv-0 ml-1">
                      the Regulator; and
                    </p>
                  </li>
                  <li>
                    <p className="small light letterSpacingNormal mv-0 ml-1">
                      the data subject, unless the identity of such data subject
                      cannot be established.
                    </p>
                  </li>
                </ul>
                <p
                  className="small light letterSpacingNormal m-0 ml-1">
                  The notification will be made as soon as reasonably possible
                  after the discovery of the compromise, taking into account the
                  legitimate needs of law enforcement or any measures reasonably
                  necessary to determine the scope of the compromise and to
                  restore the integrity of the responsible party’s information
                  system.
                </p>
              </li>
            </ol>

            <h4 className="small semiBold red-2 mt-3 mb-1">
              ON-LINE PLATFORM DISCLAIMER
            </h4>
            <p className="small light letterSpacingNormal">
              All information and content made available on any On-line Platform
              is provided by Broll. We make no representations or warranties,
              implied or otherwise, that, amongst others, the content available
              on this On-line Platform is free from errors or omissions or that
              the service will be 100% uninterrupted and error free.
            </p>
            <p className="small light letterSpacingNormal">
              We have taken, and will continue to take due care and diligence
              that all information provided on this platform is, to the best of
              our knowledge and understanding, true and correct. However, Broll
              shall not be liable for any damage, loss or liability of
              whatsoever nature arising from the use or inability to use this
              On-line Platform, any information or content provided from and
              through this On-line Platform.
            </p>
            <p className="small light letterSpacingNormal">
              There is no warranty of any kind, express or implied, regarding
              the information supplied on this On-line Platform or in respect of
              any aspect of our services. Any warranty implied by law is hereby
              excluded, except to the extent to which such exclusion would be
              unlawful.
            </p>
            <p className="small light letterSpacingNormal">
              Information, ideas and opinions expressed on this On-line Platform
              should not be regarded as professional advice. Users are
              encouraged to consult Broll before taking any course of action
              related to information or opinions expressed on this site.
            </p>
            <p className="small light letterSpacingNormal">
              This On-line Platform is supplied on an "as is" basis and has not
              been compiled or supplied to meet the user's individual
              requirements. It is the sole responsibility of the user, prior to
              entering into any agreement with Broll, to satisfy himself or
              herself that the service available through this On-line Platform
              will meet the user's individual requirements.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              ELECTRONIC MAIL DISCLAIMER
            </h4>
            <p className="small light letterSpacingNormal">
              Any e-mail and any attachments thereto may contain confidential
              and proprietary information. The e-mail is intended for the
              addressee only and should only be used by the addressee for the
              related purpose. If you are not the intended recipient of any
              e-mail, you are requested to delete it immediately. Any
              disclosure, copying, distribution of or any action taken or
              omitted in reliance on this information is prohibited and may be
              unlawful. The views expressed in the e-mail are, unless otherwise
              stated, those of the sender and not those of the Broll Property
              Group or its management. E-mails cannot be guaranteed to be secure
              or free of errors or viruses. No liability or responsibility is
              accepted for any interception, corruption, destruction, loss, late
              arrival or incompleteness of or tampering or interfering with any
              of the information contained in any e-mail or for its incorrect
              delivery or non-delivery or for its effect on any electronic
              device of the recipient.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">USAGE</h4>
            <p className="small light letterSpacingNormal">
              Broll authorises the user to view, print, and distribute the
              content of this On-line Platform, or any part thereof, provided
              that: a. such content is used for informational and/or
              non-commercial purposes only; and b. any reproduction of the
              content of this On-line Platform, or portions thereof, must
              include the following copyright notice: © Broll. Users wishing to
              use any content from this site for commercial purposes may only do
              so with prior written permission from Broll. These services do not
              address anyone under the age of 13. We do not knowingly collect
              personally identifiable information from children under 13. If you
              are a parent or guardian and you are aware that your child has
              provided us with personal information, please contact us directly.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              INTELLECTUAL PROPERTY RIGHTS
            </h4>
            <p className="small light letterSpacingNormal">
              All content, trademarks and data on this On-line Platform,
              including but not limited to data, text, designs, graphics, and
              icons are the property of Broll. As such they are protected from
              infringement by domestic and international legislation and
              treaties. Subject to the rights afforded to the user herein, all
              other rights to all intellectual property on this On-line Platform
              are expressly reserved and no license to Broll intellectual
              property has been granted on this On-line Platform.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              INFORMATION NOT UPDATED OR TO BE RELIED ON
            </h4>
            <p className="small light letterSpacingNormal">
              While Broll uses all reasonable efforts to include accurate and
              up-to-date information on the On-line Platform, Broll makes no
              warranties or representations with respect to the content of the
              On-line Platform. Broll assumes no responsibility for updating the
              On-line Platform or its content or notifying users of information
              that is inaccurate, incomplete, or out-of-date.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">NO LIABILITY</h4>
            <p className="small light letterSpacingNormal">
              Broll assumes no responsibility, and shall not be liable for, any
              damages to, or viruses that may infect, your computer equipment or
              other property on account of your access to or use of this On-line
              Platform or your downloading of any materials, data, text, or
              images from the On-line Platform.
            </p>
            <p className="small light letterSpacingNormal">
              Broll will not be held responsible or liable for any interruption
              or discontinuance of any or all functionality of this On-line
              Platform, whether the result of actions or omission of Broll or a
              third party.
            </p>
            <p className="small light letterSpacingNormal">
              Broll accepts no responsibility or liability whatsoever arising
              from or in any way connected with the use of this On-line Platform
              or its content. Broll will not be liable for the accuracy,
              completeness, adequacy, timeliness or comprehensiveness of the
              information contained on the On-line Platform.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              CHANGES AND AMENDMENTS
            </h4>
            <p className="small light letterSpacingNormal">
              Broll expressly reserves the right to alter and/or amend any
              information set out in this On-line Platform without notice.
              Furthermore, Broll may at any time revise or update these terms
              and conditions. The user is bound by such revisions and should
              therefore periodically visit this policy.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              AGREEMENTS IN TERMS OF SECTION 21 OF THE ELECTRONIC COMMUNICATIONS
              AND TRANSACTION ACT
            </h4>
            <p className="small light letterSpacingNormal">
              No information or data on this On-line Platform constitutes a
              solicitation, recommendation, endorsement or offer by Broll. No
              agreements shall be concluded merely by sending a data message to
              this On-line Platform or its owners. Valid agreements require an
              acknowledgement and offer, or receipt from this On-line Platform.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              ELECTRONIC COMMUNICATIONS
            </h4>
            <p className="small light letterSpacingNormal">
              By using this On-line Platform or communicating with Broll by
              electronic means, the user consents and acknowledges that any and
              all agreements, notices, disclosures, or any other communication
              satisfies any legal requirement, including but not limited to the
              requirement that such communications should be in writing.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              LINKS TO THIRD PARTY SITES
            </h4>
            <p className="small light letterSpacingNormal">
              Broll provides links to third party sites and other non-Broll
              on-line platforms to the user only as a convenience. Such third
              party information is independent from that provided by Broll.
              Broll accepts no responsibility for the content or accuracy or the
              use of such other non-Broll on-line platforms and does not
              endorse, and shall not be deemed to have endorsed or accepted,
              their contents. Linked other non-Broll on-line platforms or pages
              are not subject to Broll control. Broll shall not be held
              responsible or liable, directly or indirectly, in any way for the
              contents of such other non-Broll on-line platforms, the use of
              such other non-Broll on-line platforms, or inability to use or
              access any linked other non-Broll on-line platforms or any links
              contained in a linked other non-Broll on-line platforms.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              SECURITY OF INFORMATION
            </h4>
            <p className="small light letterSpacingNormal">
              It is expressly prohibited for any person, business or entity to
              gain or attempt to gain unauthorised access to any element of this
              On-line Platform, or to deliver or attempt to deliver any
              unauthorised, damaging or malicious code to this On-line Platform.
              Any person who delivers or attempts to deliver any unauthorised,
              damaging or malicious code to this On-line Platform or attempts to
              gain unauthorised access to any element of this On-line Platform
              shall be held criminally liable. In the event that Broll should
              suffer any damage or loss, civil damages will be claimed.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">SEVERABILITY</h4>
            <p className="small light letterSpacingNormal">
              These terms and conditions of use constitute the entire agreement
              between Broll and the user of this On-line Platform. Any failure
              by Broll to exercise or enforce any right or provision of these
              terms and conditions of use shall in no way constitute a waiver of
              such right or provision.
            </p>
            <p className="small light letterSpacingNormal">
              In the event that any term or condition of the use of this On-line
              Platform is not fully enforceable or valid for any reason, such
              term(s) or condition(s) shall be severable from the remaining
              terms and conditions. The remaining terms and conditions shall not
              be affected by such enforceability or invalidity and shall remain
              enforceable and applicable.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              APPLICABLE AND GOVERNING LAW
            </h4>
            <p className="small light letterSpacingNormal">
              This On-line Platform is hosted, controlled and operated from the
              Republic of South Africa, and thus South African law governs the
              use or inability to use this On-line Platform and these terms and
              conditions.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              CONTACT INFORMATION
            </h4>
            <p className="small light letterSpacingNormal">
              Any questions, queries, or requests to use any part of this
              On-line Platform can be directed to:
            </p>
            <p className="small light letterSpacingNormal">
              Broll: Contact Number: 0114414000
            </p>
            <p className="small light letterSpacingNormal">
              Alternatively, please make use of our Contact Us page on our
              broll.com website.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">E-MAIL ADDRESS</h4>
            <p className="small light letterSpacingNormal">
              E-mail addresses will only be used for the purpose for which you
              provide it and will not be added to a mailing list unless you
              request that this be done. We will not disclose it without your
              consent.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              E-MAIL SUBSCRIPTIONS
            </h4>
            <p className="small light letterSpacingNormal">
              We maintain a list of e-mail addresses to which the Broll
              Newsletter, our regular newsletter, is sent. Individuals must
              affirmatively request to join this list by using the subscribe
              form on the subscriptions page on our broll.com website. Any
              members of this list may choose to unsubscribe at any time by
              using the PAIA link www.broll.com/paia . Our list server has been
              configured in such a way that the e-mail addresses can only be
              accessed by authorised Broll staff.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">COOKIES</h4>
            <p className="small light letterSpacingNormal">
              Some elements of the On-line Platform may make use of cookies - a
              very small text file placed on your hard drive by a web server.
              Basically, it is your unique identification card that can only be
              read by the server that gave it to you.
            </p>
            <p className="small light letterSpacingNormal">
              Cookies are not used for any other purpose at this On-line
              Platform.
            </p>
            <p className="small light letterSpacingNormal">
              You may prevent the use of cookies by configuring your web browser
              accordingly. This may, however, hinder the On-line Platform's
              functionality.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              INFORMATION LOGGED
            </h4>
            <p className="small light letterSpacingNormal">
              When you make use of this On-line Platform, our server logs the
              following information:
            </p>
            <p className="small light letterSpacingNormal">
              <ul className={s.bulletList}>
                <li>
                  <p className="small light letterSpacingNormal mv-0 ml-1">
                    the type of browser and operating system your computer
                    uses.Anchor
                  </p>
                </li>
                <li>
                  <p className="small light letterSpacingNormal mv-0 ml-1">
                    your domain name extension (for example .com, .co.za).
                  </p>
                </li>
                <li>
                  <p className="small light letterSpacingNormal mv-0 ml-1">
                    the referring site's address (the site where you clicked the
                    link that led you to us).
                  </p>
                </li>
                <li>
                  <p className="small light letterSpacingNormal mv-0 ml-1">
                    your server's IP address (a number which is unique to the
                    computer connecting you to the Internet, which is usually
                    one of your service provider's computers).
                  </p>
                </li>
                <li>
                  <p className="small light letterSpacingNormal mv-0 ml-1">
                    the date and time of your visit.
                  </p>
                </li>
                <li>
                  <p className="small light letterSpacingNormal mv-0 ml-1">
                    the address of the pages visited. the documents downloaded.
                  </p>
                </li>
                <li>
                  <p className="small light letterSpacingNormal mv-0 ml-1">
                    Your device information such as but not limited to
                    manufacturer and operating system
                  </p>
                </li>
              </ul>
              <p className="small light letterSpacingNormal">
                This information is used only for statistical analysis or system
                administration purposes. It is not associated or correlated with
                any other information we collect about you.
              </p>
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              LINKS TO OTHER SITES
            </h4>
            <p className="small light letterSpacingNormal">
              This On-line Platform contains links to other sites. Broll is not
              responsible for the privacy practices of these On-line Platforms.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">
              GOOGLE DISCLAIMER
            </h4>
            <p className="small light letterSpacingNormal">
              We use third-party advertising companies to serve ads when you
              visit our On-line Platform. These companies may use information
              (not including your name, address, email address, or telephone
              number) about your visits to this and other website's in order to
              provide advertisements about goods and services of interest to
              you. If you would like more information about this practice and to
              know your choices about not having this information used by these
              companies.
            </p>
            <h4 className="small semiBold red-2 mt-3 mb-1">DEFINITIONS</h4>
            <p className="small light letterSpacingNormal">
              “Mobile Application” is an application developed by Broll and made
              publicly available on either the Google Playstore or Apple
              Appstore
            </p>
            <p className="small light letterSpacingNormal">
              “On-line Platform” refers to either the Mobile Application or
              Broll owned domains or web sites.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Privacy;
