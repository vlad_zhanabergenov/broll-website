import { useForm } from 'hooks'
import React from 'react'
import { Configure, Index } from 'react-instantsearch-core'

import { Breadcrumbs, SearchBar, NewsSearchBar, SearchStats, Pagination } from 'components'
import { NewsResults } from './components'
import type { Form } from 'components/NewsSearchBar'

import s from './style.module.sass'
import cn from 'classnames'

const LatestNewsRaw: React.FC = () => {
  const { values, change, defaultForm } = useForm<Form>({
    fields: {
      query: { initialValue: "" },
      category: { initialValue: [] }
    }
  })

  const handleReset = () => {
    defaultForm({ query: "", category: [] })
  }

  return (
    <div className={ s.LatestNewsScene }>
      <div className={ s.layout }>
        <div className={ cn(s.section1, 'pt-2 ph-17 pb-4 box-sizing') }>
          <Breadcrumbs items={ ['Media Centre', 'Latest news'] } className="mb-2"/>
          <div className={ cn(s.searchBar, 'pl-1 pr-3 box-sizing') }>
            <SearchBar/>
          </div>
        </div>
        <div className={ cn(s.section2, 'ph-content pb-3 box-sizing') }>
          <h2 className={ cn('thin m-0 mb-3') }>Latest News</h2>
          <div className={ cn(s.search, 'mv-2') }>
            <NewsSearchBar values={ values } change={ change } onReset={ handleReset }/>
            <div className={ cn(s.stats, 'mt-1') }>
              <SearchStats/>
            </div>
          </div>
          <div className={ cn(s.results, 'mt-2 mb-6') }>
            <NewsResults onReset={ handleReset }/>
            <Pagination className="mt-3"/>
          </div>
        </div>
      </div>
    </div>
  )
}

const LatestNewsScene: React.FC = () => {
  return (
    <Index indexName='News' indexId='LatestNewsScene'>
      <Configure hitsPerPage={ 9 } distinct={ 1 }/>
      <LatestNewsRaw/>
    </Index>
  )
}

export default LatestNewsScene
