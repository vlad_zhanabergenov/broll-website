import React from "react";
import s from "./style.module.sass";
import cn from "classnames";

import { CloudImage } from "components";
import TopImage from '../../../../assets/pictures/intelAndMarketAnalysis'

const topLeftList = [
  "The role of today’s real estate company has evolved. Intel is the new commodity. Now, more than ever, we need to interpret, evaluate and disseminate information quickly before it becomes stale. Broll recognises this need and invests heavily in intel gathering so as to provide its clients with the level of information necessary to make the best decisions.",
  "Our in-house team of experts have extensive industry experience in research-based real estate consulting and advisory services. This high-performance team conducts unique market analyses, which provide relevant insights about different market segments. These insights enable our clients to make informed, strategic and data-driven decisions.",
];

const topRightList = ["Commercial", "Retail", "Industrial"];

const leftList = [
  "Country reports",
  "Market studies",
  "Location and nodal analysis/reports",
  "Feasibility studies",
  "Retail mapping",
  "Portfolio optimisation and analysis",
  "Benchmarking",
  "Demographic analysis",
];

const rightList = [
  "Drive-time analysis",
  "Competitor locations",
  "Repositioning or relocating of stores within a shopping centre",
  "Development pipelines",
  "Lease optimisation, acquisition and renewals",
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
            Reliable sector-specific industry intelligence.
          </h3>
          <div className={ cn(s.topImage, "mt-1") }>
            <CloudImage
              src={ TopImage }
              alt="Broll"
              className="covered"
              responsive={{
                desktop: { w: '1920' },
              }}
            />
          </div>
          <div className={s.container}>
            <div className={cn(s.left, "")}>
              <h3 className={cn(s.title, "semiBig bold red-4")}>
                Our Value Proposition
              </h3>
              {topLeftList.map((item) => (
                <p
                  className={cn(
                    s.text,
                    "white small light letterSpacingNormal mb-0"
                  )}
                >
                  {item}
                </p>
              ))}
            </div>
            <div className={cn(s.right)}>
              <h3 className={cn(s.title, "semiBig bold m-0")}>
                Sector Access
              </h3>
              {topRightList.map((item, i) => (
                <p key={i} className="x-small regular">
                  {item}
                </p>
              ))}
              <h3 className={cn(s.title, "semiBig bold m-0 mt-1")}>
                Direct In-Country Insights
              </h3>
              <p className="x-small regular">
                South Africa, Nigeria, Ghana, Cameroon, Côte d’Ivoire, Kenya,
                Uganda, Botswana, Namibia, Mozambique, Zambia, eSwatini,
                Mauritius.
              </p>
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-4")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-1")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {leftList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
            <div className={cn(s.right, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {rightList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
