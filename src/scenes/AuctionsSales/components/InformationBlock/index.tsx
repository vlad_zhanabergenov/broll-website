import React from "react";
import CloudImage from "components/CloudImage";
import img from '../../../../assets/pictures/auctionsAndSales'
import s from "./style.module.sass";
import cn from "classnames";


const topLeftList = [
  "We bring to the market a fresh outlook coupled with an inherent understanding of how the sector works and we offer specialist services to ensure the successful sale of office, Industrial, retail mixed-use and bulk residential property. Our Seller and Buyer networks are our unique differentiator, and we have our finger on the pulse of the market.",
  "Our operating model is geared towards deep asset specialisation with dedicated teams. Our experts specialise in sectors such as Shopping Centres, Township Retail, Petrol Stations, Industrial, Student Accommodation, Apartment Blocks, Development Opportunities, Hospitality, Medical and Education. ",
  "Our key clients are commercial property funds, asset managers, owners, banks and institutions, Trustees, Liquidators, Attorneys and Business Practitioners, we work predominantly across the board for all major corporations, servicing clients with various asset classes wanting to acquire and dispose of assets. ",
  "We have an exclusive partnership with Business Day TV Channel 412, to live-stream our premier commercial auction events on national television. By reaching well over million viewers per auction event, we are pioneering our sector and empowering the next generation of property Investors.",
];

const leftList = [
  "South Africa’s premier commercial property trading platform",
  "Auctions",
  "Tenders",
  "Online & private treaty transactions",
  "Valuations",
  "Auction Platform enabling rental & acquisition of office space",
];

const InformationBlock: React.FC = () => {
  return (
    <div className={cn(s.InformationBlock)}>
      <div className={cn(s.layout)}>
        <div className={s.section1}>
          <h3 className={cn(s.subTitle, "semiBig regular m-0 ml-4 mb-4")}>
            Sales Partner of Choice. Expertise & Excellence.
          </h3>
          <div className={s.container}>
            <div className={cn(s.left, "")}>
              <h3 className={cn(s.title, "semiBig bold red-4")}>
                Our Value Proposition
              </h3>
              {topLeftList.map((item) => (
                <p
                  key={item}
                  className={cn(
                    s.text,
                    "white small light letterSpacingNormal mb-0"
                  )}
                >
                  {item}
                </p>
              ))}
            </div>
            <div className={cn(s.right)}>
              <CloudImage
                src={img}
                alt="Broll"
                className="covered"
                responsive={{
                  desktop: { w: "1920" },
                }}
              />
            </div>
          </div>
        </div>

        <div className={cn(s.section2, "grey-2 pt-3 pb-4")}>
          <h3 className={cn(s.title, "semiBig bold red-4 ml-4 mt-0 mb-1")}>
            Our Services
          </h3>
          <div className={cn(s.block, "")}>
            <div className={cn(s.left, "pl-4 pr-4 box-sizing")}>
              <div className={cn(s.list)}>
                {leftList.map((item) => (
                  <p key={item} className="x-small regular">
                    {item}
                  </p>
                ))}
              </div>
            </div>
            <div className={cn(s.right, "pl-4 pr-4 box-sizing")}></div>
          </div>
        </div>

        <div className={cn(s.section3, "grey-2 pb-4")}>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0 mt-1")}>40 years</h2>
                <p className={cn(s.text, "small light")}>of experience</p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <h2 className={cn(s.title, "small bold m-0 mt-1")}>$14m</h2>
                <p className={cn(s.text, "small light")}>
                  Monthly average value of assets auctioned
                </p>
              </div>
            </div>
          </div>
          <div className={cn(s.block, " box-sizing")}>
            <div className={cn(s.content, "light red-4")}>
              <div className={cn(s.line)}></div>
              <div className={cn(s.stats, "pl-2 box-sizing")}>
                <p className={cn(s.text, "small light mt-1")}>
                  Commercial property sales
                </p>
                <h2 className={cn(s.title, "small bold m-0")}>+$492m</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationBlock;
