import { useReactiveVar } from '@apollo/client'
import React, { useEffect, useState } from 'react'
import { HitsProvided } from 'react-instantsearch-core'
import { connectHits } from 'react-instantsearch-dom'
import { appVar } from 'cache/vars'
import parseMediaItem from 'services/parseMediaItem'

import { MediaCard } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  onReset: () => void
}

const Results: React.FC<Props & HitsProvided<any>> = ({ onReset, hits }) => {
  const { algoliaReady } = useReactiveVar(appVar)
  const [list, setList] = useState<NewsMediaItem[]>([])

  useEffect(() => {
    setList(hits.map(i => parseMediaItem(i.fields, 'blog')))
  }, [hits])

  if (!algoliaReady) {
    return null
  }

  return (
    <div className={ cn(s.BlogResults) }>
      <div className={ s.layout }>
        { hits.length ?
          <div className={ s.list }>
            { list.map(item =>
              <MediaCard key={ item.id } item={ item }/>
            )}
          </div>
        :
          <h3 className="medium mv-10 text-center">Your search returned no results. Please <span className='cursor-pointer bold red' onClick={ onReset }>reset</span> your filter and try again.</h3>
        }
      </div>
    </div>
  )
}

export default connectHits(Results)
