import { ContactBanner }from 'components'
import React from 'react'
import cn from 'classnames'

import s from './style.module.sass'

const leftList = [
  "Broll Auctions and Sales, a joint venture between Broll Property Group (Pty) Ltd and Greenday Property specialises in property auctions, deal making, tenders and private treaty sales.",
  "Broll Auctions represents buyers and sellers across the office, industrial, retail and residential sectors with a diversified clientele in agriculture, high density residential developments, leisure and hospitality sectors.",
  "Led by a team of property experts, our combined knowledge and comprehensive database of select buyers, sellers, landlords, tenants and investors contribute to our ability to create successful property transaction platforms.",
  "With a strong acumen in property transactions and astute trading experience, we are able to assist both our buyers and sellers in the acquisition and disposal of assets.",
  "We deal and trade in the best interest of our clients ensuring that we meet and exceed their needs and expectations.",
  "At Broll Auctions and Sales, we pride ourselves in professionalism, discretion and complete confidentiality in the provision of specialist, premier services to companies as well as individual buyers, sellers and investors.",
]

const rightList = [
  "Globally and in South Africa, property is seen as a safe long- term investment asset class with buyers and sellers always looking for the best buys and value for money. Property auctions are fast becoming a preferred method for buying and selling property for many investors.",
  "The auction process is transparent, buyers pay a fair market value for the purchase and what’s more, buyers also know that they are dealing with a serious seller who will price right and in line with market expectations.",
  "To participate in a live online auction visit www.brollauctions.com"
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.top }>
          <div className={ s.left }>
            { leftList.map(item =>
              <p className='small light no-mobile no-tablet m-0 letterSpacingNormal' key={ item }>{ item }</p>
            )}
            { leftList.map(item =>
              <p className='light no-desktop m-0 letterSpacingNormal' key={ item }>{ item }</p>
            )}
          </div>
          <div className={ s.right }>
            <h3 className='semiBig m-0 red-2 bold'>Why choose to auction?</h3>
            <div className={ s.list }>
              { rightList.map((item) =>
                <p key={ item } className='small light no-mobile no-tablet m-0 letterSpacingNormal'>{ item }</p>
              )}
              { rightList.map(item =>
                <p key={ item } className='light no-desktop m-0 letterSpacingNormal'>{ item }</p>
              )}
            </div>
          </div>
          <div className={ s.line }/>
        </div>
        <div className={ cn(s.bottom, 'no-tablet no-mobile') }>
          <ContactBanner />
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
