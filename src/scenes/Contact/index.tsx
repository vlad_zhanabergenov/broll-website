import React, { useState } from 'react'

import { Breadcrumbs, SearchBar } from 'components'
import { GoogleMap, Form, ContactDetails } from './componenets'

import s from './style.module.sass'
import cn from 'classnames'

const ContactScene = () => {
  const [current, setCurrent] = useState('South Africa')

  return (
    <div className={ s.ContactScene }>
      <div className={ s.layout }>
        <div className={ cn(s.section1, 'pt-2 ph-17 pb-4  box-sizing') }>
          <Breadcrumbs items={ ['Contact'] } className="mb-2"/>
          <div className={ cn(s.searchBar, 'pl-1 pr-3 box-sizing') }>
            <SearchBar/>
          </div>
        </div>
        <div className={ cn(s.section2, 'pl-14 pr-12 pb-2 box-sizing') }>
          <h2 className={ cn('thin m-0 mb-3') }>Contact Us</h2>
          <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>Broll are leaders in property management and understand how to capitalise on a property’s potential. We have branches throughout Africa and offer a range of services
            including facilities management, property leasing and we also specialise in commercial real estate.
          </p>
          <p className='light no-desktop m-0 letterSpacingNormal'>Broll are leaders in property management and understand how to capitalise on a property’s potential. We have branches throughout Africa and offer a range of services
            including facilities management, property leasing and we also specialise in commercial real estate.
          </p>
          <div className={ s.map }>
            <GoogleMap onClick={ name => setCurrent(name) } current={ current }/>
          </div>
          <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>Find your nearest Broll office address and contact details below or fill out the Enquiry Form.</p>
          <p className='light no-desktop m-0 letterSpacingNormal'>Find your nearest Broll office address and contact details below or fill out the Enquiry Form.</p>
        </div>
        <div className={ s.section3 }>
          <Form/>
        </div>
        <div className={ s.section4 }>
          <ContactDetails  current={ current } onClick={ name => setCurrent(name) }/>
        </div>
      </div>
    </div>
  )
}

export default ContactScene
