import React, { useState } from 'react'
import { useForm } from 'hooks'
import { pickBy } from 'lodash'
import { navItems } from 'utilities/constants'

import { Input, Select, Checkbox, Button } from 'components'

import s from './style.module.sass'
import cn from 'classnames'

interface Form {
  name: string
  contactNumber: string
  email: string
  region: string
  service: string
  enquiry: string
  subscribe: boolean
}

const regionOptions = [
  'South Africa', 'Botswana', 'Ghana', 'Indian Ocean', 'Kenya', 'Kenya Valuations', 'Mozambique', 'Namibia',
  'Nigeria', 'Eswatini', 'Uganda', 'Zambia'
]

const serviceOptions = navItems[2].submenu && navItems[2].submenu.map(i => i.name) || []

const Form: React.FC = () => {
  const [loading, setLoading] = useState(false)
  const [message, setMessage] = useState('')

  const handleSubmit = (_values: Form, valid: boolean, errors: { [key in keyof Form]: string | null }) => {
    if (valid) {
      setLoading(true)
    } else {
      const errorsList = Object.values(pickBy(errors))
      const firstError = errorsList.includes('Field is required') ? 'Field is required' : errorsList[0]

      if (firstError) {
        const text = firstError === 'Field is required' ? "Please fill the required fields." : firstError
        setMessage(text)
      }
    }
  }

  const { values, change, submit } = useForm<Form>({
    fields: {
      name: { initialValue: '', required: true, validator: 'min2' },
      contactNumber: { initialValue: '', required: true, validator: 'phone' },
      email: { initialValue: '', required: true, validator: 'email' },
      region: { initialValue: '', required: true },
      service: { initialValue: '', required: true },
      enquiry: { initialValue: '', required: true },
      subscribe: { initialValue: false }
    }, handleSubmit
  })

  return (
    <div className={ cn(s.Form) }>
      <div className={ cn(s.layout) }>
        <h3 className='semiBig m-0 red-2 bold'>Enquiry Form</h3>
        <div className={ s.fields }>
          <div className={ s.left }>
            <Input secondary mediumFont noBorder name="name" placeholder="Name"
                   onChange={ change } required value={ values.name }/>
            <Input secondary mediumFont noBorder name="contactNumber" placeholder="Contact Number"
                   onChange={ change } required value={ values.contactNumber }/>
            <Input secondary mediumFont noBorder name="email" placeholder="Email Address"
                   onChange={ change } required value={ values.email }/>
          </div>
          <div className={ s.right }>
            <Select secondary required noBorder  name="region" options={ regionOptions } value={ values.region }
                    onChange={ change }  placeholder="Select Your Region"/>
            <Select secondary required noBorder name="service" options={ serviceOptions } value={ values.service }
                    onChange={ change } placeholder="Select a Service"/>
            <Input secondary mediumFont name="enquiry" noBorder placeholder="Enquiry"
                   onChange={ change } required value={ values.enquiry }/>
          </div>
        </div>
        <p className={ cn({ 'no-pointer-events no-select opacity-0 mt-05 mb-05': !message }) }>{ message || "_" }</p>
        <div className={ s.footer }>
          <div className={ s.wrapper }>
            <Checkbox name='subscribe' noBorder large grey onChange={ change } value={ values.subscribe }/>
            <p className='m-0 regular' >Subscribe to Email Newsletter</p>
          </div>
          <p className='m-0 regular'>Newsletter We will communicate real estate related marketing information and related services. <br/>We respect your privacy. See our Privacy Policy</p>
          <div className={ s.btn }>
            <Button onClick={ submit } loading={ loading } darkBackground>Send</Button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Form
