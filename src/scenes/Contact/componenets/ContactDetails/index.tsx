import ArrowIcon from 'assets/icons/arrow.svg'
import { Icon } from 'components'
import React from 'react'

import s from './style.module.sass'
import cn from 'classnames'

type Props = {
  current: string
  onClick: (name: string) => void
}

type AccordionItem = {
  id: number
  name: string
}

const items: AccordionItem[] = [
  {
    id: 1,
    name: "South Africa"
  },
  {
    id: 2,
    name: "Botswana",
  },
  {
    id: 3,
    name: "Ghana",
  },
  {
    id: 4,
    name: "Indian Ocean (Mauritius)",
  },
  {
    id: 5,
    name: "Kenya",
  },
  {
    id: 6,
    name: "Kenya Valuations",
  },
  {
    id: 7,
    name: "Mozambique",
  },
  {
    id: 8,
    name: "Namibia",
  },
  {
    id: 9,
    name: "Nigeria",
  },
  {
    id: 10,
    name: "Eswatini",
  },
  {
    id: 11,
    name: "Uganda",
  },
  {
    id: 12,
    name: "Zambia",
  }
]

const data = [
  { id: 1,
    title: 'South Africa',
    info: [
      { id: 1, title: 'Johannesburg (Head Office)', addresses: [
          '61 Katherine Street', 'Sandown Ext. 54', 'Sandton'
        ], tel: '+27 11 441 4000', fax: '+27 11 441 4452', email: 'info@broll.com'
      },
      { id: 2, title: 'Bloemfontein', addresses: [
          '2nd Floor Offices', '10 Barnes Street', 'Arboretum', 'Bloemfontein, 9301'
        ], tel: '+27 51 430 3008', fax: '+27 51 430 3187'
      },
      { id: 3, title: 'Cape Town', addresses: [
          '8th Floor', '80 Strand Street', 'Cape Town'
        ], tel: '+27 21 419 7373', fax: '+27 21 419 4688'
      },
      { id: 4, title: 'Durban', addresses: [
          '3rd Floor Office', '102 Stephen Dlamini Road', 'Musgrave', 'Durban'
        ], tel: '+27 31 362 1700', fax: '+27 31 337 0306'
      },
      { id: 5, title: 'Gqeberha', addresses: [
          '73 2nd Avenue', 'Newton Park', 'Gqeberha'
        ], tel: '+27 41 363 5559', fax: '+27 41 363 3388'
      },
      { id: 6, title: 'Pretoria', tel: '+27 12 470 8660', fax: '+27 11 441 4452'
      },
      { id: 7, title: 'Nelspruit', addresses: [
          'Office 12B, 7th Floor', 'Nelspruit Sanlam Centre', '25 Samora Machel Drive', 'Nelspruit'
        ], tel: '+27 13 752 6671', fax: '+27 11 441 4452'
      },
      { id: 8, title: 'Limpopo', addresses: [
          '7 Neethling Street', 'Hampton Court', 'Bendor', 'Polokwane', 'Limpopo'
        ], tel: '+27 15 291 4722', fax: '+27 11 441 4452'
      },
      { id: 9, title: 'Johannesburg (Auctions and sales)', addresses: [
          'Suite 4, 1st Floor, Atholl Square', 'Cnr. Katherine Drive & Wierda Road East', 'Sandown Ext 3', 'Sandton'
        ], tel: '+27 87 700 8289', email: 'auctionsales@broll.com'
      },
    ]
  },
  { id: 2,
    title: 'Botswana',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          'Office 115, Plot 139', 'Gaborone International Finance Park', 'Gaborone,Botswana'
        ], tel: '+267 39 81973', email: 'botswana@broll.com'
      }
    ]
  },
  { id: 3,
    title: 'Ghana',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          '7th Floor', 'Ridge Tower', '6th Avenue', 'Ridge', 'Accra'
        ], tel: '+233 (0) 302 672 888', email: 'ghana@broll.com'
      }
    ]
  },
  { id: 4,
    title: 'Indian Ocean (Mauritius)',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          'c/o Intercontinental Trust Ltd', 'Level 3, Alexandra House', '35 Cybercity', 'Ebene', 'Mauritius'
        ], tel: '+230 403 0800', email: 'indianocean@broll.com'
      }
    ]
  },
  { id: 5,
    title: 'Kenya',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          '6th Floor', 'Fedha Plaza', 'Mpaka Road', 'Westlands', 'Nairobi'
        ], tel: '+254 712 668 448', email: 'kenya@broll.com'
      }
    ]
  },
  { id: 6,
    title: 'Kenya Valuations',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          '6th Floor', 'Fedha Plaza', 'Mpaka Road', 'Westlands', 'Nairobi'
        ], tel: '+254 712 668 448', email: 'kenya@broll.com'
      }
    ]
  },
  { id: 7,
    title: 'Mozambique',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          'Avenue 24 de Julho', 'No 1123', '2 Andar', 'Cidade de Maputo'
        ], tel: '+258 21 487 095', email: 'mozambique@broll.com'
      }
    ]
  },
  { id: 8,
    title: 'Namibia',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          'Zanlumor Building', '2nd Floor', 'Post Street Mall', 'Windhoek', 'Namibia', 'P.O. Box 2309, Windhoek, Namibia'
        ], tel: '+264 61 374 500', fax: '+264 61 237 499', email: 'info@brollnamibia.com.na'
      }
    ]
  },
  { id: 9,
    title: 'Nigeria',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          '3rd Floor', 'Number one, Lagos', '1, Akin Adesola Street', 'Victoria Island', 'Lagos'
        ], tel: '+234 1 270 1890', email: 'nigeria@broll.com'
      }
    ]
  },
  { id: 10,
    title: 'Eswatini',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          'Lot 507', 'Usuthu Crescent Industrial Sites', 'Matsapha', 'Eswatini'
        ], tel: '+268 251 87566', email: 'swaziland@broll.com'
      }
    ]
  },
  { id: 11,
    title: 'Uganda',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          'Rwenzori House', 'Ground floor', 'Plot 1 Lumumba Avenue', 'Kampala'
        ], tel: '+256 313 673569', email: 'uganda@broll.com'
      }
    ]
  },
  { id: 12,
    title: 'Zambia',
    info: [
      { id: 1, title: 'Broll Office', addresses: [
          'Manda Hill Mall', 'Cnr Great East & Manchinchi Roads', 'Lusaka'
        ], tel: '+260 211 255550', email: 'zambia@broll.com'
      }
    ]
  },
]


const ContactDetails: React.FC<Props> = ({ current, onClick }) => {

  return (
    <div className={ cn(s.ContactDetails) }>
      <div className={ cn(s.layout) }>
        <h3 className='semiBig m-0 red-2 bold'>Contact Details</h3>
        <div className={ s.wrapper }>
          <div className={ s.left }>
            { items.map(item =>
              <div className={ cn(s.item, { [s.active]: item.name === current }) } key={ item.id } onClick={ () => onClick(item.name) }>
                <div className={ cn(s.icon, 'cursor-pointer') }>
                  <Icon src={ ArrowIcon } rotate={ 90 } size='xs'/>
                </div>
                <div className={ cn(s.name, 'cursor-pointer') }>
                  <p className='m-0 letterSpacingNormal'>{ item.name }</p>
                </div>
              </div>
            )}
          </div>
          <div className={ s.right }>
            <div className={ cn(s.layout, 'scrollbar-hidden') }>
              { data.map(item =>
                <div key={ item.id } className={ cn(s.item, { [s.hidden]: item.title !== current }) }>
                  <p className='bold m-0'>{ item.title }</p>
                  { item.info.map(i =>
                    <div key={ i.id } className={ s.i }>
                      <p className='bold'>{ i.title }</p>
                      { i.addresses && i.addresses.map((el, index) =>
                        <div className={ s.addresses } key={ index }>
                          <p className='mt-0 mb-1'>{ el }</p>
                        </div>
                      )}
                      <div className={ cn(s.el, 'mb-1') }>
                        <p className='m-0'>Tel</p>
                        <p className='m-0'>{ i.tel }</p>
                      </div>
                      { i.fax &&
                        <div className={ cn(s.el, 'mb-1') }>
                          <p className='m-0'>Fax</p>
                          <p className='m-0'>{ i.fax }</p>
                        </div>
                      }
                      { i.email &&
                        <div className={ cn(s.el, 'mb-1') }>
                          <p className='m-0'>Email</p>
                          <p className='m-0'>{ i.email }</p>
                        </div>
                      }
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ContactDetails
