import React, { useEffect, useState } from 'react'
import { GoogleMap as GoogleMapComponent, Marker, LoadScript, InfoWindow } from '@react-google-maps/api'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  onClick: (name: string) => void
  current: string
}

const config = {
  apiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY || ""
}

const markers = [
  { id: 1, title: 'Botswana', lat: -24.6925484, lng: 25.87984 },
  { id: 2, title: 'Ghana', lat:5.558901, lng:-0.217897 },
  { id: 3, title: 'Indian Ocean (Mauritius)', lat:-20.24327, lng:57.487630 },
  { id: 4, title: 'Kenya', lat:-1.263597, lng:36.804374 },
  { id: 5, title: 'Kenya Valuations', lat: -1.2635972, lng: 36.8043742 },
  { id: 6, title: 'Mozambique', lat: -25.8960874, lng: 32.5406431 },
  { id: 7, title: 'Namibia', lat: -22.5664357, lng: 17.0803239 },
  { id: 8, title: 'Nigeria', lat: 6.4236191, lng: 3.4216048 },
  { id: 9, title: 'Eswatini', lat: -26.5190942, lng: 31.294249 },
  { id: 10, title: 'Uganda', lat: 0.3164922, lng: 32.5777311 },
  { id: 11, title: 'Zambia', lat: -15.3999669, lng: 28.3039433 },
  { id: 12, title: 'South Africa', lat: -26.1097184, lng: 28.0558401 },
  { id: 13, title: 'South Africa', lat: -29.1103017, lng: 26.2159756 },
  { id: 14, title: 'South Africa', lat: -33.9196545, lng: 18.4181434 },
  { id: 15, title: 'South Africa', lat: -29.8492293, lng: 30.9960533 },
  { id: 16, title: 'South Africa', lat: -33.9471151, lng: 25.5697822 },
  { id: 17, title: 'South Africa', lat: -25.8848302, lng: 28.2017193 },
  { id: 18, title: 'South Africa', lat: -25.4726811, lng: 30.9744973 },
  { id: 19, title: 'South Africa', lat: -23.8951927, lng: 29.4780232 },
  { id: 20, title: 'South Africa', lat: -26.1060917, lng: 28.0638011 }
]

const GoogleMap: React.FC<Props> = ({ onClick, current }) => {
  const [localCurrent, setLocalCurrent] = useState<null | number>(null)
  const [loading, setLoading] = useState(true)

  const handleCurrent = (id: number, name: string) => {
    setLocalCurrent(id)
    onClick(name)
  }

  useEffect(() => {
    !loading &&
    setLocalCurrent(markers.find(item => item.title === current)?.id || 1)
  }, [current, loading])


  return (
    <div className={ cn(s.GoogleMap) }>
      <div className={ s.layout }>
        <LoadScript googleMapsApiKey={ config.apiKey }>
          <GoogleMapComponent
            options={{
              mapTypeControl: false,
              fullscreenControl: false,
              panControl: false,
              streetViewControl: false,
              controlSize: 25,
            }}
            mapContainerStyle={{ width: '100%', height: '100%' }}
            center={{ lat: -15.3999669, lng: 28.3039433 }}
            zoom={ 3 }
            onLoad={ () => setLoading(false)}
          >
            { markers.map(item =>
              <Marker position={{ lat: item.lat, lng: item.lng }} onClick={ () => handleCurrent(item.id, item.title) } key={ item.id }>
                { localCurrent === item.id &&
                  <InfoWindow onCloseClick={ () => setLocalCurrent(null) }>
                    <p className='m-0 letterSpacingNormal'>{ item.title }</p>
                  </InfoWindow>
                }
              </Marker>
            )}
          </GoogleMapComponent>
        </LoadScript>
      </div>
    </div>
  )
}

export default GoogleMap
