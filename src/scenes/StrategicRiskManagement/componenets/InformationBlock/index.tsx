import React from 'react'

import { CloudImage } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrollStrategicRiskManagementPic from 'assets/pictures/brollStrategicRiskManagement'
import PeopleIcon from 'assets/icons/people.png'
import ProcessIcon from 'assets/icons/process.png'

const data = [
  { id: 1, title: 'Risk Management Consulting', description: "Health, safety and security related broad-based risk consulting to clients portfolio's. This service line implements international standardisation whilst driving efficiency on the bottom line. This deliberately enables a more resilient business to deal with business unusual.", icon: PeopleIcon },
  { id: 2, title: 'Intelligence and Reporting', description: "Market and trend insights to portfolio specific footprints and nuances, our team comprises of research and analytical expertise to not only deliver daily accurate reporting but also market analytical feedback to ensure you can make your decisions with the correct dashboard of information.", icon: PeopleIcon },
  { id: 3, title: 'Risk Technology', description: "Design, Develop, Deploy and service an integrated end to end solution to retail and commercial property portfolios across Africa.", icon: ProcessIcon },
  { id: 4, title: 'Risk Project Management', description: "Solving complex project-based challenges with the scale of Broll across Africa. Our track record of commercially viable solutions to project continuity is industry leading.", icon: ProcessIcon },
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ cn(s.section1) }>
          <div className={ s.image }>
            <CloudImage
              src={ BrollStrategicRiskManagementPic }
              alt="Broll Broking"
              className='covered'
              responsive={{
                desktop: { w: '1920' }
              }}
            />
          </div>
          <div className={ s.line }/>
        </div>
        <div className={ s.section2 }>
          <p className='light large m-0 letterSpacingNormal'>Our strategic risk consultancy operates across Africa with a diverse understanding of security, health and safety in markets such as FMCG, energy, finance and industry.</p>
          <h3 className='semiBig red-2 bold mt-4 mb-0'>Our services include</h3>
          <div className={ s.list }>
            { data.map(item =>
              <div className={ s.item } key={ item.id }>
                <div className={ s.left }>
                  <div className={ s.icon }>
                    <img src={ item.icon } alt="Broll" className='contained'/>
                  </div>
                </div>
                <div className={ s.right }>
                  <h4 className='red-4 m-0 regular'>{ item.title }</h4>
                  <p className='small light no-mobile no-tablet m-0 letterSpacingNormal'>{ item.description }</p>
                  <p className='light no-desktop m-0 letterSpacingNormal'>{ item.description }</p>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
