import React from 'react'

import { Banner } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import BrollAcademyPic from 'assets/pictures/brollAcademy'

const leftList = [
  "Established in 2002, the Broll Academy aims to improve with the property skills and knowledge of Broll staff members.",
  "It was the first of its kind to provide in-house training for employees in the commercial, retail and industrial property sectors.",
  "Our goal continues to be lifelong learning for our employees.",
  "Academy courses empower employees to be more productive in the workplace, gain confidence and maximise long-term potential within the Group.",
  "Broll Academy is associated with accredited education training providers who offer long and short courses. These include, Introduction to Property Principles, Property Management Programme, Certificate in Shopping Centre Management, Advanced Certificate in Shopping Centre Management and soft-skills courses including time management and report writing for example.",
  "As part of our efforts to improve the skills of our employees and develop candidates within the property industry, we’ve introduced two new training programmes – internships and learnerships",
]

const rightList = [
  "As a member of the Services SETA we qualify for a skills-development grant which helps us offer programmes such as these internships. We welcomed our first two interns in 2008 and the programme has swiftly grown year on year.",
  "We identify university graduates from various property courses and place them on a fast-track process in various departments within Broll to a point where we can offer them positions that best fit their skills and personalities.",
  "A learnership is a year-long course with emphasis on-the-job learning which combines education and training with work experience.",
  "A team leader, supervisor or line manager mentors the candidates in the workplace.",
  "Working together with Services SETA and Boston City Campus, the aim of the programme is to widen the skills base in the property industry. The project is spearheaded by a dedicated facilitator.",
  "Learnership qualifications include Business Administration, Customer Management, Information Technology and Management."
]

const InformationBlock: React.FC = () => {
  return (
    <div className={ cn(s.InformationBlock) }>
      <div className={ cn(s.layout) }>
        <div className={ s.top }>
          <Banner image={ BrollAcademyPic } title='Our goal' description='continues to <br/>be lifelong learning <br/>for our employees.'/>
          <div className={ s.line }/>
        </div>
        <div className={ s.bottom }>
          <div className={ s.left }>
            { leftList.map(item =>
              <p className='small light no-mobile no-tablet m-0 letterSpacingNormal' key={ item }>{ item }</p>
            )}
            { leftList.map(item =>
              <p className='light no-desktop m-0 letterSpacingNormal' key={ item }>{ item }</p>
            )}
          </div>
          <div className={ s.right }>
            <h3 className='semiBig m-0 red bold'>The Intern Programme</h3>
            <div className={ s.list }>
              { rightList.map((item) =>
                <p key={ item } className='small light no-mobile no-tablet m-0 letterSpacingNormal'>{ item }</p>
              )}
              { rightList.map(item =>
                <p key={ item } className='light no-desktop m-0 letterSpacingNormal'>{ item }</p>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InformationBlock
