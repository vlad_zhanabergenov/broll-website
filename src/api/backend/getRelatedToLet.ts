import apolloClient from 'services/apollo'
import { QUERY_RELATED_TO_LET } from 'api/queries'
import addDealTypeField from 'services/addDealTypeField'

export default async function getRelatedToLet(
  property_gmaven_key: string,
  category: string,
  suburb: string
) {
  let items: AnyProperty[] = []

  const { data } = await apolloClient.query<QueryRelatedToLetRes>({
    query: QUERY_RELATED_TO_LET,
    variables: { category, property_gmaven_key, suburb }
  }).catch(() => ({ data: null }))

  if (data?.queryRelatedToLet) {
    items = addDealTypeField(data.queryRelatedToLet)
  }

  return items
}
