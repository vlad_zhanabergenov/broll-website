import { QUERY_FEATURED_FOR_SALE, QUERY_FEATURED_TO_LET } from 'api/queries'
import { sortBy } from 'lodash'
import apolloClient from 'services/apollo'
import addDealTypeField from 'services/addDealTypeField'
import groupPropertiesByGmavenKey from 'services/groupPropertiesByGmavenKey'

export default async function getFeaturedOfferings() {
  const items: AnyProperty[] = []

  const [
    { data: featuredToLetRes },
    { data: featuredForSaleRes }
  ] = await Promise.all([
    await apolloClient.query<QueryFeaturedToLetRes>({
      query: QUERY_FEATURED_TO_LET
    }).catch(() => ({ data: null })),
    await apolloClient.query<QueryFeaturedForSaleRes>({
      query: QUERY_FEATURED_FOR_SALE
    }).catch(() => ({ data: null }))
  ])

  if (featuredToLetRes?.queryFeaturedToLet) {
    featuredToLetRes.queryFeaturedToLet.forEach(i => i.property_featured === 1 && items.push(i))
  }

  if (featuredForSaleRes?.queryFeaturedForSale) {
    featuredForSaleRes.queryFeaturedForSale.forEach(i => i.property_featured === 1 && items.push(i))
  }

  return sortBy(
    groupPropertiesByGmavenKey(addDealTypeField(items)),
    ['property_name']
  )
}
