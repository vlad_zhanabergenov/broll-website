import apolloClient from 'services/apollo'
import { sortBy } from 'lodash'
import { QUERY_PROPERTY_FOR_SALE } from 'api/queries'
import addDealTypeField from 'services/addDealTypeField'

export default async function getPropertyForSale(gmaven_mapped_key: string) {
  let items: AnyProperty[] = []

  const { data } = await apolloClient.query<QueryPropertyForSaleRes>({
    query: QUERY_PROPERTY_FOR_SALE,
    variables: { gmaven_mapped_key }
  }).catch(() => ({ data: null }))

  if (data?.queryPropertyForSale) {
    items = sortBy(addDealTypeField(data.queryPropertyForSale), ['gross_price'])
  }

  return items
}
