export { default as getStaticProperties } from './getStaticProperties'
export { default as getPropertiesCount } from './getPropertiesCount'
export { default as getPropertyToLetId } from './getPropertyToLetId'
export { default as getPropertyForSaleId } from './getPropertyForSaleId'
