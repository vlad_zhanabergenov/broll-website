import searchClient from 'services/algoliaAdmin'

export default async function getPropertyForSaleId(slug: string) {
  let gmaven_mapped_key: string | null = null

  const searchIndex = searchClient.initIndex('ForSale')

  return searchIndex.browseObjects<PropertyToLet>({
    query: '',
    filters: `property_slug:"${ slug }"`,
    batch: res => {
      gmaven_mapped_key = res[0].gmaven_mapped_key || null
    }
  })
  .then(() => gmaven_mapped_key)
  .catch(() => gmaven_mapped_key)
}
