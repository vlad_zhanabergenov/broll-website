import searchClient from 'services/algoliaAdmin'
import slugParser from 'services/slugParser'

export default async function getPropertyToLetId(slug: string) {
  let property_gmaven_key: string | null = null

  const searchIndex = searchClient.initIndex('ToLet')
  const parsedSlug = slugParser.parse(slug)

  let filters

  if (parsedSlug.unit_id) {
    filters = `unit_slug:"${ slug }"`
  } else {
    filters = `property_slug:"${ slug }"`
  }

  return searchIndex.browseObjects<PropertyToLet>({
    query: '',
    filters,
    batch: res => {
      property_gmaven_key = res[0].property_gmaven_key || null
    }
  })
  .then(() => property_gmaven_key)
  .catch(() => property_gmaven_key)
}
