import searchClient from 'services/algoliaAdmin'

export default async function getPropertiesCount(index: 'ToLet' | 'ForSale') {
  let results = 0

  const searchIndex = searchClient.initIndex(index)

  return searchIndex.browseObjects<AnyProperty>({
    query: '',
    batch: res => {
      res.forEach(() => results++)
    }
  })
  .then(() => results)
  .catch(() => results)
}
