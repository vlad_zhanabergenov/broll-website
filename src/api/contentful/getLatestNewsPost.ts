import content from 'services/contentful'
import parseMediaItem from 'services/parseMediaItem'

export default async function getLatestNewsPost(slug: string) {
  const res = await content.getEntries<NewsMediaItemContentful>({
    content_type: 'news',
    'fields.slug[match]': slug,
    include: 3
  })

  if (res.items?.[0]) {
    return parseMediaItem(res.items[0].fields, 'news')
  }
}
