import content from 'services/contentful'
import parseMediaItem from 'services/parseMediaItem'

export default async function getNewsMediaItems() {
  const items: NewsMediaItem[] = []

  const res = await content.getEntries<NewsMediaItemContentful>({ content_type: "news" })

  if (res.items) {
    res.items.forEach(({ fields }) => {
      items.push(parseMediaItem(fields, 'news'))
    })
  }

  return items
}
