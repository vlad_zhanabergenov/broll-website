import content from 'services/contentful'

export default async function getStaticLatestNews(limitPerRequest: number = 500) {
  const slugList: string[] = []

  const request = async (skip: number) => {
    const res = await content.getEntries<NewsMediaItemContentful>({
      content_type: "news",
      select: ["fields.slug"],
      limit: limitPerRequest,
      skip
    })

    res.items.forEach(({ fields }) => {
      slugList.push(fields.slug)
    })

    if (res.total > slugList.length) {
      await request(slugList.length)
    }
  }

  await request(0)

  return slugList
}
