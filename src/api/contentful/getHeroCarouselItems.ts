import content from 'services/contentful'

export default async function getHeroCarouselItems() {
  const items: HeroCarouselItem[] = []

  const res = await content.getEntries<HeroCarouselItemContentful>({ content_type: "homePageSlider" })

  if (res.items) {
    res.items.forEach(({ fields }, num) => {
      items.push({
        id: `${ num }`,
        heading: fields.mainHeading,
        headingSubText: fields.mainHeadingSubText,
        image: fields.sliderImage,
        content: fields.sliderBlurb
      })
    })
  }

  return items
}
