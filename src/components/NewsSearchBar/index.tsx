import React from 'react'

import { Query, CategoryFilter } from './components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  values: Form
  change: (data: { name: string, value: any }) => void
  onReset: () => void
}

export type Form = {
  query: string
  category: string[]
}

const NewsSearchBar: React.FC<Props> = ({ values, change, onReset }) => {
  return (
    <div className={ cn(s.NewsSearchBar) }>
      <div className={ s.layout }>
        <div className={ cn(s.form) }>
          <Query value={ values.query } onChange={ change } labelStart/>
          <CategoryFilter value={ values.category } onChange={ change }/>
          <p className="mb-0 ml-1 cursor-pointer" onClick={ onReset }>Reset</p>
        </div>
      </div>
    </div>
  )
}

export default NewsSearchBar
