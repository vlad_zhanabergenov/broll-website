import React, { useEffect } from 'react'
import { useForm } from 'hooks'
import _ from 'lodash'
import { RefinementListProvided } from 'react-instantsearch-core'
import { connectRefinementList } from 'react-instantsearch-dom'

import { Select } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  value: string[]
  onChange: (data: { name: string, value: string[] }) => void
}

type Form = {
  category: string[]
}

const CategoryFilterRaw: React.FC<Props & RefinementListProvided> = ({
  value, onChange,
  items, currentRefinement, refine
}) => {
  const sortedItems = _.sortBy(items, ['-count'])
  const categories = sortedItems.map(item => item.label)

  useEffect(() => {
    if (!_.isEqual(currentRefinement, value)) {
      refine(value)
    }
  }, [value])

  const { values, change } = useForm<Form>({
    fields: {
      category: { initialValue: value }
    },
    onChange: (name, value) => {
      onChange({ name, value })
    }
  }, [value])

  return (
    <div className={ cn(s.CategoryFilter) }>
      <Select multiWithAll name="category" options={ categories } value={ values.category }
              onChange={ change } label="Category" labelDark/>
    </div>
  )
}

const CategoryFilterConnected = connectRefinementList(CategoryFilterRaw)

const CategoryFilter: React.FC<Props> = (props) => (
  <CategoryFilterConnected attribute='fields.category.en-US' { ...props } limit={ 1000 }/>
)

export default CategoryFilter
