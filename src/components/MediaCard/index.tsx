import React from 'react'
import moment from 'moment'

import { CloudImage } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  isFeaturedPreview?: boolean
  item: NewsMediaItem,
  className?: string
}

const MediaCard: React.FC<Props> = ({
  isFeaturedPreview,
  item,
  className
}) => {
  return (
    <div className={ cn(s.MediaCard, "overflow-hidden box-sizing", { [s.isFeaturedPreview]: isFeaturedPreview }, className) }>
      <div className={ s.layout }>
        <div className={ s.top }>
          <div className={ cn(s.type, "box-sizing pv-05 ph-4") }>
            <p className="bold xx-small uppercase m-0">{ item.type }</p>
          </div>
          <div className={ cn(s.image, "overflow-hidden box-sizing") }>
            <CloudImage
              className="covered"
              src={ item.featuredImage }
              alt={ item.title }
              responsive={{
                desktopLarge: { w: '500' },
                desktop: { w: '400' },
                tablet: { w: '350' },
                mobile: { w: '300' }
              }}
            />
          </div>
          { isFeaturedPreview &&
            <div className={ cn(s.imageLabel, "pt-3 pb-1 ph-2 box-sizing") }>
              <h3 className="big bold white mt-0 mb-05 limit-string-2">{ item.category }</h3>
            </div>
          }
        </div>
        <div className={ cn(s.center, "pt-2 mb-1 ph-2 box-sizing") }>
          <p className="semiBold mt-0 mb-1 pr-4 limit-string-2">{ item.title }</p>
          <p className="small light grey mv-1 limit-string-4 letterSpacingNormal">{ item.articleSummary }</p>
          { isFeaturedPreview &&
            <p className={ cn(s.date, "small light grey m-0 pr-4") }>
              { moment(item.publishDate).format("DD MMMM YYYY") }
            </p>
          }
        </div>
        { isFeaturedPreview ?
          <div className={ cn(s.bottom, "pb-1 ph-2 box-sizing") }>
            <div className={ s.author }>
              <p className="semiBold mt-1 mb-0 limit-string-1">{ item.author?.name || "" }</p>
            </div>
          </div>
        :
          <div className={ cn(s.bottom, "pb-1 ph-2 box-sizing") }>
            <p className={ cn(s.date, "small light grey mt-05 mb-1") }>
              { moment(item.publishDate).format("DD MMMM YYYY") }
            </p>
            <div className={ s.actions }>
              <p className="semiBold m-0 limit-string-1">{ item.author?.name }</p>
            </div>
          </div>
        }
      </div>
    </div>
  )
}

export default MediaCard
