import React from 'react'

import { Icon } from 'components'

import s from './style.module.sass'
import LoadingIcon from 'assets/icons/loading.svg'

interface Props {
  size?: 's' | 'm' | 'l' | 'xl'
}

const Loading: React.FC<Props> = ({ size = 'm' }) => {
  return (
    <div className={ s.Loading }>
      <div className={ s.spinner }>
        <Icon src={ LoadingIcon } size={ size }/>
      </div>
    </div>
  )
}

export default Loading
