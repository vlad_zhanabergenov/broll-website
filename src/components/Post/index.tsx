import React from 'react'

import { CloudImage, ContactCard } from 'components'
import parseRichText from 'services/parseRichText'

import s from './style.module.sass'
import cn from 'classnames'

interface Props {
  data: NewsMediaItem
}

const Post: React.FC<Props> = ({ data }) => {
  return (
    <div className={ s.Post }>
      <div className={ s.layout }>
        <div className={ cn(s.section1) }>
          <div className={ s.left }>
            <div className={ s.image }>
              <CloudImage
                className="covered"
                src={ data.featuredImage }
                alt={ data.title }
                responsive={{
                  desktopLarge: { w: '900' },
                  desktop: { w: '800' },
                  tablet: { w: '600' },
                  mobile: { w: '400' }
                }}
              />
            </div>
          </div>
          <div className={ s.right }>
            <h3 className='m-0 white light semiBig'>{ data.title }</h3>
          </div>
        </div>
        <div className={ s.section2 }>
          <div className={ s.content }>
            { parseRichText(data.articleCopy, {
              pClass: "big light grey-3 letterSpacingNormal m-0"
            }) }
          </div>
          { data.author &&
            <div className={ cn(s.author, 'no-tablet no-mobile') }>
              <h3 className='regular m-0'>Author</h3>
              <div className={ s.wrapper }>
                <ContactCard
                  full
                  data={{
                    name: data.author.name,
                    title: data.author.title,
                    img:  data.author.profilePicture,
                    email: data.author.email,
                    number: data.author.telephone
                  }}
                />
              </div>
            </div>
          }
        </div>
      </div>
    </div>
  )
}

export default Post
