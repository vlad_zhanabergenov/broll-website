import { useReactiveVar } from '@apollo/client'
import React, { useEffect, useState } from 'react'
import { HitsProvided } from 'react-instantsearch-core'
import { connectHits } from 'react-instantsearch-dom'
import { appVar } from 'cache/vars'
import parseMediaItem from 'services/parseMediaItem'

import { MediaCard } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

// const TEST_list: NewsMediaItem[] = [
//   {
//     id: "01",
//     type: 'blog',
//     featuredImage: "https://assets.publishing.service.gov.uk/government/uploads/system/uploads/image_data/file/109178/s960_Cinematic-Blue-Earth-View-From-Space-At-Night-To-Europe-889946406_5616x3744.jpeg",
//     heroImage: "https://assets.publishing.service.gov.uk/government/uploads/system/uploads/image_data/file/109178/s960_Cinematic-Blue-Earth-View-From-Space-At-Night-To-Europe-889946406_5616x3744.jpeg",
//     category: "blog",
//     author: {
//       name: "Test Name 1",
//       title: "Test title 1",
//       email: "",
//       telephone: "",
//       bio: "",
//       profilePicture: ""
//     },
//     publishDate: "2005-08-09T18:31:42",
//     featuredArticle: false,
//     title: "What does the future of fitness look like?",
//     articleSummary: "Broll Property Intel's latest Retail Snapshot, The Future of Fitness, takes an in-depth look at how COVID-19 has affected the fitness industry, how people are staying fit while confined to their homes and what....."
//   },
//   {
//     id: "02",
//     type: 'blog',
//     featuredImage: "https://assets.publishing.service.gov.uk/government/uploads/system/uploads/image_data/file/109178/s960_Cinematic-Blue-Earth-View-From-Space-At-Night-To-Europe-889946406_5616x3744.jpeg",
//     heroImage: "https://assets.publishing.service.gov.uk/government/uploads/system/uploads/image_data/file/109178/s960_Cinematic-Blue-Earth-View-From-Space-At-Night-To-Europe-889946406_5616x3744.jpeg",
//     category: "blog",
//     author: {
//       name: "Test Name 2",
//       title: "Test title 2",
//       email: "",
//       telephone: "",
//       bio: "",
//       profilePicture: ""
//     },
//     publishDate: "2005-08-09T18:31:42",
//     featuredArticle: false,
//     title: "How Workplace Strategy will shape the office of the future",
//     articleSummary: "A great deal has been written about Covid-19 and its effect on business operations and workspaces. What is evident is that the corporate workplace landscape will never be the same again."
//   }
// ]

const Results: React.FC<HitsProvided<any>> = ({ hits }) => {
  const { algoliaReady } = useReactiveVar(appVar)
  const [list, setList] = useState<NewsMediaItem[]>([])

  useEffect(() => {
    setList(hits.map(i => parseMediaItem(i.fields, 'blog')))
  }, [hits])

  const handleResetFilters = () => {
    // resetFields({ ...initialState.search, dealType })
  }

  if (!algoliaReady) {
    return null
  }

  return (
    <div className={ cn(s.BlogResults) }>
      <div className={ s.layout }>
        { hits.length ?
          <div className={ s.list }>
            { list.map(item =>
              <MediaCard key={ item.id } item={ item }/>
            )}
          </div>
        :
          <h3 className="medium mv-10 text-center">Your search returned no results. Please <span className='cursor-pointer bold red' onClick={ handleResetFilters }>reset</span> your filter and try again.</h3>
        }
      </div>
    </div>
  )
}

export default connectHits(Results)
