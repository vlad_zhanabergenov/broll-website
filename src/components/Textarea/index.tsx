import React from 'react'

import cn from 'classnames'

interface Props {
  name: string
  className?: string
  secondary?: boolean
  fontS?: boolean
  required?: boolean
  disabled?: boolean
  error?: string | null
  label?: string
  labelDark?: boolean
  placeholder?: string
  value?: string | number
  maxLength?: number
  rows?: number
  tabIndex?: number
  onChange?: (data: { name: string, value: string | number }) => void
  onBlur?: (name: string) => void
  onPressEnter?: any
}

const Textarea: React.FC<Props> = ({
  name,
  className,
  secondary,
  fontS,
  required,
  disabled,
  // error,
  label,
  labelDark,
  placeholder,
  value,
  maxLength,
  rows,
  tabIndex,
  onChange,
  onBlur,
  onPressEnter
}) => {
  const handleChange = ({ target }: React.ChangeEvent<HTMLTextAreaElement>) => {
    if (onChange) {
      onChange({
        name, value: target.value
      })
    }
  }

  const handleBlur = () => {
    if (onBlur) {
      onBlur(name)
    }
  }

  const handleKeyPress = (event: React.KeyboardEvent<HTMLTextAreaElement>) => {
    const key = event.keyCode || event.charCode
    if (onPressEnter && key === 13) {
      event.preventDefault()
      onPressEnter()
    }
  }

  return (
    <div className={ cn("Textarea", { secondary, required }, className || "") }>
      { label &&
        <div className="label">
          <p className={ cn("no-select light", labelDark ? "black" : "white") }>{ label }</p>
        </div>
      }
      <textarea
        name={ name }
        placeholder={ placeholder || "" }
        value={ value }
        onChange={ handleChange }
        onBlur={ handleBlur }
        onKeyPress={ handleKeyPress }
        rows={ rows }
        disabled={ disabled }
        className={ cn({ fontS }) }
        maxLength={ maxLength }
        tabIndex={ tabIndex }
      />
    </div>
  )
}

export default Textarea
