import cn from 'classnames'
import React from 'react'

import s from './style.module.sass'
import LogoPic from 'assets/pictures/logo.svg'

interface Props {
  size?: 's' | 'm'
}

const Logo: React.FC<Props> = ({ size = 'm' }) => {
  return (
    <div className={ s.Logo }>
      <div className={ cn(s.layout, [s[size]]) }>
        <img className="contained" src={ LogoPic } alt="Broll logo"/>
      </div>
    </div>
  )
}

export default Logo
