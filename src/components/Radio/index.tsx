import React from 'react'

import cn from 'classnames'

interface Props {
  name: string
  options: string[]
  multi?: boolean
  multiWithAll?: boolean
  secondary?: boolean
  label?: string
  white?: boolean
  small?: boolean
  disabled?: boolean
  textAfter?: Array<string | number>
  value: string | string[]
  className?: string
  onChange?: (data: { name: string, value: string | string[] }) => void
}

const Radio: React.FC<Props> = ({
  name,
  options,
  multi,
  multiWithAll,
  secondary,
  white,
  small,
  disabled,
  textAfter,
  value,
  className,
  onChange,
}) => {
  const handleChange = (v: string) => {
    let newValue: Props['value'] = v

    if (typeof value === 'object') {
      if (multiWithAll && v === "All") {
        newValue = [v]
      } else if (multiWithAll) {
        const filteredValue = value.filter(item => item !== "All")
        if (filteredValue.includes(v)) {
          newValue = filteredValue.filter(item => item !== v)
        } else {
          if (options.length === [...filteredValue, v].length) {
            newValue = ["All"]
          } else {
            newValue = [...filteredValue, v]
          }
        }
      } else {
        if (value.includes(v)) {
          newValue = value.filter(item => item !== v)
        } else {
          newValue = [...value, v]
        }
      }
    }

    if (onChange) {
      onChange({ name, value: newValue })
    }
  }

  return (
    <div className={ cn("Radio cursor-pointer", { "no-pointer-events": disabled, secondary }, className) }>
      { multiWithAll &&
        <div className="item mv-1" onClick={ () => handleChange("All") }>
          <div className={ cn("checkbox", { white, small, active: value.includes("All") }) }/>
          <h3 className={ cn("label m-0 limit-string-1", { white, small }) }>{ "All" }</h3>
          { textAfter &&
            <p className="textAfter light small greyBlue m-0">{ textAfter[0] }</p>
          }
        </div>
      }
      { options && options.map((i, num) => {
        const active = (multi || multiWithAll) ? value?.includes(i) : value === i
        return (
          <div key={ i + num } className="item mv-1" onClick={ () => handleChange(i) }>
            <div className={ cn("checkbox", { white, small, active }) }/>
            <h3 className={ cn("label m-0 limit-string-1", { white, small }) }>{ i }</h3>
            { textAfter &&
              <p className="textAfter light small greyBlue m-0">{ textAfter[multiWithAll ? num + 1 : num] || textAfter[0] }</p>
            }
          </div>
        )
      })}
    </div>
  )
}

export default Radio
