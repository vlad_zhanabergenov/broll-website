import React from 'react'

import { Link } from 'components'
import { Props as LinkProps } from 'components/Link'

import cn from 'classnames'
import s from './style.module.sass'

export interface Props {
  items: Array<string | [string, LinkProps['to'] | LinkProps]>
  className?: string
}

type ItemWithId = {
  id: string
  title: string
  link?: LinkProps['to'] | LinkProps
}

const Breadcrumbs: React.FC<Props> = ({ items, className }) => {
  const elements: ItemWithId[] = [
    { id: '0', title: "Home", link: '/' }
  ]

  items.forEach((item, num) => {
    elements.push({
      id: `${ num + 1 }`,
      title: typeof item === 'string' ? item : item[0],
      link: typeof item === 'string' ? undefined : item[1]
    })
  })

  return (
    <div className={ cn(s.Breadcrumbs, className || "") }>
      <div className={ cn(s.list) }>
        <p className='small medium mv-0 mr-05'>You are here:</p>
        { elements.map((i, num) => {
          const linkProps = i.link ? (typeof i.link === 'string' ? { to: i.link } : { ...i.link }) : { transparent: true }

          return (
            <React.Fragment key={ i.id }>
              <Link { ...linkProps }>
                <span className={ cn(s.element, 'small light grey') }>{ i.title }</span>
              </Link>
              { num + 1 < elements.length && <span className='small light grey mh-05 no-pointer-events'>/</span> }
            </React.Fragment>
          )
        })}
      </div>
    </div>
  )
}

export default Breadcrumbs
