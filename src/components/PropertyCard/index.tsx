import React from 'react'
import { useReactiveVar } from '@apollo/client'
import { appVar } from 'cache/vars'
import { startCase } from 'lodash'
import formatNumberWith from 'services/formatNumberWith'
import roundNumber from 'services/roundNumber'

import { CloudImage, Icon, Link, SpecialOffer } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import ViewIcon from 'assets/icons/view.svg'
import PropertyCardSlider from '../PropertyCardSlider'
import NoPropertyImage from 'assets/pictures/noPropertyImage'

interface Props {
  data: AnyProperty | AnyPropertyWithCustomFields
}

const PropertyCard: React.FC<Props> = ({ data }) => {
  const { algoliaStats } = useReactiveVar(appVar)
  
  const unitsCount = algoliaStats.facets.property_gmaven_key?.[data.property_gmaven_key || ""] || 1

  const hasSpecialDeals = ('hasSpecialDeals' in data && data.hasSpecialDeals) || ('special_deals' in data && data.special_deals !== null)

  return (
    <div className={ cn(s.PropertyCard, "pv-1 ph-05 box-sizing") }>
      <div className={ s.layout }>
        <div className={ s.top }>
          { hasSpecialDeals &&
            <SpecialOffer className={ cn(s.special) }/>
          }
          <div className={ cn(s.label, "box-sizing pt-05 ph-1 no-pointer-events") }>
            <p className="bold x-small uppercase m-0">
              { `${ data.property_category } ${ startCase(data.dealType) }` }
            </p>
          </div>
          <div className={ s.image }>
            { data.property_images !== undefined && data.property_images?.length > 1 ?
              <PropertyCardSlider images={ data.property_images } data={ data }/>
            :
              <Link to='property' data={{ property: data }}>
                <CloudImage
                  className="covered"
                  src={ data.best_image || data.property_images?.[0]?.image_path_url || NoPropertyImage }
                  alt={ data.property_name }
                  responsive={{
                    desktop: { w: '500', ar: '1.5' },
                    tablet: { w: '400', ar: '1.5' },
                    mobile: { w: '300', ar: '1.2' }
                  }}
                />
              </Link>
            }
          </div>
          <div className={ cn(s.imageLabel, "pb-1 ph-3 box-sizing") }>
            <h3 className="semiBig bold white m-0">
              { !data.gross_price ?
                  "POA"
              : data.dealType === 'toLet' ?
                unitsCount > 1 ?
                  `from R${ formatNumberWith(" ", roundNumber(data.gross_price)) } per m²`
                :
                  `R${ formatNumberWith(" ", roundNumber(data.gross_price)) } per m²`
              :
                `R${ formatNumberWith(" ", roundNumber(data.gross_price)) }`
              }
            </h3>
          </div>
        </div>
        <Link to='property' data={{ property: data }}>
          <div className={ cn(s.bottom, "pt-3 pb-1 ph-2 box-sizing") }>
            <h3 className="bold mt-0 mb-2 limit-string-1">{ data.property_name }</h3>
            <p className="light limit-string-2">
              { data.marketing?.property_marketing_description || "No description provided" }
            </p>
            <div className={ s.info }>
              <div className={ cn(s.el, "mb-1") }>
                <p className="small light m-0">GLA:</p>
                <p className="small light m-0">
                  { data.min_gla !== data.max_gla ?
                    `${ formatNumberWith(' ', data.min_gla || 0) }m² - ${ formatNumberWith(' ', data.max_gla || 0) }m²`
                  :
                    `${ formatNumberWith(' ', data.min_gla || 0) }m²`
                  }
                </p>
              </div>
              { data.dealType === 'toLet' &&
                <div className={ s.el }>
                  <p className="small light m-0">Units:</p>
                  <p className="small light m-0">{ unitsCount }</p>
                </div>
              }
            </div>
            <div className={ s.view }>
              <Icon src={ ViewIcon }/>
            </div>
          </div>
        </Link>
      </div>
    </div>
  )
}

export default PropertyCard
