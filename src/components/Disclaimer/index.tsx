import React from 'react'

import cn from 'classnames'
import s from './style.module.sass'

const Disclaimer: React.FC = () => {
  return (
    <div className={ cn(s.Disclaimer, "light grey mv-4") }>
      <p className="light grey">
        Disclaimer: While every effort will be made to ensure that the information contained within the Broll website is accurate and up to date, Broll makes no warranty, representation or undertaking whether expressed or implied, nor do we assume any legal liability, whether direct or indirect, or responsibility for the accuracy, completeness, or usefulness of any information. Prospective purchasers and tenants should make their own enquiries to verify the information contained herein.
      </p>
    </div>
  )
}

export default Disclaimer
