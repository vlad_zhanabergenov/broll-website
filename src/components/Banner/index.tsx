import React from 'react'

import { CloudImage } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import Pic from 'assets/pictures/noPropertyImage'
import LogoPic from 'assets/pictures/logo.svg'
import SecondaryLogoPic from 'assets/pictures/occupierServicesBannerLogo'

interface Props {
  title?: string
  secondaryTitle?: string
  image?: string
  description?: string
  secondary?: boolean
  lineHeightNormal?: boolean
}

const Banner: React.FC<Props> = ({
  title,
  image,
  description,
  secondaryTitle,
  secondary,
  lineHeightNormal
}) => {
  return (
    <div className={ cn(s.Banner) }>
      <div className={ cn(s.layout) }>
        <div className={ s.image }>
          <CloudImage
            src={ image || Pic }
            alt="Broll"
            className='covered'
            responsive={{
              desktop: { w: '2440' }
            }}
          />
        </div>
        { secondary ?
          <div className={ s.secondaryLogo }>
            <CloudImage
              className="contained"
              src={ SecondaryLogoPic } alt="Broll"
              responsive={{
                desktop: { w: '600' },
                tablet: { w: '500' },
                mobile: { w: '300' }
              }}
            />
          </div>
        :
          <div className={ s.wrapper }>
            <div className={ s.logo }>
              <img src={ LogoPic } alt="broll" className='contained'/>
            </div>
            <div className={ s.tag }>
              { title &&
                <>
                  <h3 className={ cn('light m-0 no-mobile no-tablet letterSpacingNormal', { [s.lineHeightNormal]: lineHeightNormal }) } dangerouslySetInnerHTML={{ __html: title  }}/>
                  <h3 className={ cn('light m-0 semiBig no-desktop letterSpacingNormal', { [s.lineHeightNormal]: lineHeightNormal }) } dangerouslySetInnerHTML={{ __html: title  }}/>
                </>
              }
              { secondaryTitle &&
                <div className={ s.secondaryTitle }>
                  <p className='bold small m-0 no-mobile no-tablet letterSpacingNormal' dangerouslySetInnerHTML={{ __html: secondaryTitle }}/>
                  <p className='bold m-0 no-desktop letterSpacingNormal ' dangerouslySetInnerHTML={{ __html: secondaryTitle }}/>
                </div>
              }
              { description &&
                <>
                  <p className='light small m-0 no-mobile no-tablet letterSpacingNormal' dangerouslySetInnerHTML={{ __html: description }}/>
                  <p className='light m-0 no-desktop letterSpacingNormal' dangerouslySetInnerHTML={{ __html: description }}/>
                </>
              }
            </div>
          </div>
        }
      </div>
    </div>
  )
}

export default Banner
