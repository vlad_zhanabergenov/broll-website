import React from 'react'

import cn from 'classnames'

interface Props {
  name: string
  secondary?: boolean
  label?: string
  large?: boolean
  noBorder?: boolean
  grey?: boolean
  white?: boolean
  small?: boolean
  className?: string
  disabled?: boolean
  value?: boolean
  onChange?: (data: { name: string, value: boolean }) => void
}

const Checkbox: React.FC<Props> = ({
  name,
  secondary,
  large,
  noBorder,
  grey,
  label,
  white,
  small,
  className,
  disabled,
  value,
  onChange,
}) => {
  const handleChange = () => {
    if (onChange) {
      onChange({ name, value: !value })
    }
  }

  return (
    <div className={ cn("Checkbox cursor-pointer", { "no-pointer-events": disabled, secondary, large, grey, noBorder }, className || "") }
         onClick={ handleChange }>
      <div className="wrapper">
        <div className={ cn( "checkbox", { white, small, 'active': value }) }/>
        <h3 className={ cn("label m-0", { white, small }) }>{ label }</h3>
      </div>
    </div>
  )
}

export default Checkbox
