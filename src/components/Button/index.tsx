import React from 'react'

import { Icon } from 'components'

import cn from 'classnames'

interface Props {
  full?: boolean
  disabled?: boolean
  secondary?: boolean
  transparent?: boolean
  dark?: boolean
  white?: boolean
  red?: boolean
  rounded?: boolean
  loading?: boolean
  icon?: string
  iconHover?: string
  iconSize?: 's' | 'm' | 'l' | 'xl'
  iconRotate?: 0 | 45 | 90 | 135 | 180 | 225 | 270 | 315
  backgroundBlur?: boolean
  className?: string
  onClick?: () => void
  darkBackground?: boolean
  largeMobile?: boolean
}

const Button: React.FC<Props> = ({
  full,
  disabled,
  secondary,
  transparent,
  dark,
  white,
  red,
  rounded,
  loading,
  icon,
  iconHover,
  iconSize = "m",
  iconRotate = 0,
  className = "",
  backgroundBlur,
  onClick,
  darkBackground,
  largeMobile,
  children
}) => {
  return (
    <div
      className={ cn("Button no-select cursor-pointer", {
        full, disabled, secondary, transparent, dark, white, red, rounded,
        "withIcon": icon, "withIconHover": iconHover,
        "no-pointer-events": disabled || loading,
        "noContent": !children,
        backgroundBlur, darkBackground, largeMobile
      }, className) }
      onClick={ onClick }
    >
      { children && children }
      { icon &&
        <div className="icon">
          <Icon src={ icon } size={ iconSize } rotate={ iconRotate } position={ children ? 'right' : null }/>
          { iconHover &&
            <Icon src={ iconHover } size={ iconSize } rotate={ iconRotate } position={ children ? 'right' : null }/>
          }
        </div>
      }
    </div>
  )
}

export default Button
