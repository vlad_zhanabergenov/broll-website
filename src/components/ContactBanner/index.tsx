import { Button, ContactCard, Link } from 'components'
import React from 'react'
import cn from 'classnames'

import s from './style.module.sass'

import ContactBannerPic from 'assets/pictures/contactBanner'

interface Props {
  data?: {
    name: string,
    title: string,
    img: string,
    email: string,
    number: string
  }
  noLine?: boolean
}

const defaultData = {
  name: 'Norman Raad',
  title: 'CEO of Auctions and Sales',
  img: ContactBannerPic,
  email: 'nraad@broll.com',
  number: '+27 87 700 8290'
}

const ContactBanner: React.FC<Props> = ({ data, noLine }) => {
  const item = data || defaultData

  return (
    <div className={ cn(s.ContactBanner) }>
      <div className={ cn(s.layout) }>
        <div className={ s.left }>
          <ContactCard data={ item } full/>
          { !noLine && <div className={ cn(s.line, 'no-tablet no-mobile') }/> }
        </div>
        <div className={ s.right }>
          <p className='large medium white m-0 no-tablet no-mobile'>Broll’s African <br/>footprint</p>
          <h3 className='large medium white mt-0 no-desktop mb-1'>Broll’s African <br/>footprint</h3>
          <p className='small m-0 white light'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
          <Link to='/about/african-footprint'>
            <Button darkBackground>View</Button>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default ContactBanner
