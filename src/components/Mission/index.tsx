import React from 'react'

import { CloudImage } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import MissionPic from 'assets/pictures/mission'

const Mission: React.FC = () => {
  return (
    <div className={ cn(s.Mission, "overflow-hidden") }>
      <div className={ cn(s.layout, "ph-6 box-sizing") }>
        <div className={ s.left }>
          <h3 className="big bold white mt-0 mb-05">Our Mission</h3>
          <p className={ cn(s.text, "small light white m-0") }>
            To build a high-performance, respectful and dynamic culture that enables 
            professional real estate services, driven through trusted internal and 
            external relationships.
          </p>
        </div>
        <div className={ cn(s.right, "mr-5") }>
          <div className={ s.picture }>
            <CloudImage
              className="covered"
              src={ MissionPic } alt="Broll. We're on a Mission"
              responsive={{
                desktop: { w: '800' },
                tablet: { w: '500' },
                mobile: { w: '300' }
              }}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Mission
