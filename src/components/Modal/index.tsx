import React, { memo, useEffect, useState } from 'react'
import { createPortal } from 'react-dom'

import { Icon } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import CrossIcon from 'assets/icons/cross.svg'
import CrossLightIcon from 'assets/icons/crossLight.svg'

interface IPortalProps {
  id: string
}

interface IModalProps {
  onClose?: () => void
  secondary?: boolean
}

const Portal: React.FC<IPortalProps> = memo(({ id, children }) => {
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    setMounted(true)
  }, [])

  if (mounted) {
    return createPortal(children, document.getElementById(id) as Element)
  } else {
    return null
  }
})

const Modal: React.FC<IModalProps> = ({
  children,
  onClose,
  secondary
}) => {
  useEffect(() => {
    const modalDom = document.getElementById("Modal")
    document.body.style.overflow = "hidden"
    if (modalDom) { modalDom.style.height = "100%" }
    return () => {
      document.body.style.overflowY = "scroll"
      if (modalDom) { modalDom.style.height = "auto" }
    }
  }, [])

  return (
    <Portal id="Modal">
      <div className={ cn(s.ModalWrapper, "scrollable-y ph-content box-sizing", { [s.secondary]: secondary }) }>
        <div className={ cn(s.container, "p-4 box-sizing") }>
          { onClose &&
            <div className={ cn(s.close, "cursor-pointer no-select") } onClick={ onClose }>
              <Icon src={ secondary ? CrossLightIcon : CrossIcon } size={ secondary? 'xs' : 'm' }/>
            </div>
          }
          <div className={ s.layout }>
            { children }
          </div>
        </div>
      </div>
    </Portal>
  )
}

export default Modal
