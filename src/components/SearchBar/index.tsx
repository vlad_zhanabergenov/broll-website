import { useReactiveVar } from '@apollo/client'
import initialState from 'cache/initialState'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { useRouter } from 'next/router'
import React from 'react'
import { useThrottle, useForm } from 'hooks'
import searchStateParser from 'services/searchStateParser'
import { searchOptions } from 'utilities/constants'

import { Input, Select } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'query' | 'dealType'>

interface Props {
  reverse?: boolean
  onInputFocus?: () => void
}

const SearchBar: React.FC<Props> = ({
  reverse,
  onInputFocus
}) => {
  const router = useRouter()

  const searchState = useReactiveVar(searchVar)

  const handleInputFocus = () => {
    if (onInputFocus) {
      onInputFocus()
    }
  }

  const handleQuery = useThrottle((value: any) => {
    setField('query', value)
  })

  const handleChange = (name: keyof Options, value: any) => {
    if (name === 'dealType') {
      setField(name, value)
    } else {
      handleQuery(value)
    }
  }

  const handleSubmit = () => {
    router.push(`/search?${ searchStateParser.encode({
      ...initialState.search,
      query: searchState.query,
      dealType: searchState.dealType
    }) }`)
  }

  const { values, change, submit} = useForm<Options>({
    fields: {
      query: { initialValue: searchState.query },
      dealType: { initialValue: searchState.dealType }
    },
    onChange: handleChange,
    handleSubmit
  }, [searchState])

  const fields = [
    <Select key="dealType" name="dealType" className={ s.col1 } options={ searchOptions.dealType } value={ values.dealType }
            onChange={ change } placeholder="eg: Rent"/>,
    <Input key="query" name="query" type="search" className={ s.col2 } value={ values.query } onChange={ change } onFocus={ handleInputFocus }
           textBefore="Search" placeholder="eg: Sandton" noAutofill withFullFillIndicator onPressEnter={ submit }/>
  ]

  return (
    <div className={ cn(s.SearchBar) }>
      <div className={ s.layout }>
        <div className={ cn(s.form) }>
          { reverse ? fields.reverse() : fields }
        </div>
      </div>
    </div>
  )
}

export default SearchBar
