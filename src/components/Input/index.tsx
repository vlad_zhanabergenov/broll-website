import React, { useState } from 'react'

import { Icon } from 'components'

import cn from 'classnames'

interface Props {
  name: string
  type?: "email" | "number" | "password" | "search" | "tel" | "text" | "url"
  className?: string
  noBorder?: boolean
  secondary?: boolean
  linear?: boolean
  transparent?: boolean
  white?: boolean
  fontS?: boolean
  icon?: string
  iconHover?: string
  iconSize?: 's' | 'm' | 'l' | 'xl'
  iconPosition?: 'left' | 'right'
  onIconClick?: () => void
  required?: boolean
  disabled?: boolean
  noAutofill?: boolean
  message?: string | null
  error?: string | null
  mediumFont?: boolean
  label?: string
  labelDark?: boolean
  placeholder?: string
  textBefore?: string
  textBeforeSmall?: string
  textBeforeRight?: boolean
  value?: string | number
  min?: number
  max?: number
  maxLength?: number
  tabIndex?: number
  onChange?: (data: { name: string, value: string | number }) => void
  onFocus?: (name: string) => void
  onBlur?: (name: string) => void
  onPressEnter?: any
  withFillIndicator?: boolean
  withFullFillIndicator?: boolean
  labelStart?: boolean
}

const Input: React.FC<Props> = ({
  name,
  type,
  className,
  secondary,
  noBorder,
  mediumFont,
  linear,
  transparent,
  white,
  fontS,
  icon,
  iconHover,
  iconSize = "m",
  iconPosition = "left",
  onIconClick,
  required,
  disabled,
  noAutofill,
  message,
  error,
  label,
  labelDark,
  placeholder,
  textBefore,
  textBeforeSmall,
  textBeforeRight,
  value,
  min,
  max,
  maxLength,
  tabIndex,
  onChange,
  onFocus,
  onBlur,
  onPressEnter,
  labelStart,
  withFullFillIndicator,
  withFillIndicator
}) => {
  const [focus, setFocus] = useState(false)

  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    let newValue = target.value

    if (type === 'number') {
      if (typeof min === 'number' && parseInt(newValue) < min) {
        newValue = `${ min }`
      } else if (typeof max === 'number' && parseInt(newValue) > max) {
        newValue = `${ max }`
      }
    }

    if (onChange) {
      onChange({
        name, value: newValue
      })
    }
  }

  const handleFocus = () => {
    setFocus(true)
    if (onFocus) {
      onFocus(name)
    }
  }

  const handleBlur = () => {
    setFocus(false)
    if (onBlur) {
      onBlur(name)
    }
  }

  const handleIconClick = () => {
    if (onIconClick) {
      onIconClick()
    }
  }

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (onPressEnter && e.key === 'Enter') {
      e.preventDefault()
      onPressEnter()
    }
  }

  const IconWrapper: React.FC = () => {
    if (!icon) { return null }
    return (
      <div
        className={ cn("icon", {
          "right": iconPosition === 'right',
          "cursor-pointer": !!onIconClick || iconHover,
          "withIconHover": iconHover
        }) }
       onClick={ handleIconClick }
      >
        <Icon src={ icon } size={ iconSize }/>
        { iconHover &&
          <Icon src={ iconHover } size={ iconSize }/>
        }
      </div>
    )
  }

  return (
    <div
      className={ cn("Input", {
        secondary, linear, transparent, white, required, noBorder,
        withFillIndicator: withFillIndicator && value,
        withFullFillIndicator: withFullFillIndicator && value
      }, className || "") }
    >
      { label &&
        <div className={ cn("label", { labelStart }) }>
          <p className={ cn("no-select light", labelDark ? "black" : "white") }>{ label }</p>
        </div>
      }
      <div className={ cn("value", { focus, textBeforeSmall, textBeforeRight }) }>
        { icon && iconPosition === 'left' && <IconWrapper/> }
        { (textBefore || textBeforeSmall) &&
          <p className={ cn("before m-0 no-select no-pointer-events", { 'medium': textBefore }) }>
            { textBefore || textBeforeSmall }
          </p>
        }
        <input
          type={ type || "text" }
          name={ name }
          placeholder={ placeholder || "" }
          value={ value }
          onChange={ handleChange }
          onFocus={ handleFocus }
          onBlur={ handleBlur }
          onKeyDown={ handleKeyDown }
          disabled={ disabled }
          autoComplete={ noAutofill || type === 'search' ? "off" : "on" }
          className={ cn({ withIcon: icon, textBefore, textBeforeSmall, fontS, mediumFont }) }
          min={ min }
          max={ max }
          maxLength={ maxLength }
          tabIndex={ tabIndex }
        />
        { required && (typeof value === 'string' && value.length === 0) &&
          <p className="star light m-0 no-select no-pointer-events">{ placeholder }<span className='red'>*</span></p>
        }
        { icon && iconPosition === 'right' &&
          <IconWrapper/>
        }
      </div>
      { (error || message) &&
        <p className={ cn("message small extraLight m-0 ph-1 no-pointer-events box-sizing", white ? 'white' : 'black') }>
          { error || message }
        </p>
      }
    </div>
  )
}

export default Input
