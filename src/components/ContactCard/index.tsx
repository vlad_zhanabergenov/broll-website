import React from 'react'

import s from './style.module.sass'
import cn from 'classnames'
interface Props {
  data: {
    name: string,
    title?: string,
    img: string,
    email: string,
    number: string
  }
  full?: boolean
}

const ContactCard: React.FC<Props> = ({ data, full }) => {
  return (
    <div className={ cn(s.ContactCard, { [s.full]: full }) }>
      <div className={ cn(s.layout) }>
        <div className={ cn(s.imageWrapper, 'overflow-hidden') }>
          
        </div>
        <div className={ cn(s.wrapper, 'p-2 box-sizing') }>
          <div className={ s.top }>
            <h2 className='small mt-0 mb-05 dark-5 semiBold'>{ data.name }</h2>
            <p className='large white medium m-0'>{ data.title }</p>
          </div>
          { !!(data.email || data.number) &&
            <div className={ cn(s.bottom, 'mb-2') }>
              <p className='large light white mt-0 mb-05'>Contact me</p>
              <p className='large light white medium m-0'>
                { !data.email && data.number ?
                  data.number
                :
                  <>
                    <span className='large red-2'>{ data.email }</span>
                    { data.number &&
                      <>
                        <span className='large light mh-05'> | </span> { data.number }
                      </>
                    }
                  </>
                }
              </p>
            </div>
          }
        </div>
      </div>
    </div>
  )
}

export default ContactCard
