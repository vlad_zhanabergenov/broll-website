import React from 'react'
import cn from 'classnames'

import s from './style.module.sass'

interface Props {
  className?: string
}

const SpecialOffer: React.FC<Props> = ({ className }) => (
  <div className={ cn(s.SpecialOffer, className) }>
    <p className='uppercase white semiBold large mv-05 mh-05'>Special offer</p>
  </div>
)

export default SpecialOffer
