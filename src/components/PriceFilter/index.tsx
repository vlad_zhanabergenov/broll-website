import { isEqual } from 'lodash'
import React, { useEffect } from 'react'
import { useReactiveVar } from '@apollo/client'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { useForm } from 'hooks'
import { connectRange } from 'react-instantsearch-dom'

import { Input } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'minPrice' | 'maxPrice'>

interface Props {
  secondary?: boolean
  className?: string
}

const PriceFilterRaw: React.FC<Props & any> = ({
  secondary, className,
  min, max,
  currentRefinement, refine
}) => {
  const searchState = useReactiveVar(searchVar)
  const { minPrice, maxPrice } = searchState

  useEffect(() => {
    let parsedMin: number
    let parsedMax: number

    if (minPrice === "" || Number(minPrice) < min) {
      parsedMin = min
    } else if (Number(minPrice) > max) {
      parsedMin = max
    } else {
      parsedMin = Number(minPrice)
    }

    if (maxPrice === "" || Number(maxPrice) > max) {
      parsedMax = max
    } else if (Number(maxPrice) < min) {
      parsedMax = min
    } else {
      parsedMax = Number(maxPrice)
    }

    if (typeof parsedMin !== 'undefined' || typeof parsedMax !== 'undefined') {
      const newValue = { min: parsedMin, max: parsedMax }
      if (!isEqual(currentRefinement, newValue)) {
        refine(newValue)
      }
    }
  }, [min, max, searchState])

  const onChange = (name: keyof Options, value: any) => {
    setField(name, value)
  }

  const { values, change } = useForm<Options>({
    fields: {
      minPrice: { initialValue: minPrice },
      maxPrice: { initialValue: maxPrice }
    }, onChange
  }, [minPrice, maxPrice])

  if (secondary) {
    return (
      <div className={ cn(s.PriceFilter) }>
        <Input secondary name="minPrice" type='number' fontS className="mb-05" min={ 0 } placeholder="Min Price"
               value={ values.minPrice } onChange={ change } withFillIndicator/>
        <Input secondary name="maxPrice" type='number' fontS min={ 1 } placeholder="Max Price"
               value={ values.maxPrice } onChange={ change } withFillIndicator/>
      </div>
    )
  }

  return (
    <>
      <Input name="minPrice" type='number' fontS className={ cn("mb-05", className || "") } min={ 0 } placeholder="Min Price"
             value={ values.minPrice } onChange={ change }/>
      <Input name="maxPrice" type='number' fontS className={ className } min={ 1 } placeholder="Max Price"
             value={ values.maxPrice } onChange={ change }/>
    </>
  )
}

const PriceFilterConnected = connectRange(PriceFilterRaw)

const PriceFilter: React.FC<Props> = (props) => (
  <PriceFilterConnected attribute='gross_price' { ...props }/>
)

export default PriceFilter
