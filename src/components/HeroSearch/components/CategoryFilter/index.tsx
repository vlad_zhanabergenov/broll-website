import { useReactiveVar } from '@apollo/client'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { useForm } from 'hooks'
import { isEqual, sortBy } from 'lodash'
import React, { useEffect } from 'react'
import { RefinementListProvided } from 'react-instantsearch-core'
import { connectRefinementList } from 'react-instantsearch-dom'

import { Select } from 'components/index'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'propertyType'>

interface Props {
  theme?: 'light' | 'dark'
  className?: string
}

const CategoryFilterRaw: React.FC<Props & RefinementListProvided> = ({
  theme = 'light',
  className,
  items,
  currentRefinement, refine
}) => {
  const themeDark = theme === 'dark'

  const searchState = useReactiveVar(searchVar)
  const { propertyType } = searchState

  const sortedItems = sortBy(items, ['-count'])
  const propertyTypes = sortedItems.map(item => item.label)

  useEffect(() => {
    if (!currentRefinement || !isEqual(currentRefinement, propertyType)) {
      refine(propertyType)
    }
  }, [searchState])

  const onChange = (_name: string, value: any) => {
    setField('propertyType', value)
  }

  const { values, change } = useForm<Options>({
    fields: {
      propertyType: { initialValue: propertyType }
    }, onChange
  }, [propertyType])

  return (
    <div className={ cn(s.CategoryFilter, className || "") }>
      <Select multiWithAll name="propertyType" options={ propertyTypes } value={ values.propertyType }
              onChange={ change } label="Property Type" labelDark={ themeDark } placeholder="eg: Office"/>
    </div>
  )
}

const CategoryFilterConnected = connectRefinementList(CategoryFilterRaw)

const CategoryFilter: React.FC<Props> = (props) => (
  <CategoryFilterConnected attribute='property_category' { ...props } limit={ 1000 }/>
)

export default CategoryFilter
