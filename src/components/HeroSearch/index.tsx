import React, { useState} from 'react'
import { useReactiveVar } from '@apollo/client'
import { useRouter } from 'next/router'
import { useDeviceType, useForm } from 'hooks'
import searchStateParser from 'services/searchStateParser'
import { searchOptions } from 'utilities/constants'
import { searchVar } from 'cache/vars'
import { setField } from 'cache/mutations/search'

import { CategoryFilter } from './components'
import { Button, Input, Select, MinGLAFilter, MaxGLAFilter, PriceFilter } from 'components'

import cn from 'classnames'
import s from 'components/HeroSearch/style.module.sass'
import SearchIcon from 'assets/icons/search.svg'
import SearchPlusIcon from 'assets/icons/searchPlus.svg'
import MenuRed from 'assets/icons/menuRed.svg'

type Options = Pick<AppCache['search'], 'query' | 'dealType' | 'minGLA' | 'maxGLA' | 'minPrice' | 'maxPrice'>

interface Props {
  theme?: 'light' | 'dark'
  mobileFullOnly?: boolean
}

const HeroSearch: React.FC<Props> = ({
  theme = 'light',
  mobileFullOnly
}) => {
  const themeDark = theme === 'dark'

  const router = useRouter()
  const deviceType = useDeviceType()

  const searchState = useReactiveVar(searchVar)

  const [full, setFull] = useState(!!mobileFullOnly)

  const toggleFull = () => {
    setFull(prevState => !prevState)
  }

  const handleChange = (name: keyof Options, value: any) => {
    setField(name, value)
  }

  const handleSubmit = () => {
    router.push(`/search?${ searchStateParser.encode(searchState) }`)
  }

  const { values, change, submit } = useForm<Options>({
    fields: {
      query: { initialValue: searchState.query },
      dealType: { initialValue: searchState.dealType },
      minGLA: { initialValue: searchState.minGLA },
      maxGLA: { initialValue: searchState.maxGLA },
      minPrice: { initialValue: searchState.minPrice },
      maxPrice: { initialValue: searchState.maxPrice }
    },
    onChange: handleChange,
    handleSubmit
  }, [searchState])

  const Props = {
    query: {
      name: "query", className: s.col2, type: 'search', value: values.query, onChange: change, onPressEnter: submit,
      label: "Search Broll Properties", labelDark: themeDark, textBefore: "Search", placeholder: "eg: Sandton", noAutofill: true
    },
    queryMobile: {
      name: "query", className: s.col2, type: 'search', value: values.query, onChange: change, onPressEnter: submit,
      label: "Search Broll Properties", labelDark: themeDark, textBefore: "Search", placeholder: "eg: Sandton",
      icon: !mobileFullOnly ? MenuRed : undefined, iconPosition: 'right', iconSize: 'l', onIconClick: toggleFull, noAutofill: true
    },
    dealType: {
      name: "dealType", className: s.col1, options: searchOptions.dealType, value: values.dealType,
      onChange: change, label: "Deal type", labelDark: themeDark, placeholder: "eg: Rent"
    }
  } as const

  return (
    <div className={ cn(s.HeroSearch) }>
      <div className={ s.layout }>
        { deviceType !== 'mobile' ?
          <div className={ cn(s.form, "no-mobile") }>
            <Input { ...Props.query }/>
            <div>
              <Select { ...Props.dealType }/>
            </div>
            <CategoryFilter className={ s.col1 } theme={ theme }/>
            <Button className={ s.submit } rounded dark={ themeDark } icon={ SearchIcon } iconHover={ SearchPlusIcon } onClick={ submit }/>
            <MinGLAFilter className={ s.col1 }/>
            <MaxGLAFilter className={ s.col1 }/>
            <PriceFilter className={ s.col1 }/>
          </div>
        :
          <>
            <div className={ cn(s.form, "no-desktop no-tablet") }>
              <Input { ...Props.queryMobile }/>
              { full &&
                <>
                  <div>
                    <Select { ...Props.dealType }/>
                  </div>
                  <CategoryFilter className={ cn(s.col1, 'mb-1') } theme={ theme }/>
                  <MinGLAFilter className={ s.col1 }/>
                  <MaxGLAFilter className={ s.col1 }/>
                  <PriceFilter className={ s.col1 }/>
                </>
              }
            </div>
            <div className={ cn(s.searchButton, "no-desktop no-tablet") }>
              <Button rounded icon={ SearchPlusIcon } backgroundBlur={ !themeDark } onClick={ submit }>Search</Button>
            </div>
          </>
        }
      </div>
    </div>
  )
}

export default HeroSearch
