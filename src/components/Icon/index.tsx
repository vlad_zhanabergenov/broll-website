import React from 'react'

import cn from 'classnames'

interface Props {
  src: string
  size?: 'xs' | 's' | 'm' | 'l' | 'xl' | 'xxl' | 'xxxl' | 'xxxxl',
  className?: string,
  position?: 'right' | 'left' | null
  rotate?: 0 | 45 | 90 | 135 | 180 | 225 | 270 | 315
}

const Icon: React.FC<Props> = ({
  src,
  size = 'm',
  className = "",
  position = '',
  rotate
}) => {
  const style: React.CSSProperties = { transition: '.3s' }

  if (rotate) {
    style.transform = `rotate(${ rotate }deg)`
  }

  return (
    <div className={ cn("Icon", size, position || '', className) }>
      <img
        src={ src }
        alt="icon"
        className="contained"
        style={ style }
      />
    </div>
  )
}

export default Icon
