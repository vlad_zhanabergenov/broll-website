import { useReactiveVar } from '@apollo/client'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { useForm } from 'hooks'
import { isEqual } from 'lodash'
import React, { useEffect } from 'react'
import { connectRange } from 'react-instantsearch-dom'

import { Input } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'maxGLA'>

interface Props {
  secondary?: boolean
  className?: string
}

const MaxGLAFilterRaw: React.FC<Props & any> = ({
  secondary, className,
  min, max,
  currentRefinement, refine
}) => {
  const searchState = useReactiveVar(searchVar)
  const { maxGLA } = searchState

  useEffect(() => {
    const parsedMin = min
    let parsedMax: number

    if (maxGLA === "" || Number(maxGLA) > max) {
      parsedMax = max
    } else if (Number(maxGLA) < min) {
      parsedMax = min
    } else {
      parsedMax = Number(maxGLA)
    }

    if (typeof parsedMin !== 'undefined' || typeof parsedMax !== 'undefined') {
      const newValue = { min: parsedMin, max: parsedMax }
      if (!isEqual(currentRefinement, newValue)) {
        refine(newValue)
      }
    }
  }, [min, max, searchState])

  const onChange = (name: keyof Options, value: any) => {
    setField(name, value)
  }

  const { values, change } = useForm<Options>({
    fields: {
      maxGLA: { initialValue: maxGLA }
    }, onChange
  }, [maxGLA])

  return (
    <div className={ cn(s.MaxGLAFilter, className || "") }>
      <Input secondary={ secondary } name="maxGLA" type='number' fontS min={ 0 } placeholder="Max GLA"
             value={ values.maxGLA } onChange={ change } withFillIndicator/>
    </div>
  )
}

const MaxGLAFilterConnected = connectRange(MaxGLAFilterRaw)

const MaxGLAFilter: React.FC<Props> = (props) => (
  <MaxGLAFilterConnected attribute='max_gla' { ...props }/>
)

export default MaxGLAFilter
