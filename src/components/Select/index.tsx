import React, { useEffect, useRef, useState } from 'react'
import { useOnClickOutside } from 'hooks'

import { Icon } from 'components'

import cn from 'classnames'
import ArrowBoldIcon from 'assets/icons/arrowBold.svg'

interface Props {
  name: string
  options: string[]
  noBorder?: boolean
  required?: boolean
  multi?: boolean
  multiWithAll?: boolean
  secondary?: boolean
  label?: string
  fontLarge?: boolean
  labelDark?: boolean
  placeholder?: string
  small?: boolean
  disabled?: boolean
  value: string | string[]
  className?: string
  onChange?: (data: { name: string, value: string | string[] }) => void
  onBlur?: (name: string) => void
}

const Select: React.FC<Props> = ({
  name,
  options,
  required,
  noBorder,
  multi,
  multiWithAll,
  secondary,
  fontLarge,
  label,
  labelDark,
  placeholder,
  small,
  disabled,
  value,
  className,
  onChange,
  onBlur
}) => {
  const [listOpen, setListOpen] = useState(false)
  const ref = useRef<HTMLDivElement>(null)

  useEffect(() => {
    value && onBlur && onBlur(name)
  }, [value])

  useOnClickOutside(ref, () => {
    listOpen && setListOpen(false)
  })

  const toggleList = () => {
    if (ref.current) {
      if (!listOpen) {
        setListOpen(true)
      } else {
        setListOpen(false)
      }
    }
  }

  const handleChange = (v: string) => {
    let newValue: Props['value'] = v

    if (typeof value === 'object') {
      if (multiWithAll && v === "ALL") {
        newValue = []
      } else if (multiWithAll) {
        if (value.includes(v)) {
          newValue = value.filter(item => item !== v)
        } else {
          if (options.length === value.length + 1) {
            newValue = []
          } else {
            newValue = [...value, v]
          }
        }
      } else {
        if (value.includes(v)) {
          newValue = value.filter(item => item !== v)
        } else {
          newValue = [...value, v]
        }
      }
    }

    if (onChange) {
      onChange({ name, value: newValue })
    }

    !multi && !multiWithAll && setListOpen(false)
  }


  return (
    <div className={ cn("Select cursor-pointer", { "no-pointer-events": disabled, secondary, required, noBorder, fontLarge }, className) }>
      <div ref={ ref } className={ cn("wrapper", { "open": listOpen, disabled }) }>
        { label &&
          <div className="label text-left">
            <p className={ cn("no-select light m-0", labelDark ? "black" : "white") }>{ label }</p>
          </div>
        }
        <div className="input" onClick={ () => toggleList() }>
          <p className={ cn("no-select m-0", { small }) }>
            { value ?
                (multi || multiWithAll) ?
                  value[0] || (multiWithAll ? "All" : placeholder)
                : value
              : required ?
                <>{ placeholder }<span className='red'>*</span></>
              :
                placeholder
            }
          </p>
          { ((multi || multiWithAll) && value?.length && value.length > 1) ?
              <p className={ cn("indicator no-select m-0 pv-05 pl-05", { small }) }>{ `+${ value.length - 1 }` }</p>
            : null
          }
          <Icon src={ ArrowBoldIcon } size='xs' rotate={ 0 }/>
        </div>
        <div className="options scrollbar-hidden">
          <div className="list pt-05">
            { multiWithAll &&
              <div onClick={ () => handleChange("ALL") } className={ cn("item", { active: !value.length }) }>
                <p className={ cn("no-select text-left", { small }, !value.length ? "black" : "grey") }>All</p>
              </div>
            }
            { options.map(item => {
              const active = (multi || multiWithAll) ? value?.includes(item) : value === item
              return (
                <div key={ item } onClick={ () => handleChange(item) } className={ cn("item", { active }) }>
                  <p className={ cn("no-select text-left", { small }, active ? "black" : "grey") }>{ item }</p>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Select
