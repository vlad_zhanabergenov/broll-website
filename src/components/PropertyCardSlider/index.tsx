import React, { useRef } from 'react'
import Slider, { Settings } from 'react-slick'

import { CloudImage, Icon, Link } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowRedIcon from 'assets/icons/arrowRed.svg'

interface Props {
  images: Image[]
  data: AnyProperty
}

const settings: Settings = {
  className: 'slickSlider',
  lazyLoad: 'ondemand',
  centerMode: true,
  centerPadding: "0",
  slidesToShow: 1,
  speed: 500,
  dots: true
}

const PropertyCardSlider: React.FC<Props> = ({ images, data }) => {
  const sliderRef = useRef<Slider>(null)

  const handleChange = (type: 'prev' | 'next') => {
    if (sliderRef.current) {
      if (type === 'prev') {
        sliderRef.current.slickPrev()
      } else {
        sliderRef.current.slickNext()
      }
    }
  }

  return (
    <div className={ cn(s.PropertyCardSlider) }>
      <Slider ref={ sliderRef } { ...settings } className={ cn(s.slickSlider) }>
        { images.map((item, num) =>
          <div key={ `${ item.image_id }-${ num }` } className={ cn(s.item) }>
            <div className={ cn(s.imageWrapper) }>
              <Link to='property' data={{ property: data }}>
                <CloudImage
                  className="covered"
                  src={ item.image_path_url }
                  alt={ item.type }
                  responsive={{
                    desktop: { w: '500', ar: '1.5' },
                    tablet: { w: '400', ar: '1.5' },
                    mobile: { w: '300', ar: '1.2' }
                  }}
                />
              </Link>
            </div>
          </div>
        ) }
      </Slider>
      <div className={ cn(s.sliderControls, s.left, "cursor-pointer no-select") } onClick={ () => handleChange('prev') }>
        <Icon src={ ArrowRedIcon } rotate={ 270 } size='s'/>
      </div>
      <div className={ cn(s.sliderControls, s.right, "cursor-pointer no-select") } onClick={ () => handleChange('next') }>
        <Icon src={ ArrowRedIcon } rotate={ 90 } size='s'/>
      </div>
    </div>
  )
}

export default PropertyCardSlider
