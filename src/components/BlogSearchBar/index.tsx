import React from 'react'

import { Query, CategoryFilter } from './components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  values: Form
  change: (data: { name: string, value: any }) => void
  onReset: () => void
}

export type Form = {
  query: string
  category: string[]
}

const BlogSearchBar: React.FC<Props> = ({ values, change, onReset }) => {
  return (
    <div className={ cn(s.BlogSearchBar) }>
      <div className={ s.layout }>
        <div className={ cn(s.form) }>
          <Query value={ values.query } onChange={ change }/>
          <CategoryFilter value={ values.category } onChange={ change }/>
          <p className="mt-1 mb-0 ml-1 cursor-pointer" onClick={ onReset }>Reset</p>
        </div>
      </div>
    </div>
  )
}

export default BlogSearchBar
