import React from "react";

import { Button, Link } from "components";

import s from "./style.module.sass";
import LinkedInIcon from "assets/icons/linkedIn.svg";
import TwitterIcon from "assets/icons/twitter.svg";

const SocialLinks: React.FC = () => {
  return (
    <div className={s.SocialLinks}>
      <Link to="https://www.linkedin.com/company/88012" newTab>
        <Button
          transparent
          icon={LinkedInIcon}
          iconSize="xl"
        />
      </Link>
      <Link to="https://twitter.com/broll_insights" newTab>
        <Button
          transparent
          icon={TwitterIcon}
          iconSize="xl"
          className="ml-05"
        />
      </Link>
    </div>
  );
};

export default SocialLinks;
