import React from 'react'

import { Logo } from 'components'
import { SocialLinks } from './components'

import cn from 'classnames'
import s from './style.module.sass'

const Footer: React.FC = () => {
  return (
    <div className={ s.Footer }>
      <div className={ s.layer }/>
      <div className={ cn(s.layout, "ph-content box-sizing") }>
        <div className={ s.top }>
          <Logo/>
        </div>
        <div className={ cn(s.bottom, 'pt-3 pb-4') }>
          <div className={ cn(s.left, 'pl-3 box-sizing') }>
            <h2 className='thin greyBlue-3 m-0'>Contact<br/>Us.</h2>
            <div className={ cn(s.socials, 'no-desktop no-mobile') }>
              <SocialLinks/>
            </div>
          </div>
          <div className={ cn(s.center) }>
            <div className={ s.content }>
              <p className="bold greyBlue-3 mv-05 mt-0">Head Office</p>
              <p className="thin greyBlue-3 mv-05">
                Broll South Africa<br/>
                +27 11 441 4000 / 08610 BROLL<br/>
                61 Katherine Street, Sandown Ext. 54, 2196<br/>
                P.O. Box 1455, Saxonwold 2132
              </p>
            </div>
            <div className={ s.content }>
              <p className="thin white mv-05 mt-0">
                Fraud Hotline 080 111 6666 | Report Fraud<br/>
                Customer Care
              </p>
              <p className="bold white mt-1 mb-0">www.broll.com</p>
            </div>
          </div>
          <div className={ cn(s.right, 'no-tablet') }>
            <SocialLinks/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer
