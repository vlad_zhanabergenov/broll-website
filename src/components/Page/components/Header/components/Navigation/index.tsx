import { useReactiveVar } from '@apollo/client'
import initialState from 'cache/initialState'
import { searchVar } from 'cache/vars'
import { useDeviceType } from 'hooks'
import React, { useState } from 'react'
import { useRouter } from 'next/router'
import compareSearchFilters from 'services/compareSearchFilters'
import searchStateParser from 'services/searchStateParser'
import { navItems } from 'utilities/constants'

import { Icon, Link } from 'components'
import { Submenu } from './components'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrow.svg'

interface Props {
  onSelect?: () => void
}

const Navigation: React.FC<Props> = ({ onSelect }) => {
  const deviceType = useDeviceType()

  const { pathname } = useRouter()

  const searchState = useReactiveVar(searchVar)
  const isFiltersDefault = compareSearchFilters(searchState, initialState.search, [])

  const list = navItems.map(i => {
    const el = { ...i }

    if (el.id === 'search' && !isFiltersDefault) {
      el.path = `/search?${ searchStateParser.encode(searchState) }`
    }

    return {
      ...el,
      active: i.path === "/" ? pathname === "/" : pathname.startsWith(i.path)
    }
  })

  const [currentSubmenu, setCurrentSubmenu] = useState<string | null>(null)

  const handleSelect = (i: NavItem) => {
    if (!!i.submenu?.length) {
      setCurrentSubmenu(current => {
        return current === i.id ? null : i.id
      })
    } else {
      if (onSelect) onSelect()
    }
  }

  const handleClick = (i: NavItem) => {
    if ('submenu' in i) {
      handleSelect(i)
    }
  }

  const handleHover = (i: NavItem) => {
    if (deviceType === 'desktop' && currentSubmenu !== i.id) {
      handleClick(i)
    }
  }

  const handleBlur = () => {
    if (deviceType === 'desktop') {
      setCurrentSubmenu(null)
    }
  }

  return (
    <div className={ s.Navigation }>
      { list.map(i =>
        <Link newTab={i.id === 'careers'} key={ i.id } to={ i.path } onClick={ () => handleSelect(i) } transparent={ 'submenu' in i }
              className={ cn({ [`no-pointer-events ${ s.active }`]: i.active }) }>
          <div className={ cn(s.item, "cursor-pointer no-select box-sizing", { [s.active]: i.active }) }
               onClick={ () => handleClick(i) } onMouseEnter={ () => handleHover(i) } onMouseLeave={ () => handleBlur() }>
            { deviceType === 'desktop' ?
              <>
                <p className='small white m-0'>{ i.name }</p>
                <div className={ cn(s.underscore, 'no-pointer-events') }/>
              </>
            :
              <div className={ s.name }>
                <h4 className={ cn('no-select white light m-0 limit-string-1', { 'greyBlue-3': i.id === currentSubmenu }) }>
                  { i.name }
                </h4>
                { i.submenu && 
                  <Icon src={ ArrowIcon } rotate={ i.id === currentSubmenu ? 180 : 90 }/>
                }
              </div>
            }
            { i.submenu && i.id === currentSubmenu &&
              <Submenu
                list={ i.submenu }
                onSelect={ handleSelect }
                onClose={ () => deviceType !== 'mobile' && setCurrentSubmenu(null) }
              />
            }
          </div>
        </Link>
      )}
    </div>
  )
}

export default Navigation
