import React, { useRef } from 'react'
import { useOnClickOutside } from 'hooks'

import { Link } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  list: NavItem[]
  onSelect: (i: NavItem) => void
  onClose: () => void
}

const Submenu: React.FC<Props> = ({ list, onSelect, onClose }) => {
  const ref = useRef<HTMLDivElement>(null)

  useOnClickOutside(ref, onClose)

  return (
    <div ref={ ref } className={ cn(s.Submenu, "box-sizing") }>
      { list.map(i =>
        <Link key={ i.id } to={ i.path } onClick={ () => onSelect(i) }
              className={ cn(s.item, 'no-select') }>
          <div className={ cn(s.name, "no-desktop pv-05") }>
            <p className='white light m-0 limit-string-1'>{ i.name }</p>
          </div>
          <div className={ cn(s.name, "no-tablet no-mobile pv-05") }>
            <p className='small white light m-0 limit-string-1'>{ i.name }</p>
          </div>
        </Link>
      )}
    </div>
  )
}

export default Submenu
