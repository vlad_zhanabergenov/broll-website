import { resetFields, setField } from 'cache/mutations/search'
import { useRouter } from 'next/router'
import queryString from 'querystring'
import React, { useEffect, useRef, useState } from 'react'
import { useDeviceType, useForm, useOnClickOutside, useOnScroll } from 'hooks'

import { Link, Button, Input, Logo } from 'components'
import { Navigation } from './components'

import cn from 'classnames'
import s from './style.module.sass'
import MenuIcon from 'assets/icons/menu.svg'
import CrossIcon from 'assets/icons/cross.svg'
import SearchIcon from 'assets/icons/search.svg'
import SearchPlusIcon from 'assets/icons/searchPlus.svg'

type Options = Pick<AppCache['search'], 'query'>

interface Props {
  statical?: boolean
}

const Header: React.FC<Props> = ({
  statical
}) => {
  const deviceType = useDeviceType()
  const ref = useRef<HTMLDivElement>(null)


  const router = useRouter()

  const [withBackground, setWithBackground] = useState(false)
  const [showSideMenu, setShowSideMenu] = useState(false)

  const scrollTop = useOnScroll()

  const hideMenu = () => {
    setShowSideMenu(false)
  }

  useOnClickOutside(ref, hideMenu)

  useEffect(() => {
    if (!statical) {
      if (scrollTop > 10) {
        setWithBackground(true)
      } else {
        setWithBackground(false)
      }
    }
  }, [scrollTop])

  const handleSubmit = (values: Options) => {
    resetFields()
    setField('query', values.query)
    if (values.query) {
      router.push(`/search?${ queryString.encode({ query: values.query, deal: 'To Let' }) }`)
    } else {
      router.push('/search')
    }
  }

  const handleToggleSideMenu = () => {
    setShowSideMenu(current => !current)
  }

  const { values, change, submit } = useForm<Options>({
    fields: {
      query: { initialValue: "" }
    }, handleSubmit
  }, [])

  return (
    <div className={ cn(s.Header, { [s.withBackground]: withBackground || statical }) }>
      <div className={ s.layer }/>
      <div className={ cn(s.layout, "box-sizing") }>
        <div className={ cn(s.left, "box-sizing") }>
          <Link to="/">
            <Logo size={ withBackground || statical ? 's' : 'm' }/>
          </Link>
        </div>
        { deviceType === 'desktop' &&
          <div className={ cn(s.center, "pr-2 no-tablet no-mobile box-sizing") }>
            <Navigation/>
          </div>
        }
        <div className={ s.right }>
          { deviceType !== 'mobile' &&
            <Input className={ cn(s.search, 'no-mobile') } transparent white fontS
                   icon={ SearchIcon } iconHover={ SearchPlusIcon } onIconClick={ submit }
                   noAutofill name="query" placeholder="Property search" value={ values.query }
                   onChange={ change } onPressEnter={ submit } type='search'/>
          }
          { deviceType !== 'desktop' &&
            <Button className={ cn(s.menuButton, "no-desktop") } transparent
                    icon={ showSideMenu ? CrossIcon : MenuIcon } iconSize={ showSideMenu ? 'm' : 'l' }
                    onClick={ handleToggleSideMenu }/>
          }
        </div>
      </div>
      { deviceType !== 'desktop' &&
        <div ref={ ref } className={ cn(s.sideMenu, "no-desktop scrollable-y", { [s.opened]: showSideMenu, "no-pointer-events": !showSideMenu }) }>
          <div className={ cn(s.layout, "pv-2 ph-4 box-sizing") }>
            <Navigation onSelect={ handleToggleSideMenu }/>
          </div>
        </div>
      }
    </div>
  )
}

export default Header
