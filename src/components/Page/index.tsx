import React from 'react'
import Head from 'next/head'
import Script from 'next/script'

import { Loading } from 'components'
import { Footer, Header } from './components'

import cn from 'classnames'
import s from './style.module.sass'

const Page: React.FC<PageConfig> = ({
  title,
  withHeader,
  withHeaderStatic,
  withFooter,
  loading,
  children
}) => {
  return (
    <>
      <Script id="fcWidget-js" strategy='afterInteractive'>
        {`
          function initFreshChat() {
            window.fcWidget.init({
              token: "d07832d9-bfae-4063-ae3c-97eae12f5936", 
              host: "https://wchat.eu.freshchat.com"
            });
          }
          function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.eu.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"Freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
        `}
      </Script>
      <Head>
        <title>{ title ? `${ title } - Broll` : "Broll" }</title>
      </Head>
      <div className={ cn(s.Page, { [s.withHeaderStatic]: withHeaderStatic }) }>
        { (withHeader || withHeaderStatic) &&
          <div className={ s.header }>
            <Header statical={ withHeaderStatic }/>
          </div>
        }
        <div className={ s.content }>
          { loading ?
            <div className={ s.loading }>
              <Loading size='l'/>
            </div>
          :
            children
          }
        </div>
        { withFooter &&
          <div className={ s.footer }>
            <Footer/>
          </div>
        }
      </div>
    </>
  )
}

export default Page
