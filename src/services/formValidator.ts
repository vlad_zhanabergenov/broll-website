import { mapValues } from 'lodash'

export type FormValidatorFields<T> = {
  [key in keyof T]: {
    initialValue: T[key]
    value: T[key]
    required?: boolean
    validator: keyof typeof FormValidator.validators
  }
}

export default class FormValidator<T extends object> {
  constructor(values: T, fields: FormValidatorFields<T>) {
    this.errors = mapValues(fields, (item, key) => {
      if (item.required && !item.value) {
        return "Field is required"
      }
      if (!(!item.required && !item.value) && FormValidator.validators[item.validator]) {
        return FormValidator.validators[item.validator](values, key)
      }
      return null
    })
  }

  public readonly errors: { [key in keyof T]: string | null }

  static readonly validators = {
    email(values: { [key: string]: any }, key: string) {
      const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      if (!regExp.test(values[key])) { return "Please enter a valid email address." }
      return null
    },
    password(values: { [key: string]: any }, key: string) {
      if (values[key].length < 8) { return "Must contain at least 8 characters" }
      return null
    },
    confirmPassword(values: { [key: string]: any }, key: string) {
      if (!values.password) { return "Password not provided" }
      if (values[key] !== values.password) { return "Passwords doesn't match" }
      return null
    },
    min2(values: { [key: string]: any }, key: string) {
      if (values[key].length < 2) { return "Must contain at least 2 characters" }
      return null
    },
    min6(values: { [key: string]: any }, key: string) {
      if (values[key].length < 6) { return "Must contain at least 6 characters" }
      return null
    },
    phone(values: { [key: string]: any }, key: string) {
      const regExp = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)([2-9]1[02-9]‌​|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})\s*(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+)\s*)?$/i
      if (!regExp.test(values[key])) { return "Invalid phone number" }
      return null
    }
  }
}
