import { ApolloClient } from '@apollo/client'
import cache from 'cache'

const apolloClient = new ApolloClient({
  uri: process.env['NEXT_PUBLIC_GRAPHQL_API_URL'],
  cache,
  ssrMode: true
})

export default apolloClient
