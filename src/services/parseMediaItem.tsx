import _ from 'lodash'

const locale = 'en-US'

export default function parseMediaItem(data: NewsMediaItemContentful, type: string): NewsMediaItem {
  function getTranslatedField(path: string) {
    if (typeof _.get(data, path) === 'object' && _.get(data, `${ path }.${ locale }`)) {
      return _.get(data, `${ path }.${ locale }`)
    } else {
      return _.get(data, path)
    }
  }

  return {
    id: `${ getTranslatedField('title') }-${ getTranslatedField('category') }-${ getTranslatedField('publishDate') }`,
    type,
    slug: getTranslatedField('slug'),
    category: getTranslatedField('category'),
    author: getTranslatedField('author.fields') || null,
    publishDate: getTranslatedField('publishDate'),
    featuredArticle: getTranslatedField('featuredArticle'),
    title: getTranslatedField('title'),
    featuredImage: getTranslatedField('featuredImage'),
    heroImage: getTranslatedField('heroImage'),
    articleSummary: getTranslatedField('articleSummary'),
    articleCopy: getTranslatedField('articleCopy')
  }
}
