import _ from 'lodash'

const blackList = ['query', 'dealType', 'page']

export default function compareSearchFilters(first: Search, second: Search, customBlackList?: string[]): boolean {
  const clean = (data: Search) => _.pickBy(data, (_v, key) => !(customBlackList || blackList).includes(key))

  return _.isEqual(clean(first), clean(second))
}
