import { groupBy, mapValues, sortBy } from 'lodash'

export default function groupPropertiesByGmavenKey(list: AnyPropertyWithCustomFields[]) {
  const result: AnyPropertyWithCustomFields[] = []

  const groups = groupBy(list, 'property_gmaven_key')

  mapValues(groups, group => {
    if (group[0].dealType === 'toLet') {
      result.push({
        ...group[0],
        gross_price: sortBy(group, ['gross_price'])[0].gross_price,
        hasSpecialDeals: group.some(i => i.dealType === 'toLet' && i.special_deals),
        min_price: sortBy(group, ['gross_price'])[0].gross_price,
        max_price: sortBy(group, ['gross_price'])[group.length - 1].gross_price,
        min_gla: sortBy(group, ['min_gla'])[0].min_gla,
        max_gla: sortBy(group, ['max_gla'])[group.length - 1].max_gla
      })
    } else {
      group.forEach(i => result.push(i))
    }
  })

  return result
}
