import _ from 'lodash'

export interface StaticPropertyFromSlug {
  dealType: string
  category: string
  city: string
  suburb: string
  property_name: string
  unit_id?: string
}

export default {
  stringify: (data: StaticPropertyFromSlug) => {
    return `/${ Object.values(_.pickBy(data)).join('/') }/`
  },
  parse: (slug: string, unit?: boolean): StaticPropertyFromSlug => {
    const mapFromSlug = slug.split('/')
    const startIndex = slug.startsWith('/') ? 1 : 0
    const isUnit = typeof unit === 'undefined' ? mapFromSlug.length > (startIndex + 5) : unit

    return {
      dealType: mapFromSlug[startIndex],
      category: mapFromSlug[startIndex + 1],
      city: mapFromSlug[startIndex + 2],
      suburb: mapFromSlug[startIndex + 3],
      property_name: mapFromSlug[startIndex + 4],
      unit_id: isUnit ? mapFromSlug[startIndex + 5] : undefined
    }
  }
}
