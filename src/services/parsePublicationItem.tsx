import _ from 'lodash'

const locale = 'en-US'

export default function parsePublicationItem(data: PublicationItemContentful): PublicationItem {
  function getTranslatedField(path: string) {
    if (typeof _.get(data, path) === 'object' && _.get(data, `${ path }.${ locale }`)) {
      return _.get(data, `${ path }.${ locale }`)
    } else {
      return _.get(data, path)
    }
  }

  return {
    id: `${ getTranslatedField('title') }-${ getTranslatedField('country') }-${ getTranslatedField('downloadUrl') }`,
    title: getTranslatedField('title'),
    country: getTranslatedField('country'),
    downloadUrl: getTranslatedField('downloadUrl')
  }
}
