import { pickBy } from 'lodash'
import queryString from 'querystring'

const cleanState = (data: AppCache['search']) => {
  return pickBy({
    query: data.query,
    deal: data.dealType,
    category: data.propertyType?.length ? data.propertyType : null,
    city: data.city?.length ? data.city : null,
    suburb: data.suburb?.length ? data.suburb : null,
    minPrice: data.minPrice,
    maxPrice: data.maxPrice,
    minGLA: data.minGLA,
    maxGLA: data.maxGLA,
    broker: data.broker?.length ? data.broker : null,
    page: data.page || '1'
  })
}

export default {
  encode: (data: AppCache['search']) => {
    return queryString.encode(cleanState(data))
  },
  decode: (data: { [key: string]: any }): AppCache['search'] => {
    const {
      query, deal, category, city, suburb,
      minPrice, maxPrice, minGLA, maxGLA, broker, page
    } = data

    return {
      query: typeof query === 'string' ? query : "",
      dealType: typeof deal === 'string' ? deal : "",
      propertyType: typeof category === 'object' ? [...category] : typeof category === 'string' ? [category] : [],
      city: typeof city === 'object' ? [...city] : typeof city === 'string' ? [city] : [],
      suburb: typeof suburb === 'object' ? [...suburb] : typeof suburb === 'string' ? [suburb] : [],
      minPrice: typeof minPrice === 'string' ? minPrice : "",
      maxPrice: typeof maxPrice === 'string' ? maxPrice : "",
      minGLA: typeof minGLA === 'string' ? minGLA : "",
      maxGLA: typeof maxGLA === 'string' ? maxGLA : "",
      broker: typeof broker === 'object' ? [...broker] : typeof broker === 'string' ? [broker] : [],
      page: typeof page === 'string' ? page : '1'
    }
  },
  cleanState
}
