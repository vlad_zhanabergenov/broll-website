import algoliasearch from 'algoliasearch'

const searchClient = algoliasearch(process.env.NEXT_PUBLIC_ALGOLIA_APP_ID || "", process.env.ALGOLIA_ADMIN_API_KEY || "")

export default searchClient
