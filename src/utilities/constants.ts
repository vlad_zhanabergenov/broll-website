export const navItems: NavItem[] = [
  { id: 'home', name: "Home", path: "/" },
  {
    id: 'about-us',
    name: "About Us",
    path: "/about",
    submenu: [
      { id: 'about-group-overview', name: "Our Group Overview", path: "/about/group-overview" },
      { id: 'about-african-footprint', name: "Our African Footprint", path: "/about/african-footprint" },
      { id: 'about-commitment', name: "Our Commitment", path: "/about/commitment" },
      { id: 'about-capabilities', name: "Our Capabilities", path: "/about/capabilities" },
    ]
  },
  {
    id: 'services',
    name: "Services",
    path: "/services",
    submenu: [
      { id: 'services-property-management', name: "Property Management", path: "/services/property-management" },
      { id: 'services-occupier-services', name: "Occupier Services", path: "/services/occupier-services" },
      { id: 'capital-markets', name: "Capital Markets", path: "/services/capital-markets" },
      { id: 'strategy-consulting', name: "Strategy & Consulting", path: "/services/strategy-and-consulting" },
      { id: 'transaction-services', name: "Transaction Services", path: "/services/transaction-services" },
      { id: 'auctions-sales', name: "Auctions & Sales", path: "/services/auctions-and-sales" },
      { id: 'workplace-solutions', name: "Workplace Solutions", path: "/services/workplace-solutions" },
      { id: 'facilities-management', name: "Facilities Management", path: "/services/facilities-management" },
      { id: 'services-valuation-and-advisory-services', name: "Valuation & Advisory Services", path: "/services/valuation-and-advisory-services" },
      { id: 'intel-and-market-analysis', name: "Intel & Market Analysis", path: "/services/intel-and-market-analysis" },
      { id: 'technology-and-prop-tech-solutions', name: "Technology and Prop Tech Solutions", path: "/services/technology-and-prop-tech-solutions" },

      // { id: 'services-auctioneering', name: "Auctioneering", path: "/services/auctioneering" },
      // { id: 'services-broking', name: "Broking", path: "/services/broking" },
      // { id: 'services-research', name: "Research", path: "/services/research" },
      // { id: 'services-retail-leasing-and-consultancy', name: "Retail Leasing & Consultancy", path: "/services/retail-leasing-and-consultancy" },
      // { id: 'services-real-estate-investor-services', name: "Real Estate Investor Services", path: "/services/real-estate-investor-services" },

      { id: 'services-business-solutions-specialists', name: "Business Solutions Specialists", path: "/services/business-solutions-specialists" },
      { id: 'services-strategic-risk-management', name: "Strategic Risk Management", path: "/services/strategic-risk-management" }
    ]
  },
  { id: 'search', name: "Property Search", path: "/search" },
  {
    id: 'media-centre',
    name: "Media Centre",
    path: "/media-centre",
    submenu: [
      { id: 'media-centre-publications', name: "Publications", path: "/media-centre/publications" },
      { id: 'media-centre-latest-news', name: "Latest News", path: "/media-centre/latest-news" },
      // { id: 'media-centre-blog', name: "Blog", path: "/media-centre/blog" },
      { id: 'media-centre-awards', name: "Awards", path: "/media-centre/awards" }
    ]
  },
  { id: 'careers', name: "Careers", path: "https://broll.simplify.hr/" },
  { id: 'contact', name: "Contact", path: "/contact" },
  { id: 'privacy', name: "Privacy", path: "/privacy" }
]

export enum searchIndexes {
  Buy = "ForSale",
  Rent = "ToLet"
}

export const searchConfig = {
  distinct: 100,
  facetingAfterDistinct: false,
  hitsPerPage: 6,
  attributesToRetrieve: ["*"],
  facets: ['property_gmaven_key'],
  maxValuesPerFacet: 10000
}

export const searchOptions: HeroSearchOptions = {
  dealType: ["For Sale", "To Let"]
}
