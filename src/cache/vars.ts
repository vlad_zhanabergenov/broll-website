import { makeVar, ReactiveVar } from '@apollo/client'
import initialState from 'cache/initialState'

export const appVar: ReactiveVar<AppCache['app']> = makeVar<AppCache['app']>({
  ...initialState.app
})

export const searchVar: ReactiveVar<AppCache['search']> = makeVar<AppCache['search']>({
  ...initialState.search
})
