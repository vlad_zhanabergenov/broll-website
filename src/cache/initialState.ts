import { searchIndexes, searchOptions } from 'utilities/constants'

const initialState: AppCache = {
  app: {
    algoliaReady: false,
    algoliaStats: {
      nbHits: {
        [searchIndexes.Buy]: 0,
        [searchIndexes.Rent]: 0
      },
      facets: {}
    }
  },
  search: {
    query: "",
    dealType: searchOptions.dealType[1],
    propertyType: [],
    city: [],
    suburb: [],
    minGLA: "",
    maxGLA: "",
    minPrice: "",
    maxPrice: "",
    broker: [],
    page: '1',
  }
}

export default initialState
