import { appVar } from 'cache/vars'

export default function setAlgoliaStatsField(name: keyof AppCache['app']['algoliaStats'], value: any) {
  const prev = appVar()

  appVar({
    ...prev,
    algoliaStats: {
      ...prev.algoliaStats,
      [name]: value
    }
  })
}
