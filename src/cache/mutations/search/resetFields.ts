import { searchVar } from 'cache/vars'
import initialState from 'cache/initialState'

export default function resetFields(newFields?: AppCache['search']) {
  const newState = newFields ? newFields : initialState.search
  searchVar({ ...newState })
}
