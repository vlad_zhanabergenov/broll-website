Technologies in use: [Next.js](https://nextjs.org/), TypeScript, GraphQL, Contentful.

## Getting Started

To run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Deploy on Netlify

The app will be automatically deployed on `git push`
