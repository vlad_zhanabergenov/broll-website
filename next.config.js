const path = require('path')
const withPlugins = require('next-compose-plugins')
const imagesPlugin = require('next-images')

module.exports = withPlugins([
  [imagesPlugin, {}]
], {
  images: {
    disableStaticImages: true
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'src/styles')]
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /\.(graphql|gql)$/,
      exclude: /node_modules/,
      loader: 'graphql-tag/loader'
    })
    return config
  },
  webpackDevMiddleware: (config) => {
    return config
  }
})
