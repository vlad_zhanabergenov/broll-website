import { GetStaticProps } from 'next'

// import BlogPostScene from 'scenes/BlogPost'
import TestScene from 'scenes/Test'

export default function BlogPost() {
  return (
    <TestScene title="Blog Post"/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Blog Post",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 60
  }
}
