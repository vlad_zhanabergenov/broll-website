import NextDocument, { DocumentContext, Head, Html, Main, NextScript } from 'next/document'

class Document extends NextDocument {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await NextDocument.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html lang="en">
        <Head>
          <link rel="shortcut icon" type="image/png" href={ "/icons/favicon.png" }/>
        </Head>
        <body>
          <Main/>
          <div id="Modal"/>
          <NextScript/>
        </body>
      </Html>
    )
  }
}

export default Document
