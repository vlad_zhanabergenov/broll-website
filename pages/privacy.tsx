import { GetStaticProps } from 'next'

import PrivacyScene from 'scenes/Privacy'

export default function Privacy() {
  return (
    <PrivacyScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Privacy",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
