import React from 'react'
import { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import apolloClient from 'services/apollo'
import { searchIndexes } from 'utilities/constants'

import { Page, SearchContext } from 'components'

import 'styles/global.sass'

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <ApolloProvider client={ apolloClient }>
      <SearchContext
        propertiesCounts={{
          [searchIndexes.Rent]: pageProps.propertiesCounts?.[searchIndexes.Rent] || 0,
          [searchIndexes.Buy]: pageProps.propertiesCounts?.[searchIndexes.Buy] || 0
        }}
      >
        <Page { ...pageProps.pageConfig }>
          <Component { ...pageProps }/>
        </Page>
      </SearchContext>
    </ApolloProvider>
  )
}

export default App
