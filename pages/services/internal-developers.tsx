import { GetStaticProps } from 'next'
import InternalDevelopersScene from 'scenes/InternalDevelopers'

export default function InternalDevelopers() {
  return (
    <InternalDevelopersScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Internal Developers - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
