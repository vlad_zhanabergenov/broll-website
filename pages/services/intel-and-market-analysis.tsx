import { GetStaticProps } from 'next'
import IntelAndMarketAnalysisScene from 'scenes/IntelAndMarketAnalysis'

export default function IntelAndMarketAnalysis() {
  return (
    <IntelAndMarketAnalysisScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Intel & Market Analysis - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
