import { GetStaticProps } from 'next'

import BusinessSolutionsSpecialistsScene from 'scenes/BusinessSolutionsSpecialists'

export default function BusinessSolutionsSpecialists() {
  return (
    <BusinessSolutionsSpecialistsScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Business Solutions Specialists - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
