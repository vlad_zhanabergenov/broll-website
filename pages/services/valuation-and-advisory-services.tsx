import { GetStaticProps } from 'next'

import ValuationAndAdvisoryServicesScene from 'scenes/ValuationAndAdvisoryServices'

export default function ValuationAndAdvisoryServices() {
  return (
    <ValuationAndAdvisoryServicesScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Valuation and Advisory Services - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
