import { GetStaticProps } from 'next'

import ResearchScene from 'scenes/Research'

export default function Research() {
  return (
    <ResearchScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Research - Services",
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
