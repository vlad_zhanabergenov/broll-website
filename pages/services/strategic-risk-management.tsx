import { GetStaticProps } from 'next'

import StrategicRiskManagementScene from 'scenes/StrategicRiskManagement'

export default function StrategicRiskManagement() {
  return (
    <StrategicRiskManagementScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Strategic Risk Management - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
