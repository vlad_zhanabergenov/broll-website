import { GetStaticProps } from 'next'

import AuctioneeringScene from 'scenes/Auctioneering'

export default function Auctioneering() {
  return (
    <AuctioneeringScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Auctioneering - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 60
  }
}
