import { GetStaticProps } from 'next'

import StrategyConsultingScene from 'scenes/StrategyConsulting'

export default function StrategyConsulting() {
  return (
    <StrategyConsultingScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Strategy & Consulting - Services",
        withHeader: true,
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
