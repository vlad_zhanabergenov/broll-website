import { GetStaticProps } from 'next'

import PropertyManagementScene from 'scenes/PropertyManagement'

export default function PropertyManagement() {
  return (
    <PropertyManagementScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Property Management - Services",
        withHeader: true,
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
