import { GetStaticProps } from 'next'

import CapitalMarketsScene from 'scenes/CapitalMarkets'

export default function CapitalMarkets() {
  return (
    <CapitalMarketsScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Capital Markets - Services",
        withHeader: true,
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
