import { GetStaticProps } from 'next'

import BrokingScene from 'scenes/Broking'

export default function Broking() {
  return (
    <BrokingScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Broking - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 60
  }
}
