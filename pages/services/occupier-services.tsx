import { GetStaticProps } from 'next'

import OccupierServicesScene from 'scenes/OccupierServices'

export default function OccupierServices() {
  return (
    <OccupierServicesScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Occupier Services - Services",
        withHeader: true,
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
