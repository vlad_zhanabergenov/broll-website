import { GetStaticProps } from 'next'

import RealEstateInvestorServicesScene from 'scenes/RealEstateInvestorServices'

export default function RealEstateInvestorServices() {
  return (
    <RealEstateInvestorServicesScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Real Estate Investor Services - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
