import { GetStaticProps } from 'next'

import FacilitiesManagementScene from 'scenes/FacilitiesManagement'

export default function FacilitiesManagement() {
  return (
    <FacilitiesManagementScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Facilities Management - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 60
  }
}
