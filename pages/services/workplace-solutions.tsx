import { GetStaticProps } from 'next'

import WorkplaceSolutionsScene from 'scenes/WorkplaceSolutions'

export default function WorkplaceSolutions() {
  return (
    <WorkplaceSolutionsScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Workplace Solutions - Services",
        withHeader: true,
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
