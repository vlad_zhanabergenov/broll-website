import { GetStaticProps } from 'next'

import TransactionServicesScene from 'scenes/TransactionServices'

export default function StrategyConsulting() {
  return (
    <TransactionServicesScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Transaction Services - Services",
        withHeader: true,
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
