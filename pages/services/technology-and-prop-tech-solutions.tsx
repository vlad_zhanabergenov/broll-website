import { GetStaticProps } from 'next'

import TechnologyAndPropTechSolutionsScene from 'scenes/TechnologyAndPropTechSolutions'

export default function TechnologyAndPropTechSolutions() {
  return (
    <TechnologyAndPropTechSolutionsScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Technology and Prop Tech Solutions - Services",
        withHeader: true,
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
