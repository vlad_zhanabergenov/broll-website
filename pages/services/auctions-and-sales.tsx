import { GetStaticProps } from 'next'

import AuctionsSalesScene from 'scenes/AuctionsSales'

export default function StrategyConsulting() {
  return (
    <AuctionsSalesScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Auctions & Sales - Services",
        withHeader: true,
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
