import { GetStaticProps } from 'next'

import RetailLeasingAndConsultancyScene from 'scenes/RetailLeasingAndConsultancy'

export default function RetailLeasingAndConsultancy() {
  return (
    <RetailLeasingAndConsultancyScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Retail Leasing and Consultancy - Services",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
