import { GetStaticProps } from 'next'

import AwardsScene from 'scenes/Awards'

export default function Awards() {
  return (
    <AwardsScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Awards - Media Centre",
        withHeader: true,
        withFooter: true,
        withHeaderStatic: true
      }
    },
    revalidate: 30
  }
}
