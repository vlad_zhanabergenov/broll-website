import { GetStaticProps } from "next";

import TestScene from "scenes/Test";

export default function Blog() {
  return <TestScene title="Blog" />;
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> =
  async () => {
    return {
      props: {
        pageConfig: {
          title: "Blog - Media Centre",
          withHeader: true,
          withHeaderStatic: true,
          withFooter: true,
        },
      },
      revalidate: 30,
    };
  };
