import { GetStaticProps } from 'next'

import LatestNewsScene from 'scenes/LatestNews'

export default function LatestNews() {
  return (
    <LatestNewsScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Latest News - Media Centre",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
