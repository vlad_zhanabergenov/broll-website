import { getStaticLatestNews, getLatestNewsPost } from 'api/contentful'
import { GetStaticPaths, GetStaticProps } from 'next'

import NewsPostScene from 'scenes/NewsPost'

interface Props {
  newsPost: NewsMediaItem
}

export default function NewsPost({ newsPost }: Props) {
  return (
    <NewsPostScene newsPost={ newsPost }/>
  )
}

export const getStaticPaths: GetStaticPaths = async () => {
  const newsList = await getStaticLatestNews()

  const paths = newsList.map(i => {
    return {
      params: {
        slug: i
      }
    }
  })

  return {
    paths,
    fallback: 'blocking'
  }
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig } & Props> = async ({ params }) => {
  const slug = typeof params?.slug === 'string' ? params.slug : ""

  const newsPost = await getLatestNewsPost(slug)

  if (!newsPost) {
    return {
      notFound: true
    }
  }

  return {
    props: {
      pageConfig: {
        title: `${ newsPost.title } - Latest News`,
        withHeaderStatic: true,
        withFooter: true
      },
      newsPost
    },
    revalidate: 60
  }
}
