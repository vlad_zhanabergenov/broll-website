import { GetStaticProps } from 'next'

import PublicationsScene from 'scenes/Publications'

export default function Publications() {
  return (
    <PublicationsScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Publications - Media Centre",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
