import { GetStaticProps } from 'next'
import { getHeroCarouselItems, getNewsMediaItems } from 'api/contentful'
import { getFeaturedOfferings } from 'api/backend'

import HomeScene from 'scenes/Home'

interface Props {
  heroCarouselList: HeroCarouselItem[]
  newsMediaList: NewsMediaItem[]
  featuredOfferingsList: AnyPropertyWithCustomFields[]
}

export default function Home(props: Props) {
  return (
    <HomeScene { ...props }/>
  )
}

export const getStaticProps: GetStaticProps<Props & { pageConfig: PageConfig }> = async () => {
  const [
    heroCarouselItems,
    newsMediaItems,
    featuredOfferings
  ] = await Promise.all([
    await getHeroCarouselItems(),
    await getNewsMediaItems(),
    await getFeaturedOfferings()
  ])

  return {
    props: {
      pageConfig: {
        withHeader: true,
        withFooter: true
      },
      heroCarouselList: heroCarouselItems,
      newsMediaList: newsMediaItems,
      featuredOfferingsList: featuredOfferings
    },
    revalidate: 30
  }
}
