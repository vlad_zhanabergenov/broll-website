import { useRouter } from 'next/router'
import { GetStaticPaths, GetStaticProps } from 'next'
import { getStaticProperties, getPropertyToLetId, getPropertyForSaleId } from 'api/algolia'
import { getPropertyToLet, getPropertyForSale, getRelatedToLet, getRelatedForSale } from 'api/backend'
import slugParser from 'services/slugParser'

import LoadingScene from 'scenes/Loading'
import PropertyScene from 'scenes/Property'

interface Props {
  isUnit: boolean
  propertyList: AnyProperty[]
  relatedPropertiesList: AnyProperty[]
}

export default function Property({ isUnit, propertyList, relatedPropertiesList }: Props) {
  const router = useRouter()

  if (router.isFallback) {
    return (
      <LoadingScene/>
    )
  } else {
    return (
      <PropertyScene
        isUnit={ isUnit }
        propertyList={ propertyList }
        relatedProperties={ relatedPropertiesList }
      />
    )
  }
}

export const getStaticPaths: GetStaticPaths = async () => {
  const [toLetProperties, forSaleProperties] = await Promise.all([
    await getStaticProperties('ToLet'),
    await getStaticProperties('ForSale')
  ])

  const paths = [...toLetProperties, ...forSaleProperties].map(i => {
    const params = [i.dealType, i.category, i.city, i.suburb, i.property_name]

    return {
      params: {
        property: i.unit_id ? [...params, i.unit_id] : params
      }
    }
  })

  return {
    paths,
    fallback: true
  }
}

export const getStaticProps: GetStaticProps<Props & { pageConfig: PageConfig }> = async (ctx) => {
  let isUnit: boolean = false
  let propertyList: AnyProperty[] = []
  let relatedPropertiesList: AnyProperty[] = []

  const params = ctx.params ? ctx.params.property as string[] : null

  if (params) {
    try {
      const pathMap = slugParser.parse(params.join('/'))
      const slug = slugParser.stringify(pathMap)

      if (pathMap.unit_id) {
        isUnit = true
      }

      if (pathMap.dealType === 'to-let') {
        const property_gmaven_key = await getPropertyToLetId(slug)

        propertyList = property_gmaven_key ? await getPropertyToLet(property_gmaven_key) : []

        if (propertyList.length) {
          const { property_category, property_gmaven_key, suburb } = propertyList[0]
          relatedPropertiesList = await getRelatedToLet(property_gmaven_key || "", property_category || "", suburb || "")
        }
      } else if (pathMap.dealType === 'for-sale') {
        const gmaven_mapped_key = await getPropertyForSaleId(slug)

        propertyList = gmaven_mapped_key ? await getPropertyForSale(gmaven_mapped_key) : []

        if (propertyList.length) {
          const { property_category, gmaven_mapped_key, suburb } = propertyList[0]
          relatedPropertiesList = await getRelatedForSale(gmaven_mapped_key || "", property_category || "", suburb || "")
        }
      }
    } catch (err) {
      console.log('>>> ERROR:', err)
    }
  }

  if (!propertyList.length) {
    return {
      notFound: true,
      revalidate: 30
    }
  }

  return {
    props: {
      pageConfig: {
        title: propertyList[0].property_name,
        withHeaderStatic: true,
        withHeader: true,
        withFooter: true
      },
      isUnit,
      propertyList,
      relatedPropertiesList
    },
    revalidate: 30
  }
}
