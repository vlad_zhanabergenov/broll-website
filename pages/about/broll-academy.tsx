import { GetStaticProps } from 'next'

import BrollAcademyScene from 'scenes/BrollAcademy'

export default function BrollAcademy() {
  return (
    <BrollAcademyScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Broll Academy - About Us",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 60
  }
}
