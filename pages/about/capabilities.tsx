import { GetStaticProps } from 'next'

import CapabilitiesScene from 'scenes/Capabilities'

export default function Capabilities() {
  return (
    <CapabilitiesScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Our Capabilities - About Us",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
