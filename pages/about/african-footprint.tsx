import { GetStaticProps } from 'next'

import AfricanFootprintScene from 'scenes/AfricanFootprint'

export default function AfricanFootprint() {
  return (
    <AfricanFootprintScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "African Footprint - About Us",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
