import { GetStaticProps } from 'next'

import BrollFoundationScene from 'scenes/BrollFoundation'

export default function BrollFoundation() {
  return (
    <BrollFoundationScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Broll Foundation - About Us",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
