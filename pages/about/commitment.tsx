import { GetStaticProps } from 'next'

import CommitmentScene from 'scenes/Commitment'

export default function Commitment() {
  return (
    <CommitmentScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Our Commitment - About Us",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
