import { GetStaticProps } from 'next'

import GroupOverviewScene from 'scenes/GroupOverview'

export default function GroupOverview() {
  return (
    <GroupOverviewScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Group Overview - About Us",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 60
  }
}
