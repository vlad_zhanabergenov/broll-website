import { getPropertiesCount } from 'api/algolia'
import { GetStaticProps } from 'next'
import { searchIndexes } from 'utilities/constants'

import SearchScene from 'scenes/Search'

interface Props {
  propertiesCounts: {
    [searchIndexes.Rent]: number
    [searchIndexes.Buy]: number
  }
}

export default function Search() {
  return (
    <SearchScene/>
  )
}

export const getStaticProps: GetStaticProps<Props & { pageConfig: PageConfig }> = async () => {
  const [toLetPropertiesCount, forSalePropertiesCount] = await Promise.all([
    await getPropertiesCount('ToLet'),
    await getPropertiesCount('ForSale')
  ])

  return {
    props: {
      pageConfig: {
        title: "Search",
        withHeaderStatic: true,
        withFooter: true
      },
      propertiesCounts: {
        [searchIndexes.Rent]: toLetPropertiesCount,
        [searchIndexes.Buy]: forSalePropertiesCount
      }
    },
    revalidate: 30
  }
}
