import { GetStaticProps } from 'next'

import CareersScene from 'scenes/Careers'

export default function Careers() {
  return (
    <CareersScene />
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Careers",
        withHeaderStatic: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
