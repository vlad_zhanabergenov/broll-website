declare type QueryFeaturedToLetRes = {
  queryFeaturedToLet: PropertyToLet[]
}

declare type QueryFeaturedForSaleRes = {
  queryFeaturedForSale: PropertyForSale[]
}

declare type QueryPropertyToLetRes = {
  queryPropertyToLet: PropertyToLet[]
}

declare type QueryPropertyForSaleRes = {
  queryPropertyForSale: PropertyForSale[]
}

declare type QueryRelatedToLetRes = {
  queryRelatedToLet: PropertyToLet[]
}

declare type QueryRelatedForSaleRes = {
  queryRelatedForSale: PropertyForSale[]
}
