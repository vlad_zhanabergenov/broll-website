declare interface PageConfig {
  title?: string
  withHeader?: boolean
  withHeaderStatic?: boolean
  withFooter?: boolean
  loading?: boolean
}
