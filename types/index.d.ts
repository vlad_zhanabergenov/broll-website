declare type NavItem = {
  id: string
  name: string
  path: string
  submenu?: Omit<NavItem, 'submenu'>[]
}

declare type HeroSearchOptions = {
  dealType: ["For Sale", "To Let"]
}

declare type HeroCarouselItem = {
  id: string
  heading: string
  headingSubText: string
  image: string
  content: any
}

declare type FeaturedOfferingItem = {
  id: string
  label: string
  image: string
  imageLabel: string
  name: string
  gla: number
  type: 'Commercial'
  location: string
}

declare type NewsMediaItem = {
  id: string
  type: string
  featuredImage: string
  heroImage: string
  author: NewsMediaItemContentful['author']['fields']
} & Omit<NewsMediaItemContentful, 'featuredImage' | 'heroImage' | 'author'>

declare type PDFItem = 'propertyToLet' | 'propertyUnitToLet' | 'propertyForSale'

declare type PublicationItem = {
  id: string
  title: string,
  country: string,
  downloadUrl: string
}
