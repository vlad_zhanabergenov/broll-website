declare type ContentfulFile = {
  metadata: any
  fields: {
    file: {
      url: string
    }
  }
}

declare type HeroCarouselItemContentful = {
  sliderOrder: number
  sliderHasLink: boolean
  mainHeading: string
  mainHeadingSubText: string
  sliderBlurb: {
    data: any
    content: any[]
    nodeType: string
  }
  sliderImage: string
}

declare type NewsMediaItemContentful = {
  category: string
  author: {
    fields: {
      name: string
      title: string
      email: string
      telephone: string
      bio: string
      profilePicture: string
    }
  }
  publishDate: string
  featuredArticle: boolean
  title: string
  slug: string
  featuredImage: string
  heroImage: string
  articleSummary: string
  articleCopy: any
}

declare type PublicationItemContentful = {
  fields: {
    title: string,
    country: string,
    downloadUrl: string
  }
}
